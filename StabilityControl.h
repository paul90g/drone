#ifndef _STABILITY_CONTROL_H
#define _STABILITY_CONTROL_H

#include "Drivers.h"

typedef struct{
  float x,y,z;
} angle_type;

#define ANGLE_FILTER    0.80

void StabilityControl_calcAngle(angle_type *angle, gyro_type gyro, accel_type accel, float dt);

#endif /* _STABILITY_CONTROL_H */