#ifndef _MPU6050_DRIVER_H
#define _MPU6050_DRIVER_H

#include "Drivers.h"
#include "Type.h"

#define MPU6050_ADDR    0x68

/* REGISTERS */
#define MPU6050_SMPRT_DIV       0x19
#define MPU6050_CONFIG          0x1A
#define MPU6050_GYRO_CONFIG     0x1B
#define MPU6050_ACCEL_CONFIG    0x1C
#define MPU6050_ACCEL_XOUT_H    0x3B
#define MPU6050_ACCEL_XOUT_L    0x3C
#define MPU6050_ACCEL_YOUT_H    0x3D
#define MPU6050_ACCEL_YOUT_L    0x3E
#define MPU6050_ACCEL_ZOUT_H    0x3F
#define MPU6050_ACCEL_ZOUT_L    0x40
#define MPU6050_TEMP_OUT_H      0x41
#define MPU6050_TEMP_OUT_L      0x42
#define MPU6050_GYRO_XOUT_H     0x43
#define MPU6050_GYRO_XOUT_L     0x44
#define MPU6050_GYRO_YOUT_H     0x45
#define MPU6050_GYRO_YOUT_L     0x46
#define MPU6050_GYRO_ZOUT_H     0x47
#define MPU6050_GYRO_ZOUT_L     0x48
#define MPU6050_PWR_MGMT_1      0x6B
#define MPU6050_PWR_MGMT_2      0x6C
#define MPU6050_WHO_AM_I        0x75
/* END REGISTERS */

#define MPU6050_CALIBRATION_SAMPLES     200

#define MPU6050_ACCEL_SCALE_FACTOR      8192.0
#define MPU6050_GYRO_SCALE_FACTOR       16.4

void MPU6050_Init(void);
static void calibrate_gyro(void);
static void calibrate_accel(void);

void MPU6050_ReadGyro(gyro_type *gyro_data);
void MPU6050_ReadAccel(accel_type *accel_data);
void MPU6050_ReadAccel_Gyro(accel_type *accel_data, gyro_type *gyro_data);

void MPU6050_IssueRead(void);

void MPU6050_Handler(uint8 *data, uint8 data_size);

#endif /* _MPU6050_DRIVER_H */