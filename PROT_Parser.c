#include "Type.h"
#include "String.h"
#include "Timers.h"
#include "Drivers.h"
#include "PROT_Parser.h"
#include "ESP8266_Driver.h"
#include "ESP8266_Config.h"

extern ServerState Server_fsm;
extern uint8 command_buff[COMMAND_BUFFER_SIZE];
extern Timer16bit espTimer;
extern Timer16bit espRxTimer;
extern clientType client;

static uint8 comm[COMMAND_BUFFER_SIZE];

static uint8 str[2];

static const uint8 ping[]       = "ping";
static const uint8 error[]      = "error";

void decodeMessage(const uint8 *message){
  if(NULL_PTR != strstr(message, ping)){                // ping message
    init16bitTimer(&espTimer);
    strcpy(command_buff, "ping OK");
    clearBuffer();
    strcpy(comm, "AT+CIPSEND=");
    strncat(comm, COMMAND_BUFFER_SIZE, to_string(client.ID));
    strncat(comm, COMMAND_BUFFER_SIZE, ",");
    strncat(comm, COMMAND_BUFFER_SIZE, "7");
    strncat(comm, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
    init16bitTimer(&espRxTimer);
    ESP_Command(comm);
  } else if (NULL_PTR != strstr(message, error)){       // error message
    init16bitTimer(&espTimer);
    strcpy(command_buff, "error ACK");
    clearBuffer();
    strcpy(comm, "AT+CIPSEND=");
    strncat(comm, COMMAND_BUFFER_SIZE, to_string(client.ID));
    strncat(comm, COMMAND_BUFFER_SIZE, ",");
    strncat(comm, COMMAND_BUFFER_SIZE, "9");
    strncat(comm, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
    init16bitTimer(&espRxTimer);
    ESP_Command(comm);
  }
  return;
}


static uint8 *to_string(uint8 nr){
  str[0] = (nr + 48);
  str[1] = '\0';
  return str;
}