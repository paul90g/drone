#ifndef _L3G4200D_DRIVER
#define _L3G4200D_DRIVER

#include "Drivers.h"
#include "type.h"

#define L3G4200D_ADDR   0x69

/* internal registers */
#define L3G4200D_CTRL_REG1      0x20
#define L3G4200D_CTRL_REG2      0x21
#define L3G4200D_CTRL_REG3      0x22
#define L3G4200D_CTRL_REG4      0x23
#define L3G4200D_CTRL_REG5      0x24
#define L3G4200D_OUT_TEMP       0x26
#define L3G4200D_STATUS_REG     0x27
#define L3G4200D_OUT_X_L        0x28
#define L3G4200D_OUT_X_H        0x29
#define L3G4200D_OUT_Y_L        0x2A
#define L3G4200D_OUT_Y_H        0x2B
#define L3G4200D_OUT_Z_L        0x2C
#define L3G4200D_OUT_Z_H        0x2D
#define L3G4200D_FIFO_CTRL_REG  0x2E
#define L3G4200D_FIFO_SRC_REG   0x2F
#define L3G4200D_INT1_CFG       0x30
#define L3G4200D_INT1_SRC       0x31
#define L3G4200D_INT1_TSH_XH    0x32
#define L3G4200D_INT1_TSH_XL    0x33
#define L3G4200D_INT1_TSH_YH    0x34
#define L3G4200D_INT1_TSH_YL    0x35
#define L3G4200D_INT1_TSH_ZH    0x36
#define L3G4200D_INT1_TSH_ZL    0x37
#define L3G4200D_INT1_DURATION  0x38

#define CALIBRATION_SAMPLES     200

#define L3G4200D_SCALE_FACTOR   0.0175

void L3G4200D_Init(void);
static void calibrate_gyro(void);
void L3G4200D_ReadGyro(gyro_type *gyro_data);

void L3G4200D_IssueRead(void);

void L3G4200D_Handler(uint8 *data, uint8 data_size);

#endif /* _L3G4200D_DRIVER */