#ifndef _DMA2_DRIVER_H
#define _DMA2_DRIVER_H

#include "Type.h"
#include "Drivers.h"
#include "DMA_Type.h"

#define DMA2    ((DMA_Type *)DMA2_BASE)

/* LIFCR */
#define DMA2_ClearStream0Flags()        (DMA2->LIFCR |= STREAM0_CLEAR_BITMASK)
#define DMA2_ClearStream1Flags()        (DMA2->LIFCR |= STREAM1_CLEAR_BITMASK)
#define DMA2_ClearStream2Flags()        (DMA2->LIFCR |= STREAM2_CLEAR_BITMASK)
#define DMA2_ClearStream3Flags()        (DMA2->LIFCR |= STREAM3_CLEAR_BITMASK)

/* HIFCR */
#define DMA2_ClearStream4Flags()        (DMA2->HIFCR |= STREAM4_CLEAR_BITMASK)
#define DMA2_ClearStream5Flags()        (DMA2->HIFCR |= STREAM5_CLEAR_BITMASK)
#define DMA2_ClearStream6Flags()        (DMA2->HIFCR |= STREAM6_CLEAR_BITMASK)
#define DMA2_ClearStream7Flags()        (DMA2->HIFCR |= STREAM7_CLEAR_BITMASK)

/* Stream 0 */
#define DMA2_Stream0_Channel(CH)                {(DMA2->S0CR &= ~(CHSEL_BITMASK)); (DMA2->S0CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream0_Priority(PR_lvl)           {(DMA2->S0CR &= ~(PL_BITMASK)); (DMA2->S0CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream0_MemSize(MEM_size)          {(DMA2->S0CR &= ~(MSIZE_BITMASK)); (DMA2->S0CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream0_PeriphSize(MEM_size)       {(DMA2->S0CR &= ~(PSIZE_BITMASK)); (DMA2->S0CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream0_MemPointerFixed()          (DMA2->S0CR &= ~MINC_BIT)
#define DMA2_Stream0_MemPointerIncr()           (DMA2->S0CR |= MINC)
#define DMA2_Stream0_PeriphPointerFixed()       (DMA2->S0CR &= ~PINC_BIT)
#define DMA2_Stream0_PeriphPointerIncr()        (DMA2->S0CR |= PINC)
#define DMA2_Stream0_DataDirection(DATA_dir)    {(DMA2->S0CR &= ~DIR_BITMASK); (DMA2->S0CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream0_DMAControlFlow()           (DMA2->S0CR &= ~PFCTRL_BIT)
#define DMA2_Stream0_PeriphControlFlow()        (DMA2->S0CR |= PFCTRL_BIT)
#define DMA2_Stream0_TransfCompleteIntEn()      (DMA2->S0CR |= TCIE_DMA_BIT)
#define DMA2_Stream0_TransfCompleteIntDis()     (DMA2->S0CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream0_HaltTransfIntEn()          (DMA2->S0CR |= HTIE_BIT)
#define DMA2_Stream0_HaltTransfIntDis()         (DMA2->S0CR &= ~HTIE_BIT)
#define DMA2_Stream0_TransfErrIntEn()           (DMA2->S0CR |= TEIE_BIT)
#define DMA2_Stream0_TransfErrIntDis()          (DMA2->S0CR &= ~TEIE_BIT)
#define DMA2_Stream0_DirectModeIntEn()          (DMA2->S0CR |= DMEIE_BIT)
#define DMA2_Stream0_DirectModeIntDis()         (DMA2->S0CR &= ~DMEIE_BIT)
#define DMA2_Stream0_Enable()                   (DMA2->S0CR |= EN_BIT)
#define DMA2_Stream0_Disable()                  (DMA2->S0CR &= ~EN_BIT)
#define DMA2_Stream0_NrOfData(NR_data)          (DMA2->S0NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream0_PeriphAddress(Paddr)       (DMA2->S0PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream0_Mem0Address(Mem0addr)      (DMA2->S0M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream0_Mem1Address(Mem1addr)      (DMA2->S0M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream0_ReadTCFlag()               (DMA2->LISR & TCIF0_BIT)

/* Stream 1 */
#define DMA2_Stream1_Channel(CH)                {(DMA2->S1CR &= ~(CHSEL_BITMASK)); (DMA2->S1CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream1_Priority(PR_lvl)           {(DMA2->S1CR &= ~(PL_BITMASK)); (DMA2->S1CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream1_MemSize(MEM_size)          {(DMA2->S1CR &= ~(MSIZE_BITMASK)); (DMA2->S1CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream1_PeriphSize(MEM_size)       {(DMA2->S1CR &= ~(PSIZE_BITMASK)); (DMA2->S1CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream1_MemPointerFixed()          (DMA2->S1CR &= ~MINC_BIT)
#define DMA2_Stream1_MemPointerIncr()           (DMA2->S1CR |= MINC)
#define DMA2_Stream1_PeriphPointerFixed()       (DMA2->S1CR &= ~PINC_BIT)
#define DMA2_Stream1_PeriphPointerIncr()        (DMA2->S1CR |= PINC)
#define DMA2_Stream1_DataDirection(DATA_dir)    {(DMA2->S1CR &= ~DIR_BITMASK); (DMA2->S1CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream1_DMAControlFlow()           (DMA2->S1CR &= ~PFCTRL_BIT)
#define DMA2_Stream1_PeriphControlFlow()        (DMA2->S1CR |= PFCTRL_BIT)
#define DMA2_Stream1_TransfCompleteIntEn()      (DMA2->S1CR |= TCIE_DMA_BIT)
#define DMA2_Stream1_TransfCompleteIntDis()     (DMA2->S1CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream1_HaltTransfIntEn()          (DMA2->S1CR |= HTIE_BIT)
#define DMA2_Stream1_HaltTransfIntDis()         (DMA2->S1CR &= ~HTIE_BIT)
#define DMA2_Stream1_TransfErrIntEn()           (DMA2->S1CR |= TEIE_BIT)
#define DMA2_Stream1_TransfErrIntDis()          (DMA2->S1CR &= ~TEIE_BIT)
#define DMA2_Stream1_DirectModeIntEn()          (DMA2->S1CR |= DMEIE_BIT)
#define DMA2_Stream1_DirectModeIntDis()         (DMA2->S1CR &= ~DMEIE_BIT)
#define DMA2_Stream1_Enable()                   (DMA2->S1CR |= EN_BIT)
#define DMA2_Stream1_Disable()                  (DMA2->S1CR &= ~EN_BIT)
#define DMA2_Stream1_NrOfData(NR_data)          (DMA2->S1NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream1_PeriphAddress(Paddr)       (DMA2->S1PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream1_Mem0Address(Mem0addr)      (DMA2->S1M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream1_Mem1Address(Mem1addr)      (DMA2->S1M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream1_ReadTCFlag()               (DMA2->LISR & TCIF1_BIT)

/* Stream 2 */
#define DMA2_Stream2_Channel(CH)                {(DMA2->S2CR &= ~(CHSEL_BITMASK)); (DMA2->S2CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream2_Priority(PR_lvl)           {(DMA2->S2CR &= ~(PL_BITMASK)); (DMA2->S2CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream2_MemSize(MEM_size)          {(DMA2->S2CR &= ~(MSIZE_BITMASK)); (DMA2->S2CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream2_PeriphSize(MEM_size)       {(DMA2->S2CR &= ~(PSIZE_BITMASK)); (DMA2->S2CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream2_MemPointerFixed()          (DMA2->S2CR &= ~MINC_BIT)
#define DMA2_Stream2_MemPointerIncr()           (DMA2->S2CR |= MINC)
#define DMA2_Stream2_PeriphPointerFixed()       (DMA2->S2CR &= ~PINC_BIT)
#define DMA2_Stream2_PeriphPointerIncr()        (DMA2->S2CR |= PINC)
#define DMA2_Stream2_DataDirection(DATA_dir)    {(DMA2->S2CR &= ~DIR_BITMASK); (DMA2->S2CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream2_DMAControlFlow()           (DMA2->S2CR &= ~PFCTRL_BIT)
#define DMA2_Stream2_PeriphControlFlow()        (DMA2->S2CR |= PFCTRL_BIT)
#define DMA2_Stream2_TransfCompleteIntEn()      (DMA2->S2CR |= TCIE_DMA_BIT)
#define DMA2_Stream2_TransfCompleteIntDis()     (DMA2->S2CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream2_HaltTransfIntEn()          (DMA2->S2CR |= HTIE_BIT)
#define DMA2_Stream2_HaltTransfIntDis()         (DMA2->S2CR &= ~HTIE_BIT)
#define DMA2_Stream2_TransfErrIntEn()           (DMA2->S2CR |= TEIE_BIT)
#define DMA2_Stream2_TransfErrIntDis()          (DMA2->S2CR &= ~TEIE_BIT)
#define DMA2_Stream2_DirectModeIntEn()          (DMA2->S2CR |= DMEIE_BIT)
#define DMA2_Stream2_DirectModeIntDis()         (DMA2->S2CR &= ~DMEIE_BIT)
#define DMA2_Stream2_Enable()                   (DMA2->S2CR |= EN_BIT)
#define DMA2_Stream2_Disable()                  (DMA2->S2CR &= ~EN_BIT)
#define DMA2_Stream2_NrOfData(NR_data)          (DMA2->S2NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream2_PeriphAddress(Paddr)       (DMA2->S2PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream2_Mem0Address(Mem0addr)      (DMA2->S2M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream2_Mem1Address(Mem1addr)      (DMA2->S2M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream2_ReadTCFlag()               (DMA2->LISR & TCIF2_BIT)

/* Stream 3 */
#define DMA2_Stream3_Channel(CH)                {(DMA2->S3CR &= ~(CHSEL_BITMASK)); (DMA2->S3CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream3_Priority(PR_lvl)           {(DMA2->S3CR &= ~(PL_BITMASK)); (DMA2->S3CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream3_MemSize(MEM_size)          {(DMA2->S3CR &= ~(MSIZE_BITMASK)); (DMA2->S3CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream3_PeriphSize(MEM_size)       {(DMA2->S3CR &= ~(PSIZE_BITMASK)); (DMA2->S3CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream3_MemPointerFixed()          (DMA2->S3CR &= ~MINC_BIT)
#define DMA2_Stream3_MemPointerIncr()           (DMA2->S3CR |= MINC)
#define DMA2_Stream3_PeriphPointerFixed()       (DMA2->S3CR &= ~PINC_BIT)
#define DMA2_Stream3_PeriphPointerIncr()        (DMA2->S3CR |= PINC)
#define DMA2_Stream3_DataDirection(DATA_dir)    {(DMA2->S3CR &= ~DIR_BITMASK); (DMA2->S3CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream3_DMAControlFlow()           (DMA2->S3CR &= ~PFCTRL_BIT)
#define DMA2_Stream3_PeriphControlFlow()        (DMA2->S3CR |= PFCTRL_BIT)
#define DMA2_Stream3_TransfCompleteIntEn()      (DMA2->S3CR |= TCIE_DMA_BIT)
#define DMA2_Stream3_TransfCompleteIntDis()     (DMA2->S3CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream3_HaltTransfIntEn()          (DMA2->S3CR |= HTIE_BIT)
#define DMA2_Stream3_HaltTransfIntDis()         (DMA2->S3CR &= ~HTIE_BIT)
#define DMA2_Stream3_TransfErrIntEn()           (DMA2->S3CR |= TEIE_BIT)
#define DMA2_Stream3_TransfErrIntDis()          (DMA2->S3CR &= ~TEIE_BIT)
#define DMA2_Stream3_DirectModeIntEn()          (DMA2->S3CR |= DMEIE_BIT)
#define DMA2_Stream3_DirectModeIntDis()         (DMA2->S3CR &= ~DMEIE_BIT)
#define DMA2_Stream3_Enable()                   (DMA2->S3CR |= EN_BIT)
#define DMA2_Stream3_Disable()                  (DMA2->S3CR &= ~EN_BIT)
#define DMA2_Stream3_NrOfData(NR_data)          (DMA2->S3NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream3_PeriphAddress(Paddr)       (DMA2->S3PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream3_Mem0Address(Mem0addr)      (DMA2->S3M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream3_Mem1Address(Mem1addr)      (DMA2->S3M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream3_ReadTCFlag()               (DMA2->LISR & TCIF3_BIT)

/* Stream 4 */
#define DMA2_Stream4_Channel(CH)                {(DMA2->S4CR &= ~(CHSEL_BITMASK)); (DMA2->S4CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream4_Priority(PR_lvl)           {(DMA2->S4CR &= ~(PL_BITMASK)); (DMA2->S4CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream4_MemSize(MEM_size)          {(DMA2->S4CR &= ~(MSIZE_BITMASK)); (DMA2->S4CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream4_PeriphSize(MEM_size)       {(DMA2->S4CR &= ~(PSIZE_BITMASK)); (DMA2->S4CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream4_MemPointerFixed()          (DMA2->S4CR &= ~MINC_BIT)
#define DMA2_Stream4_MemPointerIncr()           (DMA2->S4CR |= MINC)
#define DMA2_Stream4_PeriphPointerFixed()       (DMA2->S4CR &= ~PINC_BIT)
#define DMA2_Stream4_PeriphPointerIncr()        (DMA2->S4CR |= PINC)
#define DMA2_Stream4_DataDirection(DATA_dir)    {(DMA2->S4CR &= ~DIR_BITMASK); (DMA2->S4CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream4_DMAControlFlow()           (DMA2->S4CR &= ~PFCTRL_BIT)
#define DMA2_Stream4_PeriphControlFlow()        (DMA2->S4CR |= PFCTRL_BIT)
#define DMA2_Stream4_TransfCompleteIntEn()      (DMA2->S4CR |= TCIE_DMA_BIT)
#define DMA2_Stream4_TransfCompleteIntDis()     (DMA2->S4CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream4_HaltTransfIntEn()          (DMA2->S4CR |= HTIE_BIT)
#define DMA2_Stream4_HaltTransfIntDis()         (DMA2->S4CR &= ~HTIE_BIT)
#define DMA2_Stream4_TransfErrIntEn()           (DMA2->S4CR |= TEIE_BIT)
#define DMA2_Stream4_TransfErrIntDis()          (DMA2->S4CR &= ~TEIE_BIT)
#define DMA2_Stream4_DirectModeIntEn()          (DMA2->S4CR |= DMEIE_BIT)
#define DMA2_Stream4_DirectModeIntDis()         (DMA2->S4CR &= ~DMEIE_BIT)
#define DMA2_Stream4_Enable()                   (DMA2->S4CR |= EN_BIT)
#define DMA2_Stream4_Disable()                  (DMA2->S4CR &= ~EN_BIT)
#define DMA2_Stream4_NrOfData(NR_data)          (DMA2->S4NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream4_PeriphAddress(Paddr)       (DMA2->S4PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream4_Mem0Address(Mem0addr)      (DMA2->S4M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream4_Mem1Address(Mem1addr)      (DMA2->S4M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream4_ReadTCFlag()               (DMA2->HISR & TCIF4_BIT)

/* Stream 5 */
#define DMA2_Stream5_Channel(CH)                {(DMA2->S5CR &= ~(CHSEL_BITMASK)); (DMA2->S5CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream5_Priority(PR_lvl)           {(DMA2->S5CR &= ~(PL_BITMASK)); (DMA2->S5CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream5_MemSize(MEM_size)          {(DMA2->S5CR &= ~(MSIZE_BITMASK)); (DMA2->S5CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream5_PeriphSize(MEM_size)       {(DMA2->S5CR &= ~(PSIZE_BITMASK)); (DMA2->S5CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream5_MemPointerFixed()          (DMA2->S5CR &= ~MINC_BIT)
#define DMA2_Stream5_MemPointerIncr()           (DMA2->S5CR |= MINC)
#define DMA2_Stream5_PeriphPointerFixed()       (DMA2->S5CR &= ~PINC_BIT)
#define DMA2_Stream5_PeriphPointerIncr()        (DMA2->S5CR |= PINC)
#define DMA2_Stream5_DataDirection(DATA_dir)    {(DMA2->S5CR &= ~DIR_BITMASK); (DMA2->S5CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream5_DMAControlFlow()           (DMA2->S5CR &= ~PFCTRL_BIT)
#define DMA2_Stream5_PeriphControlFlow()        (DMA2->S5CR |= PFCTRL_BIT)
#define DMA2_Stream5_TransfCompleteIntEn()      (DMA2->S5CR |= TCIE_DMA_BIT)
#define DMA2_Stream5_TransfCompleteIntDis()     (DMA2->S5CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream5_HaltTransfIntEn()          (DMA2->S5CR |= HTIE_BIT)
#define DMA2_Stream5_HaltTransfIntDis()         (DMA2->S5CR &= ~HTIE_BIT)
#define DMA2_Stream5_TransfErrIntEn()           (DMA2->S5CR |= TEIE_BIT)
#define DMA2_Stream5_TransfErrIntDis()          (DMA2->S5CR &= ~TEIE_BIT)
#define DMA2_Stream5_DirectModeIntEn()          (DMA2->S5CR |= DMEIE_BIT)
#define DMA2_Stream5_DirectModeIntDis()         (DMA2->S5CR &= ~DMEIE_BIT)
#define DMA2_Stream5_Enable()                   (DMA2->S5CR |= EN_BIT)
#define DMA2_Stream5_Disable()                  (DMA2->S5CR &= ~EN_BIT)
#define DMA2_Stream5_NrOfData(NR_data)          (DMA2->S5NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream5_PeriphAddress(Paddr)       (DMA2->S5PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream5_Mem0Address(Mem0addr)      (DMA2->S5M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream5_Mem1Address(Mem1addr)      (DMA2->S5M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream5_ReadTCFlag()               (DMA2->HISR & TCIF5_BIT)

/* Stream 6 */
#define DMA2_Stream6_Channel(CH)                {(DMA2->S6CR &= ~(CHSEL_BITMASK)); (DMA2->S6CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream6_Priority(PR_lvl)           {(DMA2->S6CR &= ~(PL_BITMASK)); (DMA2->S6CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream6_MemSize(MEM_size)          {(DMA2->S6CR &= ~(MSIZE_BITMASK)); (DMA2->S6CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream6_PeriphSize(MEM_size)       {(DMA2->S6CR &= ~(PSIZE_BITMASK)); (DMA2->S6CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream6_MemPointerFixed()          (DMA2->S6CR &= ~MINC_BIT)
#define DMA2_Stream6_MemPointerIncr()           (DMA2->S6CR |= MINC_BIT)
#define DMA2_Stream6_PeriphPointerFixed()       (DMA2->S6CR &= ~PINC_BIT)
#define DMA2_Stream6_PeriphPointerIncr()        (DMA2->S6CR |= PINC)
#define DMA2_Stream6_DataDirection(DATA_dir)    {(DMA2->S6CR &= ~DIR_BITMASK); (DMA2->S6CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream6_DMAControlFlow()           (DMA2->S6CR &= ~PFCTRL_BIT)
#define DMA2_Stream6_PeriphControlFlow()        (DMA2->S6CR |= PFCTRL_BIT)
#define DMA2_Stream6_TransfCompleteIntEn()      (DMA2->S6CR |= TCIE_DMA_BIT)
#define DMA2_Stream6_TransfCompleteIntDis()     (DMA2->S6CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream6_HaltTransfIntEn()          (DMA2->S6CR |= HTIE_BIT)
#define DMA2_Stream6_HaltTransfIntDis()         (DMA2->S6CR &= ~HTIE_BIT)
#define DMA2_Stream6_TransfErrIntEn()           (DMA2->S6CR |= TEIE_BIT)
#define DMA2_Stream6_TransfErrIntDis()          (DMA2->S6CR &= ~TEIE_BIT)
#define DMA2_Stream6_DirectModeIntEn()          (DMA2->S6CR |= DMEIE_BIT)
#define DMA2_Stream6_DirectModeIntDis()         (DMA2->S6CR &= ~DMEIE_BIT)
#define DMA2_Stream6_Enable()                   (DMA2->S6CR |= EN_BIT)
#define DMA2_Stream6_Disable()                  (DMA2->S6CR &= ~EN_BIT)
#define DMA2_Stream6_NrOfData(NR_data)          (DMA2->S6NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream6_PeriphAddress(Paddr)       (DMA2->S6PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream6_Mem0Address(Mem0addr)      (DMA2->S6M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream6_Mem1Address(Mem1addr)      (DMA2->S6M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream6_ReadTCFlag()               (DMA2->HISR & TCIF6_BIT)

/* Stream 7 */
#define DMA2_Stream7_Channel(CH)                {(DMA2->S7CR &= ~(CHSEL_BITMASK)); (DMA2->S7CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA2_Stream7_Priority(PR_lvl)           {(DMA2->S7CR &= ~(PL_BITMASK)); (DMA2->S7CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA2_Stream7_MemSize(MEM_size)          {(DMA2->S7CR &= ~(MSIZE_BITMASK)); (DMA2->S7CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA2_Stream7_PeriphSize(MEM_size)       {(DMA2->S7CR &= ~(PSIZE_BITMASK)); (DMA2->S7CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA2_Stream7_MemPointerFixed()          (DMA2->S7CR &= ~MINC_BIT)
#define DMA2_Stream7_MemPointerIncr()           (DMA2->S7CR |= MINC_BIT)
#define DMA2_Stream7_PeriphPointerFixed()       (DMA2->S7CR &= ~PINC_BIT)
#define DMA2_Stream7_PeriphPointerIncr()        (DMA2->S7CR |= PINC_BIT)
#define DMA2_Stream7_DataDirection(DATA_dir)    {(DMA2->S7CR &= ~DIR_BITMASK); (DMA2->S7CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA2_Stream7_DMAControlFlow()           (DMA2->S7CR &= ~PFCTRL_BIT)
#define DMA2_Stream7_PeriphControlFlow()        (DMA2->S7CR |= PFCTRL_BIT)
#define DMA2_Stream7_TransfCompleteIntEn()      (DMA2->S7CR |= TCIE_DMA_BIT)
#define DMA2_Stream7_TransfCompleteIntDis()     (DMA2->S7CR &= ~TCIE_DMA_BIT)
#define DMA2_Stream7_HaltTransfIntEn()          (DMA2->S7CR |= HTIE_BIT)
#define DMA2_Stream7_HaltTransfIntDis()         (DMA2->S7CR &= ~HTIE_BIT)
#define DMA2_Stream7_TransfErrIntEn()           (DMA2->S7CR |= TEIE_BIT)
#define DMA2_Stream7_TransfErrIntDis()          (DMA2->S7CR &= ~TEIE_BIT)
#define DMA2_Stream7_DirectModeIntEn()          (DMA2->S7CR |= DMEIE_BIT)
#define DMA2_Stream7_DirectModeIntDis()         (DMA2->S7CR &= ~DMEIE_BIT)
#define DMA2_Stream7_Enable()                   (DMA2->S7CR |= EN_BIT)
#define DMA2_Stream7_Disable()                  (DMA2->S7CR &= ~EN_BIT)
#define DMA2_Stream7_NrOfData(NR_data)          (DMA2->S7NDTR = (NR_data & NDT_BITMASK))
#define DMA2_Stream7_PeriphAddress(Paddr)       (DMA2->S7PAR = (Paddr & PAR_BITMASK))
#define DMA2_Stream7_Mem0Address(Mem0addr)      (DMA2->S7M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA2_Stream7_Mem1Address(Mem1addr)      (DMA2->S7M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA2_Stream7_ReadTCFlag()               (DMA2->HISR & TCIF7_BIT)

void DMA2_Init(void);

#endif /* _DMA2_DRIVER_H */