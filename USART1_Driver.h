#ifndef _USART1_DRIVER_H
#define _USART1_DRIVER_H

#include "USART_Type.h"

#define ONE_STOP_BIT    0
#define HALF_STOP_BIT   1
#define TWO_STOP_BIT    2

#define USART1  ((USART_Type *)USART1_BASE)

/* Status Register */
#define USART1_ClearStatusFlags()       (USART1->SR = 0x00C00000)

/* Data Register */
#define USART1_WriteData(DATA)          (USART1->DR = (DATA & 0x1FF))
#define USART1_ReadData()               (USART1->DR)

/* Baud Rate Register */
#define USART1_ResetBaudRegister()      (USART1->BRR = 0x00)
#define USART1_SetBaud_Register(BAUD)   (USART1->BRR = BAUD)

/* Control Register 1 */
#define USART1_SetOversampling8()       (USART1->CR1 |= OVER8_BIT)
#define USART1_SetOversampling16()      (USART1->CR1 &= ~OVER8_BIT)
#define USART1_Enable()                 (USART1->CR1 |= UE_BIT)
#define USART1_Disable()                (USART1->CR1 &= ~UE_BIT)
#define USART1_WordLength8Bit()         (USART1->CR1 &= ~M_BIT)
#define USART1_WordLength9Bit()         (USART1->CR1 |= M_BIT)
#define USART1_EnableParityControl()    (USART1->CR1 |= PCE_BIT)
#define USART1_DisableParityControl()   (USART1->CR1 &= ~PCE_BIT)
#define USART1_EnablePEInterrupt()      (USART1->CR1 |= PEIE_BIT)
#define USART1_DisablePEInterrupt()     (USART1->CR1 &= ~PEIE_BIT)
#define USART1_EnableTXEInterrupt()     (USART1->CR1 |= TXEIE_BIT)
#define USART1_DisableTXEInterrupt()    (USART1->CR1 &= ~TXEIE_BIT)
#define USART1_EnableTCInterrupt()      (USART1->CR1 |= TCIE_BIT)
#define USART1_DisableTCInterrupt()     (USART1->CR1 &= ~TCIE_BIT)
#define USART1_EnableRXNEInterrupt()    (USART1->CR1 |= RXNEIE_BIT)
#define USART1_DisableRXNEInterrupt()   (USART1->CR1 &= ~RXNEIE_BIT)
#define USART1_EnableIDLEInterrupt()    (USART1->CR1 |= IDLEIE_BIT
#define USART1_DisableIDLEInterrupt()   (USART1->CR1 &= ~IDLEIE_BIT)
#define USART1_EnableTransmitter()      (USART1->CR1 |= TE_BIT)
#define USART1_DisableTransmitter()     (USART1->CR1 &= ~TE_BIT)
#define USART1_EnableReceiver()         (USART1->CR1 |= RE_BIT)
#define USART1_DisableReceiver()        (USART1->CR1 &= ~RE_BIT)

/* Control Register 2 */
#define USART1_SetStopBits(STOP)        {(USART1->CR2 &= ~STOP_BITMASK); (USART1->CR2 |= (STOP << STOP_BITS_OFFSET));}
#define USART1_ClockEnable()            (USART1->CR2 |= CLKEN_BIT)
#define USART1_ClockDisable()           (USART1->CR2 &= ~CLKEN_BIT)

/* Control Register 3 */
#define USART1_EnableDMATransm()        (USART1->CR3 |= DMAT_BIT)
#define USART1_EnableDMAReceive()       (USART1->CR3 |= DMAR_BIT)

void USART1_Init(void);
void USART1_SendData(const uint8 *data);
static void USART1_SetBaud(uint32 baud);

#endif /* _USART1_DRIVER_H */