#include "ADC_Driver.h"
#include "Drivers.h"

void ADC_Init(){
  Enable_ADC1_Clk();
  
  ADC_SetClk();
  ADC_SetResolution(RES_12BIT);
  ADC_NrOfChannels(1);
  ADC_SetSEQ1(0);
  
  ADC_EOCIntEnable();
  ADC_Enable();
  
  return;
}