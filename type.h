#ifndef _TYPE_H
#define _TYPE_H

typedef unsigned int    uint32;
typedef int             int32;
typedef unsigned short  uint16;
typedef short           int16;
typedef unsigned char   uint8;
typedef char            int8;
typedef float           float32;
typedef uint8           boolean;

typedef struct{
  float x,y,z;
} gyro_type;

typedef union{
  uint8 data[6];
  struct{
   int16 x,y,z;
  } xyz_data;
} gyro_union;

typedef struct{
  float x,y,z;
} accel_type;

typedef union{
  uint8 data[6];
  struct{
    int16 x,y,z;
  } xyz_data;
} accel_union;

typedef union{
  uint8 data[2];
  int16 temp;
} temp_union;

#define TRUE            (boolean)1
#define FALSE           (boolean)0
#define NULL_PTR        ((void *)0)


#endif /* _TYPE_H */