#ifndef _FLASH_INTERF_DRIVER_H
#define _FLASH_INTERF_DRIVER_H

#include "mem_map.h"

typedef struct{
  uint32 ACR;
  uint32 KEYR;
  uint32 OPTKEYR;
  uint32 SR;
  uint32 CR;
  uint32 OPTCR;
} FlashIntf_Type;

#define FlashIntf       ((FlashIntf_Type *) FLASH_INTF_BASE)

#define DCEN_BIT                10      // Data Cache Enable Bit
#define ICEN_BIT                9       // Instruction Cache Enable Bit
#define PRFTEN                  8       // Prefetch Enable Bit

#define LATENCY_BITMASK         0x0F
#define LATENCY_BITS_OFFSET     0

#define Enable_DataCache()              (FlashIntf->ACR |= (1 << DCEN_BIT))
#define Disable_DataCache()             (FlashIntf->ACR &= ~(1 << DCEN_BIT))
#define Enable_InstrCache()             (FlashIntf->ACR |= (1 << ICEN_BIT))
#define Disable_InstrCache()            (FlashIntf->ACR &= ~(1 << ICEN_BIT))

#define Set_Flash_Latency(Latency)      {FlashIntf->ACR &= ~(LATENCY_BITMASK << LATENCY_BITS_OFFSET); FlashIntf->ACR |= (Latency << LATENCY_BITS_OFFSET);}

void FlashIntf_Init(void);

#endif /* _FLASH_INTERF_DRIVER_H */