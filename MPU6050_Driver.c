#include "MPU6050_Driver.h"
#include "Type.h"

static struct{
  int16 x,y,z;
} gyro_xyz_offset;

static struct{
  int16 x,y,z;
} accel_xyz_offset;

static gyro_union gyro_local;
static accel_union accel_local;
static temp_union temp_local;
static uint8 data_m;
static boolean hasData;

void MPU6050_Init()
{
  /* check comm with MPU6050 */
  I2C1_WriteByte(MPU6050_ADDR, MPU6050_WHO_AM_I);
  do{
    I2C1_Tick();
  } while(I2C1_IsBusy());
  uint8 check = 0x00;
  if(TRUE == I2C1_ReadByte(MPU6050_ADDR, &MPU6050_Handler))
  {
    while(!hasData)
    {
      I2C1_Tick();
    }
    hasData = FALSE;
    check = data_m;
  }
  if(MPU6050_ADDR != check)
  {
    /* error state ! */
    __asm("BKPT #1");
  }
  
  hasData = FALSE;
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_PWR_MGMT_1, 0x00);
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_PWR_MGMT_2, 0x00);
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_SMPRT_DIV, 9);
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_CONFIG, 0x02);
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_GYRO_CONFIG, 0x18);
  I2C1_WriteReg(MPU6050_ADDR, MPU6050_ACCEL_CONFIG, 0x08);
  
  calibrate_gyro();
  //calibrate_accel();
  return;
}

static void calibrate_gyro()
{
  int32 x,y,z;
  x = 0;
  y = 0;
  z = 0;
  for (uint8 i=0; i < MPU6050_CALIBRATION_SAMPLES; i++)
  {
    I2C1_WriteByte(MPU6050_ADDR, MPU6050_GYRO_XOUT_H);
    I2C1_ReadByteStream(MPU6050_ADDR, 6, &MPU6050_Handler);
    while(!hasData)
    {
      I2C1_Tick();
    }
    hasData = FALSE;
    x += gyro_local.xyz_data.x;
    y += gyro_local.xyz_data.y;
    z += gyro_local.xyz_data.z;
  }
  gyro_xyz_offset.x = x / MPU6050_CALIBRATION_SAMPLES;
  gyro_xyz_offset.y = y / MPU6050_CALIBRATION_SAMPLES;
  gyro_xyz_offset.z = z / MPU6050_CALIBRATION_SAMPLES;
  return;
}

static void calibrate_accel()
{
  int32 x,y,z;
  x = 0;
  y = 0;
  z = 0;
  for (uint8 i=0; i < MPU6050_CALIBRATION_SAMPLES; i++)
  {
    I2C1_WriteByte(MPU6050_ADDR, MPU6050_ACCEL_XOUT_H);
    I2C1_ReadByteStream(MPU6050_ADDR, 6, &MPU6050_Handler);
    while(!hasData)
    {
      I2C1_Tick();
    }
    hasData = FALSE;
    x += accel_local.xyz_data.x;
    y += accel_local.xyz_data.y;
    z += accel_local.xyz_data.z;
  }
  accel_xyz_offset.x = x / MPU6050_CALIBRATION_SAMPLES;
  accel_xyz_offset.y = y / MPU6050_CALIBRATION_SAMPLES;
  accel_xyz_offset.z = z / MPU6050_CALIBRATION_SAMPLES;
  return;
}

void MPU6050_IssueRead()
{
  if(!I2C1_ReserveReq(2))
    return;
  if(FALSE == I2C1_WriteByte(MPU6050_ADDR, MPU6050_ACCEL_XOUT_H))
    return;
  if(FALSE == I2C1_ReadByteStream(MPU6050_ADDR, 14, &MPU6050_Handler))
    return;
  return;
}

void MPU6050_ReadGyro(gyro_type *gyro_data)
{
  if(hasData)
  {
    hasData = FALSE;
    
    gyro_data->x = (gyro_local.xyz_data.x - gyro_xyz_offset.x) / (float32)MPU6050_GYRO_SCALE_FACTOR;
    gyro_data->y = (gyro_local.xyz_data.y - gyro_xyz_offset.y) / (float32)MPU6050_GYRO_SCALE_FACTOR;
    gyro_data->z = (gyro_local.xyz_data.z - gyro_xyz_offset.z) / (float32)MPU6050_GYRO_SCALE_FACTOR;
  }
  else
  {
    USART6_SendData("ADXL345 Comm Fault\n\r");
  }
  return;
}

void MPU6050_ReadAccel(accel_type *accel_data)
{
  if(hasData)
  {
    hasData = FALSE;
    
    accel_data->x = (accel_local.xyz_data.x - accel_xyz_offset.x) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
    accel_data->y = (accel_local.xyz_data.y - accel_xyz_offset.y) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
    accel_data->z = (accel_local.xyz_data.z - accel_xyz_offset.z) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
  }
  else
  {
    USART6_SendData("ADXL345 Comm Fault\n\r");
  }
  return;
}

void MPU6050_ReadAccel_Gyro(accel_type *accel_data, gyro_type *gyro_data)
{
  if(hasData)
  {
    hasData = FALSE;
    
    /* compute sensor temperature in degrees Celsius */
    float32 temperature = (temp_local.temp / 340.0) + 36.53;
    
    accel_data->x = (accel_local.xyz_data.x - accel_xyz_offset.x) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
    accel_data->y = (accel_local.xyz_data.y - accel_xyz_offset.y) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
    accel_data->z = (accel_local.xyz_data.z - accel_xyz_offset.z) / (float32)MPU6050_ACCEL_SCALE_FACTOR;
    
    gyro_data->x = (gyro_local.xyz_data.x - gyro_xyz_offset.x) / (float32)MPU6050_GYRO_SCALE_FACTOR;
    gyro_data->y = (gyro_local.xyz_data.y - gyro_xyz_offset.y) / (float32)MPU6050_GYRO_SCALE_FACTOR;
    gyro_data->z = (gyro_local.xyz_data.z - gyro_xyz_offset.z) / (float32)MPU6050_GYRO_SCALE_FACTOR;
  }
  else
  {
    USART6_SendData("ADXL345 Comm Fault\n\r");
  }
  return;
}

void MPU6050_Handler(uint8 *data, uint8 data_size)
{
  if(14 == data_size)
  {
    /* copy accelerometer data */
    for(uint8 i=0; i < 6; i+=2)
    {
      accel_local.data[i] = data[i+1];
      accel_local.data[i+1] = data[i];
    }
    
    /* copy temperature data */
    temp_local.data[0] = data[7];
    temp_local.data[1] = data[6];
    
    /* copy gyroscope data */
    for(uint8 i=8; i < data_size; i+=2)
    {
      gyro_local.data[i - 8] = data[i+1];
      gyro_local.data[i - 7] = data[i];
    }
  }
  else if(6 == data_size)
  {
    for(uint8 i=0; i < data_size; i+=2)
    {
      accel_local.data[i] = data[i+1];
      accel_local.data[i+1] = data[i];
      gyro_local.data[i] = data[i+1];
      gyro_local.data[i+1] = data[i];
    }
  }
  else if(1 == data_size)
  {
    data_m = data[0];
  }
  hasData = TRUE;
  return;
}