#include "ADXL345_Driver.h"

static accel_union accel_local;
static accel_type accel_lp_local;
static boolean hasData;

static struct{
  int16 x,y,z;
} xyz_offset;

static const float low_pass_const = 0.2;

void ADXL345_Init()
{
  hasData = FALSE;
  I2C1_WriteReg(ADXL345_ADDR, BW_RATE_REG, 0x1A);
  I2C1_WriteReg(ADXL345_ADDR, POWER_CTRL_REG, 0x08);
  I2C1_WriteReg(ADXL345_ADDR, DATA_FORMAT_REG, 0x0B);
  
  accel_lp_local.x = accel_lp_local.y = accel_lp_local.z = 0.0;
  calibrate_accel();
  
  return;
}

static void calibrate_accel()
{
  int32 x,y,z;
  x = 0;
  y = 0;
  z = 0;
  for (uint8 i=0; i < ADXL345_CALIBRATION_SAMPLES; i++)
  {
    I2C1_WriteByte(ADXL345_ADDR, DATAX0_REG);
    I2C1_ReadByteStream(ADXL345_ADDR, 6, &ADXL345_Handle);
    while(!hasData)
    {
      I2C1_Tick();
    }
    hasData = FALSE;
    x += accel_local.xyz_data.x;
    y += accel_local.xyz_data.y;
    z += accel_local.xyz_data.z;
  }
  xyz_offset.x = (x / CALIBRATION_SAMPLES);
  xyz_offset.y = (y / CALIBRATION_SAMPLES);
  xyz_offset.z = (z / CALIBRATION_SAMPLES) - 256; // considered to be -1g after scaling
  
  return;
}

void ADXL345_IssueRead()
{
  if(!I2C1_ReserveReq(2))
    return;
  if(FALSE == I2C1_WriteByte(ADXL345_ADDR, DATAX0_REG))
    return;
  if(FALSE == I2C1_ReadByteStream(ADXL345_ADDR, 6, &ADXL345_Handle))
    return;
}

void ADXL345_ReadAccel(accel_type *accel_data)
{
  if(hasData)
  {
    hasData = FALSE;
    /*
    accel_lp_local.x = accel_lp_local.x - (low_pass_const * (accel_lp_local.x - ((float)(accel_local.xyz_data.x - xyz_offset.x) * ADXL345_SCALE_FACTOR)));
    accel_lp_local.y = accel_lp_local.y - (low_pass_const * (accel_lp_local.y - ((float)(accel_local.xyz_data.y - xyz_offset.y) * ADXL345_SCALE_FACTOR)));
    accel_lp_local.z = accel_lp_local.z - (low_pass_const * (accel_lp_local.z - ((float)(accel_local.xyz_data.z - xyz_offset.z) * ADXL345_SCALE_FACTOR)));
    
    accel_data->x = accel_lp_local.x;
    accel_data->y = accel_lp_local.y;
    accel_data->z = accel_lp_local.z;
    */
    accel_data->x = (accel_local.xyz_data.x - xyz_offset.x) * ADXL345_SCALE_FACTOR;
    accel_data->y = (accel_local.xyz_data.y - xyz_offset.y) * ADXL345_SCALE_FACTOR;
    accel_data->z = (accel_local.xyz_data.z - xyz_offset.z) * ADXL345_SCALE_FACTOR;
    
  }
  else
  {
    USART6_SendData("ADXL345 Comm Fault\n\r");
  }
  
  
  return;
}

void ADXL345_Handle(uint8 *data, uint8 data_size)
{
  for(uint8 i=0; i<data_size; i++)
  {
    accel_local.data[i] = data[i];
  }
  hasData = TRUE;
  return;
}