#ifndef _ESP8266_CONFIG_H
#define _ESP8266_CONFIG_H


#define COMMAND_BUFFER_SIZE     50
#define DATA_BUFFER_SIZE        50

#define ESP_InitCommDelay       8500
#define ESP_WaitClientDelay     2500
#define ESP_RxDelay             60

#define RECEIVE_DATA_END        '~'

// when setting AP, the order of the parameters are: SSID, pwd, chn, enc
#define WIFI_MODE       "2"       // 1 - Station, 2 - AccessPoint, 3 - Both
#define SSID            "\"Drone\""     // AccessPoint's name
#define PASSWORD        "\"pass\""     // AccessPoint's password
#define WIFI_CHANNEL    "6"       // WiFi Channel number
#define WIFI_ENCRYPTION "0"       // With or withouth password (1 may be faulty!)

#define WIFI_MULTIPLE_CONNECTION  "1"

#define WIFI_SERVER_ON          "1"
#define WIFI_SERVER_OFF         "0"

#define WIFI_SERVER_PORT        "8001"

#endif /* _ESP8266_CONFIG_H */