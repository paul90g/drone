#include "DMA1_Driver.h"
#include "Type.h"
#include "Drivers.h"

void DMA1_Init(){
  Enable_DMA1_Clk();
  
  DMA1_Stream0_Channel(1);
  DMA1_Stream0_Priority(0);
  DMA1_Stream0_MemPointerIncr();
  DMA1_Stream0_DataDirection(PERIPH_TO_MEM);
  DMA1_Stream0_PeriphSize(BYTE_SIZE);
  DMA1_Stream0_PeriphAddress((uint32)&I2C1->DR);
  DMA1_Stream0_MemSize(BYTE_SIZE);
  DMA1_ClearStream0Flags();
  DMA1_Stream0_TransfCompleteIntEn();
  
  return;
}