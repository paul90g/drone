/* startup code */
#include "type.h"

extern int CSTACK$$Limit;
void __iar_program_start(void);
void Unused_Handler(void);
void NMI_Handler(void);
void HardFault_Handler(void);          /* Hard Fault Handler */
void MemManage_Handler(void);          /* MPU Fault Handler */
void BusFault_Handler(void);           /* Bus Fault Handler */
void UsageFault_Handler(void);         /* Usage Fault Handler */
void SVC_Handler(void);                /* SVCall Handler */
void DebugMon_Handler(void);           /* Debug Monitor Handler */
void PendSV_Handler(void);             /* PendSV Handler */
void SysTick_Handler(void);            /* SysTick Handler */
void WWDG_IRQHandler(void);
void PVD_IRQHandler(void);
void TAMPER_IRQHandler(void);
void RTC_IRQHandler(void);
void FLASH_IRQHandler(void);
void RCC_IRQHandler(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI4_IRQHandler(void);
void DMA1_Stream0_IRQHandler(void);
void DMA1_Stream1_IRQHandler(void);
void DMA1_Stream2_IRQHandler(void);
void DMA1_Stream3_IRQHandler(void);
void DMA1_Stream4_IRQHandler(void);
void DMA1_Stream5_IRQHandler(void);
void DMA1_Stream6_IRQHandler(void);
void ADC1_2_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void TIM1_BRK_IRQHandler(void);
void TIM1_UP_IRQHandler(void);
void TIM1_TRG_COM_IRQHandler(void);
void TIM1_CC_IRQHandler(void);
void TIM2_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
void I2C1_EV_IRQHandler(void);
void I2C1_ER_IRQHandler(void);
void I2C2_EV_IRQHandler(void);
void I2C2_ER_IRQHandler(void);
void SPI1_IRQHandler(void);
void SPI2_IRQHandler(void);
void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void RTCAlarm_IRQHandler(void);
void OTG_FS_WKUP_IRQHandler(void);
void DMA1_Stream7_IRQHandler(void);
void SDIO_IRQHandler(void);
void TIM5_IRQHandler(void);
void SPI3_IRQHandler(void);
void DMA2_Stream0_IRQHandler(void);
void DMA2_Stream1_IRQHandler(void);
void DMA2_Stream2_IRQHandler(void);
void DMA2_Stream3_IRQHandler(void);
void DMA2_Stream4_IRQHandler(void);
void OTG_FS_IRQHandler(void);
void DMA2_Stream5_IRQHandler(void);
void DMA2_Stream6_IRQHandler(void);
void DMA2_Stream7_IRQHandler(void);
void USART6_IRQHandler(void);
void I2C3_EV_IRQHandler(void);
void I2C3_ER_IRQHandler(void);
void FPU_IRQHandler(void);
void SPI4_IRQHandler(void);
void SPI5_IRQHandler(void);


#pragma weak SVC_Handler                = Unused_Handler
#pragma weak DebugMon_Handler           = Unused_Handler
#pragma weak PendSV_Handler             = Unused_Handler
#pragma weak SysTick_Handler            = Unused_Handler
#pragma weak WWDG_IRQHandler            = Unused_Handler
#pragma weak PVD_IRQHandler             = Unused_Handler
#pragma weak TAMPER_IRQHandler          = Unused_Handler
#pragma weak RTC_IRQHandler             = Unused_Handler
#pragma weak FLASH_IRQHandler           = Unused_Handler
#pragma weak RCC_IRQHandler             = Unused_Handler
#pragma weak EXTI0_IRQHandler           = Unused_Handler
#pragma weak EXTI1_IRQHandler           = Unused_Handler
#pragma weak EXTI2_IRQHandler           = Unused_Handler
#pragma weak EXTI3_IRQHandler           = Unused_Handler
#pragma weak EXTI4_IRQHandler           = Unused_Handler
#pragma weak DMA1_Stream0_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream1_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream2_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream3_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream4_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream5_IRQHandler    = Unused_Handler
#pragma weak DMA1_Stream6_IRQHandler    = Unused_Handler
#pragma weak ADC1_2_IRQHandler          = Unused_Handler
#pragma weak EXTI9_5_IRQHandler         = Unused_Handler
#pragma weak TIM1_BRK_IRQHandler        = Unused_Handler
#pragma weak TIM1_UP_IRQHandler         = Unused_Handler
#pragma weak TIM1_TRG_COM_IRQHandler    = Unused_Handler
#pragma weak TIM1_CC_IRQHandler         = Unused_Handler
#pragma weak TIM2_IRQHandler            = Unused_Handler
#pragma weak TIM3_IRQHandler            = Unused_Handler
#pragma weak TIM4_IRQHandler            = Unused_Handler
#pragma weak I2C1_EV_IRQHandler         = Unused_Handler
#pragma weak I2C1_ER_IRQHandler         = Unused_Handler
#pragma weak I2C2_EV_IRQHandler         = Unused_Handler
#pragma weak I2C2_ER_IRQHandler         = Unused_Handler
#pragma weak SPI1_IRQHandler            = Unused_Handler
#pragma weak SPI2_IRQHandler            = Unused_Handler
#pragma weak USART1_IRQHandler          = Unused_Handler
#pragma weak USART2_IRQHandler          = Unused_Handler
#pragma weak EXTI15_10_IRQHandler       = Unused_Handler
#pragma weak RTCAlarm_IRQHandler        = Unused_Handler
#pragma weak OTG_FS_WKUP_IRQHandler     = Unused_Handler
#pragma weak DMA1_Stream7_IRQHandler    = Unused_Handler
#pragma weak SDIO_IRQHandler            = Unused_Handler
#pragma weak TIM5_IRQHandler            = Unused_Handler
#pragma weak SPI3_IRQHandler            = Unused_Handler
#pragma weak DMA2_Stream0_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream1_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream2_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream3_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream4_IRQHandler    = Unused_Handler
#pragma weak OTG_FS_IRQHandler          = Unused_Handler
#pragma weak DMA2_Stream5_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream6_IRQHandler    = Unused_Handler
#pragma weak DMA2_Stream7_IRQHandler    = Unused_Handler
#pragma weak USART6_IRQHandler          = Unused_Handler
#pragma weak I2C3_EV_IRQHandler         = Unused_Handler
#pragma weak I2C3_ER_IRQHandler         = Unused_Handler
#pragma weak FPU_IRQHandler             = Unused_Handler
#pragma weak SPI4_IRQHandler            = Unused_Handler
#pragma weak SPI5_IRQHandler            = Unused_Handler

const uint32 __vector_table[] @ ".intvec" = {
    (int)&CSTACK$$Limit,                /* The stack pointer after relocation */
    (int)&__iar_program_start,          /* Reset Handler */
    (int)&NMI_Handler,                  /* NMI Handler */
    (int)&HardFault_Handler,            /* Hard Fault Handler */
    (int)&MemManage_Handler,            /* MPU Fault Handler */
    (int)&BusFault_Handler,             /* Bus Fault Handler */
    (int)&UsageFault_Handler,           /* Usage Fault Handler */
    0,                                  /* Reserved */
    0,                                  /* Reserved */
    0,                                  /* Reserved */
    0,                                  /* Reserved */
    (int)&SVC_Handler,                  /* SVCall Handler */
    (int)&DebugMon_Handler,             /* Debug Monitor Handler */
    0,                                  /* Reserved */
    (int)&PendSV_Handler,               /* PendSV Handler */
    (int)&SysTick_Handler,              /* SysTick Handler */
    /* External Interrupts */
    (int)&WWDG_IRQHandler,            /* Window Watchdog */
    (int)&PVD_IRQHandler,             /* PVD through EXTI Line detect */
    (int)&TAMPER_IRQHandler,          /* Tamper */
    (int)&RTC_IRQHandler,             /* RTC */
    (int)&FLASH_IRQHandler,           /* Flash */
    (int)&RCC_IRQHandler,             /* RCC */
    (int)&EXTI0_IRQHandler,           /* EXTI Line 0 */
    (int)&EXTI1_IRQHandler,           /* EXTI Line 1 */
    (int)&EXTI2_IRQHandler,           /* EXTI Line 2 */
    (int)&EXTI3_IRQHandler,           /* EXTI Line 3 */
    (int)&EXTI4_IRQHandler,           /* EXTI Line 4 */
    (int)&DMA1_Stream0_IRQHandler,   /* DMA1 Stream 1 */
    (int)&DMA1_Stream1_IRQHandler,   /* DMA1 Stream 2 */
    (int)&DMA1_Stream2_IRQHandler,   /* DMA1 Stream 3 */
    (int)&DMA1_Stream3_IRQHandler,   /* DMA1 Stream 4 */
    (int)&DMA1_Stream4_IRQHandler,   /* DMA1 Stream 5 */
    (int)&DMA1_Stream5_IRQHandler,   /* DMA1 Stream 6 */
    (int)&DMA1_Stream6_IRQHandler,   /* DMA1 Stream 7 */
    (int)&ADC1_2_IRQHandler,          /* ADC1 & ADC2 */
    0,
    0,
    0,
    0,
    (int)&EXTI9_5_IRQHandler,         /* EXTI Line 9..5 */
    (int)&TIM1_BRK_IRQHandler,        /* TIM1 Break */
    (int)&TIM1_UP_IRQHandler,         /* TIM1 Update */
    (int)&TIM1_TRG_COM_IRQHandler,    /* TIM1 Trigger and Commutation */
    (int)&TIM1_CC_IRQHandler,         /* TIM1 Capture Compare */
    (int)&TIM2_IRQHandler,            /* TIM2 */
    (int)&TIM3_IRQHandler,            /* TIM3 */
    (int)&TIM4_IRQHandler,            /* TIM4 */
    (int)&I2C1_EV_IRQHandler,         /* I2C1 Event */
    (int)&I2C1_ER_IRQHandler,         /* I2C1 Error */
    (int)&I2C2_EV_IRQHandler,         /* I2C2 Event */
    (int)&I2C2_ER_IRQHandler,         /* I2C2 Error */
    (int)&SPI1_IRQHandler,            /* SPI1 */
    (int)&SPI2_IRQHandler,            /* SPI2 */
    (int)&USART1_IRQHandler,          /* USART1 */
    (int)&USART2_IRQHandler,          /* USART2 */
    0,
    (int)&EXTI15_10_IRQHandler,       /* EXTI Line 15..10 */
    (int)&RTCAlarm_IRQHandler,        /* RTC Alarm through EXTI Line */
    (int)&OTG_FS_WKUP_IRQHandler,     /* USB Wakeup from suspend */
    0,
    0,
    0,
    0,
    (int)&DMA1_Stream7_IRQHandler,
    0,
    (int)&SDIO_IRQHandler,
    (int)&TIM5_IRQHandler,
    (int)&SPI3_IRQHandler,
    0,
    0,
    0,
    0,
    (int)&DMA2_Stream0_IRQHandler,
    (int)&DMA2_Stream1_IRQHandler,
    (int)&DMA2_Stream2_IRQHandler,
    (int)&DMA2_Stream3_IRQHandler,
    (int)&DMA2_Stream4_IRQHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    (int)&OTG_FS_IRQHandler,
    (int)&DMA2_Stream5_IRQHandler,
    (int)&DMA2_Stream6_IRQHandler,
    (int)&DMA2_Stream7_IRQHandler,
    (int)&USART6_IRQHandler,
    (int)&I2C3_EV_IRQHandler,
    (int)&I2C3_ER_IRQHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    (int)&FPU_IRQHandler,
    0,
    0,
    (int)&SPI4_IRQHandler,
    (int)&SPI5_IRQHandler
};


__stackless void NMI_Handler(){
  while(1){
  }
}

__stackless void HardFault_Handler(){
  __iar_program_start();
}

__stackless void MemManage_Handler(){
  while(1){
  }
}

__stackless void BusFault_Handler(){
  __iar_program_start();
}

__stackless void UsageFault_Handler(){
  while(1){
  }
}

__stackless void Unused_Handler(){
  while(1){
  }
}