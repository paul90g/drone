#ifndef _MEM_MAP_H
#define _MEM_MAP_H

#include "type.h"
#include "config.h"


/***********************/
/*   APB1 Memory Map   */
/***********************/
#define APB1_BASE               ((uint32) 0x40000000)
#define TIM2_BASE               ((uint32)(APB1_BASE + 0x0000))
#define TIM3_BASE               ((uint32)(APB1_BASE + 0x0400))
#define TIM4_BASE               ((uint32)(APB1_BASE + 0x0800))
#define TIM5_BASE               ((uint32)(APB1_BASE + 0x0C00))
#define RTC_BKP_REG_BASE        ((uint32)(APB1_BASE + 0x2800))
#define WWDG_BASE               ((uint32)(APB1_BASE + 0x2C00))
#define IWDG_BASE               ((uint32)(APB1_BASE + 0x3000))
#define I2S2ext_BASE            ((uint32)(APB1_BASE + 0x3400))
#define SPI2_I2S2_BASE          ((uint32)(APB1_BASE + 0x3800))
#define SPI3_I2S3_BASE          ((uint32)(APB1_BASE + 0x3C00))
#define I2S3ext_BASE            ((uint32)(APB1_BASE + 0x4000))
#define USART2_BASE             ((uint32)(APB1_BASE + 0x4400))
#define I2C1_BASE               ((uint32)(APB1_BASE + 0x5400))
#define I2C2_BASE               ((uint32)(APB1_BASE + 0x5800))
#define I2C3_BASE               ((uint32)(APB1_BASE + 0x5C00))
#define PWR_BASE                ((uint32)(APB1_BASE + 0x7000))
/***********************/
/* APB1 Memory Map end */
/***********************/


/***********************/
/*   APB2 Memory Map   */
/***********************/
#define APB2_BASE               ((uint32) 0x40010000)
#define TIM1_BASE               ((uint32)(APB2_BASE + 0x0000))
#define USART1_BASE             ((uint32)(APB2_BASE + 0x1000))
#define USART6_BASE             ((uint32)(APB2_BASE + 0x1400))
#define ADC1_BASE               ((uint32)(APB2_BASE + 0x2000))
#define SDIO_BASE               ((uint32)(APB2_BASE + 0x2C00))
#define SPI1_BASE               ((uint32)(APB2_BASE + 0x3000))
#define SPI4_BASE               ((uint32)(APB2_BASE + 0x3400))
#define SYSCFG_BASE             ((uint32)(APB2_BASE + 0x3800))
#define EXTI_BASE               ((uint32)(APB2_BASE + 0x3C00))
#define TIM9_BASE               ((uint32)(APB2_BASE + 0x4000))
#define TIM10_BASE              ((uint32)(APB2_BASE + 0x4400))
#define TIM11_BASE              ((uint32)(APB2_BASE + 0x4800))
#define SPI5_BASE               ((uint32)(APB2_BASE + 0x5000))
/***********************/
/* APB2 Memory Map end */
/***********************/


/***********************/
/*   AHB1 Memory Map   */
/***********************/
#define AHB1_BASE               ((uint32) 0x40020000)
#define GPIOA_BASE              ((uint32)(AHB1_BASE + 0x0000))
#define GPIOB_BASE              ((uint32)(AHB1_BASE + 0x0400))
#define GPIOC_BASE              ((uint32)(AHB1_BASE + 0x0800))
#define GPIOD_BASE              ((uint32)(AHB1_BASE + 0x0C00))
#define GPIOE_BASE              ((uint32)(AHB1_BASE + 0x1000))
#define GPIOH_BASE              ((uint32)(AHB1_BASE + 0x1C00))
#define CRC_BASE                ((uint32)(AHB1_BASE + 0x3000))
#define RCC_BASE                ((uint32)(AHB1_BASE + 0x3800))
#define FLASH_INTF_BASE         ((uint32)(AHB1_BASE + 0x3C00))
#define DMA1_BASE               ((uint32)(AHB1_BASE + 0x6000))
#define DMA2_BASE               ((uint32)(AHB1_BASE + 0x6400))
/***********************/
/* AHB1 Memory Map end */
/***********************/


/***********************/
/*   AHB2 Memory Map   */
/***********************/
#define AHB2_BASE               ((uint32) 0x50000000)
#define USB_OTG_FS_BASE         ((uint32)(AHB2_BASE + 0x00000))
/***********************/
/* AHB2 Memory Map end */
/***********************/


/*******************************************/
/* PPB Memory Map (Private Peripheral Bus) */
/*******************************************/
#define PPB_BASE                ((uint32) 0xE0000000)
#define SYSTICK_BASE            ((uint32)(PPB_BASE + 0xE010))
#define NVIC_BASE               ((uint32)(PPB_BASE + 0xE100))
#define SCB_BASE                ((uint32)(PPB_BASE + 0xED00))
#define SCB_BASE                ((uint32)(PPB_BASE + 0xED00))
#define FPU_COP_BASE            ((uint32)(PPB_BASE + 0xED88))
#define MPU_BASE                ((uint32)(PPB_BASE + 0xED90))
#define NVIC_STIR_BASE          ((uint32)(PPB_BASE + 0xEF00))
#define FPU_BASE                ((uint32)(PPB_BASE + 0xEF30))
/*******************************************/
/*           PPB Memory Map end            */
/*******************************************/


/******************************/
/* Debug Registers Memory Map */
/******************************/
#define DBGMCU_BASE             ((uint32) 0xE0042000)
/******************************/
/*    Debug Registers end     */
/******************************/


#endif /* _MEM_MAP_H */