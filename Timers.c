#include "Timers.h"

extern volatile uint32 sysTimer;

void init32bitTimer(Timer32bit* timer){
  (*timer).timerState = Timer_Reset;
  return;
}
void init16bitTimer(Timer16bit* timer){
  (*timer).timerState = Timer_Reset;
  return;
}

void start32bitTimer(Timer32bit* timer, uint32 timeout){
  (*timer).timerState = Timer_Running;
  (*timer).timerVal = timeout + sysTimer;
  return;
}

void start16bitTimer(Timer16bit* timer, uint16 timeout){
  (*timer).timerState = Timer_Running;
  (*timer).timerVal = (uint16)((uint32)timeout + sysTimer);
  return;
}

boolean is16bitTimerPassed(Timer16bit* timer){
  if(Timer_Passed == timer->timerState)
    return TRUE;
  else if((*timer).timerVal <= sysTimer){
    (*timer).timerState = Timer_Passed;
    return TRUE;
  } else 
    return FALSE;
}

boolean is32bitTimerPassed(Timer32bit* timer){
  if(Timer_Passed == timer->timerState)
    return TRUE;
  else if((*timer).timerVal <= sysTimer){
    (*timer).timerState = Timer_Passed;
    return TRUE;
  } else 
    return FALSE;
}

TimerType getStateNoStart16bitTimer(Timer16bit *timer){
  if((*timer).timerVal <= sysTimer)
    (*timer).timerState = Timer_Passed;
  return (*timer).timerState;
}

TimerType getStateNoStart32bitTimer(Timer32bit *timer){
  if((*timer).timerVal <= sysTimer)
    (*timer).timerState = Timer_Passed;
  return (*timer).timerState;
}

TimerType getState32bitTimer(Timer32bit* timer, uint32 timeout){
  if(Timer_Reset == (*timer).timerState){
    (*timer).timerVal = timeout + sysTimer;
    (*timer).timerState = Timer_Running;
  }
  if((*timer).timerVal <= sysTimer){
    (*timer).timerState = Timer_Passed;
  }
  return (*timer).timerState;
}

TimerType getState16bitTimer(Timer16bit* timer, uint16 timeout){
  if(Timer_Reset == (*timer).timerState){
    (*timer).timerVal = (uint16)((uint32)timeout + sysTimer);
    (*timer).timerState = Timer_Running;
  }
  if((*timer).timerVal <= sysTimer){
    (*timer).timerState = Timer_Passed;
  }
  return (*timer).timerState;
}