#include "Drivers.h"
#include "GPIOC_Driver.h"

void GPIOC_Init(){
  Enable_GPIOC_Clk();

  // enable USART6
  GPIOC_PinMode(PIN6, PIN_MODE_ALT_FUNC); // TX
  GPIOC_Pin_SetAltFunc(PIN6, AF8);
  GPIOC_Pin_Speed(PIN6, HIGH_SPEED);
  GPIOC_PinMode(PIN7, PIN_MODE_ALT_FUNC); // RX
  GPIOC_Pin_SetAltFunc(PIN7, AF8);
  GPIOC_Pin_Speed(PIN7, HIGH_SPEED);
  
  return;
}