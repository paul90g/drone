#ifndef _I2C_DRIVER_TYPE_H
#define _I2C_DRIVER_TYPE_H

#include "Type.h"

typedef struct
{
  uint32 CR1;        /*!< I2C Control register 1,     Address offset: 0x00 */
  uint32 CR2;        /*!< I2C Control register 2,     Address offset: 0x04 */
  uint32 OAR1;       /*!< I2C Own address register 1, Address offset: 0x08 */
  uint32 OAR2;       /*!< I2C Own address register 2, Address offset: 0x0C */
  uint32 DR;         /*!< I2C Data register,          Address offset: 0x10 */
  uint32 SR1;        /*!< I2C Status register 1,      Address offset: 0x14 */
  uint32 SR2;        /*!< I2C Status register 2,      Address offset: 0x18 */
  uint32 CCR;        /*!< I2C Clock control register, Address offset: 0x1C */
  uint32 TRISE;      /*!< I2C TRISE register,         Address offset: 0x20 */
  uint32 FLTR;       /*!< I2C FLTR register,          Address offset: 0x24 */
} I2C_TypeDef;


/* CR1 */
#define NOSTRETCH_BIT   (1<<7)
#define START_BIT       (1<<8)
#define STOP_BIT        (1<<9)
#define ACK_BIT         (1<<10)
#define I2C_PE_BIT      (1<<0)          // Peripheral Enable
/********/

/* CR2 */
#define LAST_BIT        (1<<12)
#define DMAEN_BIT       (1<<11)
#define ITBUFEN_BIT     (1<<10)         // Buffer Interrupt Enable
#define ITEVTEN_BIT     (1<<9)          // Event Interrupt Enable
#define ITERREN_BIT     (1<<8)          // Error Interrupt Enable
#define FREQ_BITS       (1<<0)          // Peripheral clock frequency, 6 bits
/*******/

/* SR1 */
#define I2C_TXE_BIT     (1<<7)
#define I2C_RXNE_BIT    (1<<6)
#define STOPF_BIT       (1<<4)
#define I2C_ADDR_BIT    (1<<1)
#define I2C_SB_BIT      (1<<0)
/*******/

/* CCR */
#define FS_BIT          (1<<15)         // FastMode / SlowMode
#define DUTY_BIT        (1<<14)         // Fm duty cycle
#define CCR_BITS        (1<<0)          // Clock Control Reg, 12 bits
/*******/

/* OAR1 */
#define ADDMODE_10BIT   0x11
#define ADDMODE_7BIT    0x01
#define ADDMODE_BITMASK 11
#define ADDMODE_BITS    (1<<14)         // bit 0 is dummy value, always set to 1
/********/

/* FLTR */
#define ANOFF_BIT       (1<<4)          // Analog filter
/********/

#define RequestRead7BitAddr(ADDR)       ((ADDR<<1) | 0x01)
#define RequestWrite7BitAddr(ADDR)      (ADDR<<1)

#endif /* _I2C_DRIVER_TYPE_H */