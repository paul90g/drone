#include "String.h"
#include "Type.h"

uint16 strlen(const uint8 *string){
  uint16 len = 0;
  while(NULL_CHAR != string[len]){
    len++;
  }
  return len;
}


void strcpy(uint8 *dest, const uint8 *source){
  uint8 it = 0;
  while('\0' != source[it]){
    dest[it] = source[it];
    it++;
  }
  dest[it] = '\0';
  return;
}

void strncpy(uint8 *dest, const uint8 *source, const uint16 size){
  uint8 it = 0;
  while('\0' != source[it]){
    if (it < size)
    {
      dest[it] = source[it];
      it++;
    }
  }
  dest[it] = '\0';
  return;
}


uint8 *strstr(const uint8 *haystack, const uint8 *needle){
  uint8 *cp = (uint8*)haystack;
  uint8 *h, *n;
  if((const uint8*)NULL_PTR == needle)
    return (uint8*)haystack;
  while('\0' != *cp){
    h = cp;
    n = (uint8*)needle;
    while(('\0' != *h) && ('\0' != *n) && 0 == (*h - *n)){
      h++;n++;
    }
    if('\0' == *n)
      return cp;
    cp++;
  }
  return (uint8*)NULL_PTR;
}


uint8 *strncat(uint8 *str1, const uint16 destSize, const uint8 *str2){
  uint32 i=0, j=0;
  uint8 *dest = str1;
  while(NULL_CHAR != str1[i]){
    if(i < destSize)
      i++;
  }
  while(NULL_CHAR != str2[j]){
    if(i < destSize){
      str1[i]=str2[j];
      i++;j++;
    }
  }
  if(i < destSize)
    str1[i]=str2[j];
  return dest;
}


int8 strcmp(const uint8 *str1, const uint8 *str2){
  int8 it = 0;
  while('\0' != str1[it]){
    if('\0' == str2[it])
      return 1;
    else if(str1[it] < str2[it])
      return -1;
    else if(str1[it] > str2[it])
      return 1;
    it++;
  }
  return 0;
}

void itoa(uint8 *str, int32 nr)
{
  int16 i = 0;
  uint8 isNegative = FALSE;
  
  if (nr == 0)
  {
    str[i++] = '0';
    str[i] = '\0';
    return;
  }
  if (nr < 0)
  {
    isNegative = TRUE;
    nr = -nr;
  }
  while (nr != 0)
  {
    int32 rem = nr % 10;
    str[i++] = rem + '0';
    nr /= 10;
  }
  if (isNegative)
    str[i++] = '-';
  str[i] = '\0';
  reverse(str);
  return;
}

void ftoa(uint8 *str, float nr)
{
  union {
    float f;
    struct{
      unsigned int    mantissa_lo : 16;
      unsigned int    mantissa_hi : 7;    
      unsigned int     exponent : 8;
      unsigned int     sign : 1;
    };
  } helper;
  
  unsigned long mantissa;
  signed char exponent;
  unsigned int int_part;
  char frac_part[3];
  int i, count = 0;
  
  helper.f = nr;
  //mantissa is LS 23 bits
  mantissa = helper.mantissa_lo;
  mantissa += ((unsigned long) helper.mantissa_hi << 16);
  //add the 24th bit to get 1.mmmm^eeee format
  mantissa += 0x00800000;
  //exponent is biased by 127
  exponent = (signed char) helper.exponent - 127;
  
  //too big to shove into 8 chars
  if (exponent > 18)
  {
    str[0] = 'I';
    str[1] = 'n';
    str[2] = 'f';
    str[3] = '\0';
    return;
  }
  
  //too small to resolve (resolution of 1/8)
  if (exponent < -3)
  {
    str[0] = '0';
    str[1] = '\0';
    return;
  }
  
  count = 0;
  
  //add negative sign (if applicable)
  if (helper.sign)
  {
    str[0] = '-';
    count++;
  }
  
  //get the integer part
  int_part = mantissa >> (23 - exponent);    
  //convert to string
  itoa(&str[count], int_part);
  
  //find the end of the integer
  for (i = 0; i < 8; i++)
    if (str[i] == '\0')
    {
      count = i;
      break;
    }        
  
  //not enough room in the buffer for the frac part    
  if (count > 5)
    return;
  
  //add the decimal point    
  str[count++] = '.';
  
  //use switch to resolve the fractional part
  switch (0x7 & (mantissa  >> (20 - exponent)))
  {
  case 0:
    frac_part[0] = '0';
    frac_part[1] = '0';
    frac_part[2] = '0';
    break;
  case 1:
    frac_part[0] = '1';
    frac_part[1] = '2';
    frac_part[2] = '5';            
    break;
  case 2:
    frac_part[0] = '2';
    frac_part[1] = '5';
    frac_part[2] = '0';            
    break;
  case 3:
    frac_part[0] = '3';
    frac_part[1] = '7';
    frac_part[2] = '5';            
    break;
  case 4:
    frac_part[0] = '5';
    frac_part[1] = '0';
    frac_part[2] = '0';            
    break;
  case 5:
    frac_part[0] = '6';
    frac_part[1] = '2';
    frac_part[2] = '5';            
    break;
  case 6:
    frac_part[0] = '7';
    frac_part[1] = '5';
    frac_part[2] = '0';            
    break;
  case 7:
    frac_part[0] = '8';
    frac_part[1] = '7';
    frac_part[2] = '5';                    
    break;
  }
  
  //add the fractional part to the output string
  for (i = 0; i < 3; i++)
    if (count < 7)
      str[count++] = frac_part[i];
  
  str[count] = '\0';
}

void reverse(uint8 *str)
{
  if (str)
  {
    uint8 *end = str + strlen(str) - 1;
    
#   define XOR_SWAP(a,b) do\
    {\
      a ^= b;\
        b ^= a;\
          a ^= b;\
    } while (0)
      
      while (str < end)
      {
        XOR_SWAP(*str, *end);
        str++;
        end--;
      }
#   undef XOR_SWAP
  }
}