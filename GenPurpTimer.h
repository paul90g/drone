#ifndef _GEN_PURP_TIMER_H
#define _GEN_PURP_TIMER_H

typedef struct {
  uint32 CR1;           /* TIMER Control Register 1             Address offset: 0x00    */
  uint32 CR2;           /* TIMER Control Register 2             Address offset: 0x04    */
  uint32 SMCR;          /* TIMER Slave Mode Control Register    Address offset: 0x08    */
  uint32 DIER;          /* TIMER DMA/Interrupt Enable Register  Address offset: 0x0C    */
  uint32 SR;            /* TIMER Status Register                Address offset: 0x10    */
  uint32 EGR;           /* TIMER Event Generation Register      Address offset: 0x14    */
  uint32 CCMR1;         /* TIMER Capture/Compare Mode Register1 Address offset: 0x18    */
  uint32 CCMR2;         /* TIMER Capture/Compare Mode Register2 Address offset: 0x1C    */
  uint32 CCER;          /* TIMER Capture/Compare Enable Reg     Address offset: 0x20    */
  uint32 CNT;           /* TIMER Counter                        Address offset: 0x24    */
  uint32 PSC;           /* TIMER Prescaler                      Address offset: 0x28    */
  uint32 ARR;           /* TIMER Auto-reload Register           Address offset: 0x2C    */
  uint32 RESERVED1;     /* TIMER reserved                       Address offset: 0x30    */
  uint32 CCR1;          /* TIMER Capture/Compare Register 1     Address offset: 0x34    */
  uint32 CCR2;          /* TIMER Capture/Compare Register 2     Address offset: 0x38    */
  uint32 CCR3;          /* TIMER Capture/Compare Register 3     Address offset: 0x3C    */
  uint32 CCR4;          /* TIMER Capture/Compare Register 4     Address offset: 0x40    */
  uint32 RESERVED2;     /* TIMER reserved                       Address offset: 0x44    */
  uint32 DCR;           /* TIMER DMA Control Register           Address offset: 0x48    */
  uint32 DMAR;          /* TIMER DMA Address for full Transfer  Address offset: 0x4C    */
} GP_TIM_Type;

#endif /* _GEN_PURP_TIMER_H */