#ifndef _DMA1_DRIVER_H
#define _DMA1_DRIVER_H

#include "Type.h"
#include "Drivers.h"
#include "DMA_Type.h"

#define DMA1    ((DMA_Type *)DMA1_BASE)

/* LIFCR */
#define DMA1_ClearStream0Flags()        (DMA1->LIFCR |= STREAM0_CLEAR_BITMASK)
#define DMA1_ClearStream1Flags()        (DMA1->LIFCR |= STREAM1_CLEAR_BITMASK)
#define DMA1_ClearStream2Flags()        (DMA1->LIFCR |= STREAM2_CLEAR_BITMASK)
#define DMA1_ClearStream3Flags()        (DMA1->LIFCR |= STREAM3_CLEAR_BITMASK)

/* HIFCR */
#define DMA1_ClearStream4Flags()        (DMA1->HIFCR |= STREAM4_CLEAR_BITMASK)
#define DMA1_ClearStream5Flags()        (DMA1->HIFCR |= STREAM5_CLEAR_BITMASK)
#define DMA1_ClearStream6Flags()        (DMA1->HIFCR |= STREAM6_CLEAR_BITMASK)
#define DMA1_ClearStream7Flags()        (DMA1->HIFCR |= STREAM7_CLEAR_BITMASK)

/* Stream 0 */
#define DMA1_Stream0_Channel(CH)                {(DMA1->S0CR &= ~(CHSEL_BITMASK)); (DMA1->S0CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream0_Priority(PR_lvl)           {(DMA1->S0CR &= ~(PL_BITMASK)); (DMA1->S0CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream0_MemSize(MEM_size)          {(DMA1->S0CR &= ~(MSIZE_BITMASK)); (DMA1->S0CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream0_PeriphSize(MEM_size)       {(DMA1->S0CR &= ~(PSIZE_BITMASK)); (DMA1->S0CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream0_MemPointerFixed()          (DMA1->S0CR &= ~MINC_BIT)
#define DMA1_Stream0_MemPointerIncr()           (DMA1->S0CR |= MINC_BIT)
#define DMA1_Stream0_PeriphPointerFixed()       (DMA1->S0CR &= ~PINC_BIT)
#define DMA1_Stream0_PeriphPointerIncr()        (DMA1->S0CR |= PINC)
#define DMA1_Stream0_DataDirection(DATA_dir)    {(DMA1->S0CR &= ~DIR_BITMASK); (DMA1->S0CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream0_DMAControlFlow()           (DMA1->S0CR &= ~PFCTRL_BIT)
#define DMA1_Stream0_PeriphControlFlow()        (DMA1->S0CR |= PFCTRL_BIT)
#define DMA1_Stream0_TransfCompleteIntEn()      (DMA1->S0CR |= TCIE_DMA_BIT)
#define DMA1_Stream0_TransfCompleteIntDis()     (DMA1->S0CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream0_HaltTransfIntEn()          (DMA1->S0CR |= HTIE_BIT)
#define DMA1_Stream0_HaltTransfIntDis()         (DMA1->S0CR &= ~HTIE_BIT)
#define DMA1_Stream0_TransfErrIntEn()           (DMA1->S0CR |= TEIE_BIT)
#define DMA1_Stream0_TransfErrIntDis()          (DMA1->S0CR &= ~TEIE_BIT)
#define DMA1_Stream0_DirectModeIntEn()          (DMA1->S0CR |= DMEIE_BIT)
#define DMA1_Stream0_DirectModeIntDis()         (DMA1->S0CR &= ~DMEIE_BIT)
#define DMA1_Stream0_Enable()                   (DMA1->S0CR |= EN_BIT)
#define DMA1_Stream0_Disable()                  (DMA1->S0CR &= ~EN_BIT)
#define DMA1_Stream0_NrOfData(NR_data)          (DMA1->S0NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream0_PeriphAddress(Paddr)       (DMA1->S0PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream0_Mem0Address(Mem0addr)      (DMA1->S0M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream0_Mem1Address(Mem1addr)      (DMA1->S0M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream0_ReadTCFlag()               (DMA1->LISR & TCIF0_BIT)

/* Stream 1 */
#define DMA1_Stream1_Channel(CH)                {(DMA1->S1CR &= ~(CHSEL_BITMASK)); (DMA1->S1CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream1_Priority(PR_lvl)           {(DMA1->S1CR &= ~(PL_BITMASK)); (DMA1->S1CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream1_MemSize(MEM_size)          {(DMA1->S1CR &= ~(MSIZE_BITMASK)); (DMA1->S1CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream1_PeriphSize(MEM_size)       {(DMA1->S1CR &= ~(PSIZE_BITMASK)); (DMA1->S1CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream1_MemPointerFixed()          (DMA1->S1CR &= ~MINC_BIT)
#define DMA1_Stream1_MemPointerIncr()           (DMA1->S1CR |= MINC)
#define DMA1_Stream1_PeriphPointerFixed()       (DMA1->S1CR &= ~PINC_BIT)
#define DMA1_Stream1_PeriphPointerIncr()        (DMA1->S1CR |= PINC)
#define DMA1_Stream1_DataDirection(DATA_dir)    {(DMA1->S1CR &= ~DIR_BITMASK); (DMA1->S1CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream1_DMAControlFlow()           (DMA1->S1CR &= ~PFCTRL_BIT)
#define DMA1_Stream1_PeriphControlFlow()        (DMA1->S1CR |= PFCTRL_BIT)
#define DMA1_Stream1_TransfCompleteIntEn()      (DMA1->S1CR |= TCIE_DMA_BIT)
#define DMA1_Stream1_TransfCompleteIntDis()     (DMA1->S1CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream1_HaltTransfIntEn()          (DMA1->S1CR |= HTIE_BIT)
#define DMA1_Stream1_HaltTransfIntDis()         (DMA1->S1CR &= ~HTIE_BIT)
#define DMA1_Stream1_TransfErrIntEn()           (DMA1->S1CR |= TEIE_BIT)
#define DMA1_Stream1_TransfErrIntDis()          (DMA1->S1CR &= ~TEIE_BIT)
#define DMA1_Stream1_DirectModeIntEn()          (DMA1->S1CR |= DMEIE_BIT)
#define DMA1_Stream1_DirectModeIntDis()         (DMA1->S1CR &= ~DMEIE_BIT)
#define DMA1_Stream1_Enable()                   (DMA1->S1CR |= EN_BIT)
#define DMA1_Stream1_Disable()                  (DMA1->S1CR &= ~EN_BIT)
#define DMA1_Stream1_NrOfData(NR_data)          (DMA1->S1NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream1_PeriphAddress(Paddr)       (DMA1->S1PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream1_Mem0Address(Mem0addr)      (DMA1->S1M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream1_Mem1Address(Mem1addr)      (DMA1->S1M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream1_ReadTCFlag()               (DMA1->LISR & TCIF1_BIT)

/* Stream 2 */
#define DMA1_Stream2_Channel(CH)                {(DMA1->S2CR &= ~(CHSEL_BITMASK)); (DMA1->S2CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream2_Priority(PR_lvl)           {(DMA1->S2CR &= ~(PL_BITMASK)); (DMA1->S2CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream2_MemSize(MEM_size)          {(DMA1->S2CR &= ~(MSIZE_BITMASK)); (DMA1->S2CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream2_PeriphSize(MEM_size)       {(DMA1->S2CR &= ~(PSIZE_BITMASK)); (DMA1->S2CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream2_MemPointerFixed()          (DMA1->S2CR &= ~MINC_BIT)
#define DMA1_Stream2_MemPointerIncr()           (DMA1->S2CR |= MINC)
#define DMA1_Stream2_PeriphPointerFixed()       (DMA1->S2CR &= ~PINC_BIT)
#define DMA1_Stream2_PeriphPointerIncr()        (DMA1->S2CR |= PINC)
#define DMA1_Stream2_DataDirection(DATA_dir)    {(DMA1->S2CR &= ~DIR_BITMASK); (DMA1->S2CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream2_DMAControlFlow()           (DMA1->S2CR &= ~PFCTRL_BIT)
#define DMA1_Stream2_PeriphControlFlow()        (DMA1->S2CR |= PFCTRL_BIT)
#define DMA1_Stream2_TransfCompleteIntEn()      (DMA1->S2CR |= TCIE_DMA_BIT)
#define DMA1_Stream2_TransfCompleteIntDis()     (DMA1->S2CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream2_HaltTransfIntEn()          (DMA1->S2CR |= HTIE_BIT)
#define DMA1_Stream2_HaltTransfIntDis()         (DMA1->S2CR &= ~HTIE_BIT)
#define DMA1_Stream2_TransfErrIntEn()           (DMA1->S2CR |= TEIE_BIT)
#define DMA1_Stream2_TransfErrIntDis()          (DMA1->S2CR &= ~TEIE_BIT)
#define DMA1_Stream2_DirectModeIntEn()          (DMA1->S2CR |= DMEIE_BIT)
#define DMA1_Stream2_DirectModeIntDis()         (DMA1->S2CR &= ~DMEIE_BIT)
#define DMA1_Stream2_Enable()                   (DMA1->S2CR |= EN_BIT)
#define DMA1_Stream2_Disable()                  (DMA1->S2CR &= ~EN_BIT)
#define DMA1_Stream2_NrOfData(NR_data)          (DMA1->S2NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream2_PeriphAddress(Paddr)       (DMA1->S2PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream2_Mem0Address(Mem0addr)      (DMA1->S2M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream2_Mem1Address(Mem1addr)      (DMA1->S2M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream2_ReadTCFlag()               (DMA1->LISR & TCIF2_BIT)

/* Stream 3 */
#define DMA1_Stream3_Channel(CH)                {(DMA1->S3CR &= ~(CHSEL_BITMASK)); (DMA1->S3CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream3_Priority(PR_lvl)           {(DMA1->S3CR &= ~(PL_BITMASK)); (DMA1->S3CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream3_MemSize(MEM_size)          {(DMA1->S3CR &= ~(MSIZE_BITMASK)); (DMA1->S3CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream3_PeriphSize(MEM_size)       {(DMA1->S3CR &= ~(PSIZE_BITMASK)); (DMA1->S3CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream3_MemPointerFixed()          (DMA1->S3CR &= ~MINC_BIT)
#define DMA1_Stream3_MemPointerIncr()           (DMA1->S3CR |= MINC)
#define DMA1_Stream3_PeriphPointerFixed()       (DMA1->S3CR &= ~PINC_BIT)
#define DMA1_Stream3_PeriphPointerIncr()        (DMA1->S3CR |= PINC)
#define DMA1_Stream3_DataDirection(DATA_dir)    {(DMA1->S3CR &= ~DIR_BITMASK); (DMA1->S3CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream3_DMAControlFlow()           (DMA1->S3CR &= ~PFCTRL_BIT)
#define DMA1_Stream3_PeriphControlFlow()        (DMA1->S3CR |= PFCTRL_BIT)
#define DMA1_Stream3_TransfCompleteIntEn()      (DMA1->S3CR |= TCIE_DMA_BIT)
#define DMA1_Stream3_TransfCompleteIntDis()     (DMA1->S3CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream3_HaltTransfIntEn()          (DMA1->S3CR |= HTIE_BIT)
#define DMA1_Stream3_HaltTransfIntDis()         (DMA1->S3CR &= ~HTIE_BIT)
#define DMA1_Stream3_TransfErrIntEn()           (DMA1->S3CR |= TEIE_BIT)
#define DMA1_Stream3_TransfErrIntDis()          (DMA1->S3CR &= ~TEIE_BIT)
#define DMA1_Stream3_DirectModeIntEn()          (DMA1->S3CR |= DMEIE_BIT)
#define DMA1_Stream3_DirectModeIntDis()         (DMA1->S3CR &= ~DMEIE_BIT)
#define DMA1_Stream3_Enable()                   (DMA1->S3CR |= EN_BIT)
#define DMA1_Stream3_Disable()                  (DMA1->S3CR &= ~EN_BIT)
#define DMA1_Stream3_NrOfData(NR_data)          (DMA1->S3NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream3_PeriphAddress(Paddr)       (DMA1->S3PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream3_Mem0Address(Mem0addr)      (DMA1->S3M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream3_Mem1Address(Mem1addr)      (DMA1->S3M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream3_ReadTCFlag()               (DMA1->LISR & TCIF3_BIT)

/* Stream 4 */
#define DMA1_Stream4_Channel(CH)                {(DMA1->S4CR &= ~(CHSEL_BITMASK)); (DMA1->S4CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream4_Priority(PR_lvl)           {(DMA1->S4CR &= ~(PL_BITMASK)); (DMA1->S4CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream4_MemSize(MEM_size)          {(DMA1->S4CR &= ~(MSIZE_BITMASK)); (DMA1->S4CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream4_PeriphSize(MEM_size)       {(DMA1->S4CR &= ~(PSIZE_BITMASK)); (DMA1->S4CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream4_MemPointerFixed()          (DMA1->S4CR &= ~MINC_BIT)
#define DMA1_Stream4_MemPointerIncr()           (DMA1->S4CR |= MINC)
#define DMA1_Stream4_PeriphPointerFixed()       (DMA1->S4CR &= ~PINC_BIT)
#define DMA1_Stream4_PeriphPointerIncr()        (DMA1->S4CR |= PINC)
#define DMA1_Stream4_DataDirection(DATA_dir)    {(DMA1->S4CR &= ~DIR_BITMASK); (DMA1->S4CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream4_DMAControlFlow()           (DMA1->S4CR &= ~PFCTRL_BIT)
#define DMA1_Stream4_PeriphControlFlow()        (DMA1->S4CR |= PFCTRL_BIT)
#define DMA1_Stream4_TransfCompleteIntEn()      (DMA1->S4CR |= TCIE_DMA_BIT)
#define DMA1_Stream4_TransfCompleteIntDis()     (DMA1->S4CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream4_HaltTransfIntEn()          (DMA1->S4CR |= HTIE_BIT)
#define DMA1_Stream4_HaltTransfIntDis()         (DMA1->S4CR &= ~HTIE_BIT)
#define DMA1_Stream4_TransfErrIntEn()           (DMA1->S4CR |= TEIE_BIT)
#define DMA1_Stream4_TransfErrIntDis()          (DMA1->S4CR &= ~TEIE_BIT)
#define DMA1_Stream4_DirectModeIntEn()          (DMA1->S4CR |= DMEIE_BIT)
#define DMA1_Stream4_DirectModeIntDis()         (DMA1->S4CR &= ~DMEIE_BIT)
#define DMA1_Stream4_Enable()                   (DMA1->S4CR |= EN_BIT)
#define DMA1_Stream4_Disable()                  (DMA1->S4CR &= ~EN_BIT)
#define DMA1_Stream4_NrOfData(NR_data)          (DMA1->S4NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream4_PeriphAddress(Paddr)       (DMA1->S4PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream4_Mem0Address(Mem0addr)      (DMA1->S4M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream4_Mem1Address(Mem1addr)      (DMA1->S4M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream4_ReadTCFlag()               (DMA1->HISR & TCIF4_BIT)

/* Stream 5 */
#define DMA1_Stream5_Channel(CH)                {(DMA1->S5CR &= ~(CHSEL_BITMASK)); (DMA1->S5CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream5_Priority(PR_lvl)           {(DMA1->S5CR &= ~(PL_BITMASK)); (DMA1->S5CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream5_MemSize(MEM_size)          {(DMA1->S5CR &= ~(MSIZE_BITMASK)); (DMA1->S5CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream5_PeriphSize(MEM_size)       {(DMA1->S5CR &= ~(PSIZE_BITMASK)); (DMA1->S5CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream5_MemPointerFixed()          (DMA1->S5CR &= ~MINC_BIT)
#define DMA1_Stream5_MemPointerIncr()           (DMA1->S5CR |= MINC)
#define DMA1_Stream5_PeriphPointerFixed()       (DMA1->S5CR &= ~PINC_BIT)
#define DMA1_Stream5_PeriphPointerIncr()        (DMA1->S5CR |= PINC)
#define DMA1_Stream5_DataDirection(DATA_dir)    {(DMA1->S5CR &= ~DIR_BITMASK); (DMA1->S5CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream5_DMAControlFlow()           (DMA1->S5CR &= ~PFCTRL_BIT)
#define DMA1_Stream5_PeriphControlFlow()        (DMA1->S5CR |= PFCTRL_BIT)
#define DMA1_Stream5_TransfCompleteIntEn()      (DMA1->S5CR |= TCIE_DMA_BIT)
#define DMA1_Stream5_TransfCompleteIntDis()     (DMA1->S5CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream5_HaltTransfIntEn()          (DMA1->S5CR |= HTIE_BIT)
#define DMA1_Stream5_HaltTransfIntDis()         (DMA1->S5CR &= ~HTIE_BIT)
#define DMA1_Stream5_TransfErrIntEn()           (DMA1->S5CR |= TEIE_BIT)
#define DMA1_Stream5_TransfErrIntDis()          (DMA1->S5CR &= ~TEIE_BIT)
#define DMA1_Stream5_DirectModeIntEn()          (DMA1->S5CR |= DMEIE_BIT)
#define DMA1_Stream5_DirectModeIntDis()         (DMA1->S5CR &= ~DMEIE_BIT)
#define DMA1_Stream5_Enable()                   (DMA1->S5CR |= EN_BIT)
#define DMA1_Stream5_Disable()                  (DMA1->S5CR &= ~EN_BIT)
#define DMA1_Stream5_NrOfData(NR_data)          (DMA1->S5NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream5_PeriphAddress(Paddr)       (DMA1->S5PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream5_Mem0Address(Mem0addr)      (DMA1->S5M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream5_Mem1Address(Mem1addr)      (DMA1->S5M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream5_ReadTCFlag()               (DMA1->HISR & TCIF5_BIT)

/* Stream 6 */
#define DMA1_Stream6_Channel(CH)                {(DMA1->S6CR &= ~(CHSEL_BITMASK)); (DMA1->S6CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream6_Priority(PR_lvl)           {(DMA1->S6CR &= ~(PL_BITMASK)); (DMA1->S6CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream6_MemSize(MEM_size)          {(DMA1->S6CR &= ~(MSIZE_BITMASK)); (DMA1->S6CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream6_PeriphSize(MEM_size)       {(DMA1->S6CR &= ~(PSIZE_BITMASK)); (DMA1->S6CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream6_MemPointerFixed()          (DMA1->S6CR &= ~MINC_BIT)
#define DMA1_Stream6_MemPointerIncr()           (DMA1->S6CR |= MINC)
#define DMA1_Stream6_PeriphPointerFixed()       (DMA1->S6CR &= ~PINC_BIT)
#define DMA1_Stream6_PeriphPointerIncr()        (DMA1->S6CR |= PINC)
#define DMA1_Stream6_DataDirection(DATA_dir)    {(DMA1->S6CR &= ~DIR_BITMASK); (DMA1->S6CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream6_DMAControlFlow()           (DMA1->S6CR &= ~PFCTRL_BIT)
#define DMA1_Stream6_PeriphControlFlow()        (DMA1->S6CR |= PFCTRL_BIT)
#define DMA1_Stream6_TransfCompleteIntEn()      (DMA1->S6CR |= TCIE_DMA_BIT)
#define DMA1_Stream6_TransfCompleteIntDis()     (DMA1->S6CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream6_HaltTransfIntEn()          (DMA1->S6CR |= HTIE_BIT)
#define DMA1_Stream6_HaltTransfIntDis()         (DMA1->S6CR &= ~HTIE_BIT)
#define DMA1_Stream6_TransfErrIntEn()           (DMA1->S6CR |= TEIE_BIT)
#define DMA1_Stream6_TransfErrIntDis()          (DMA1->S6CR &= ~TEIE_BIT)
#define DMA1_Stream6_DirectModeIntEn()          (DMA1->S6CR |= DMEIE_BIT)
#define DMA1_Stream6_DirectModeIntDis()         (DMA1->S6CR &= ~DMEIE_BIT)
#define DMA1_Stream6_Enable()                   (DMA1->S6CR |= EN_BIT)
#define DMA1_Stream6_Disable()                  (DMA1->S6CR &= ~EN_BIT)
#define DMA1_Stream6_NrOfData(NR_data)          (DMA1->S6NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream6_PeriphAddress(Paddr)       (DMA1->S6PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream6_Mem0Address(Mem0addr)      (DMA1->S6M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream6_Mem1Address(Mem1addr)      (DMA1->S6M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream6_ReadTCFlag()               (DMA1->HISR & TCIF6_BIT)

/* Stream 7 */
#define DMA1_Stream7_Channel(CH)                {(DMA1->S7CR &= ~(CHSEL_BITMASK)); (DMA1->S7CR |= (CH << CHSEL_BITS_OFFSET));}
#define DMA1_Stream7_Priority(PR_lvl)           {(DMA1->S7CR &= ~(PL_BITMASK)); (DMA1->S7CR |= (PR_lvl << PL_BITS_OFFSET));}
#define DMA1_Stream7_MemSize(MEM_size)          {(DMA1->S7CR &= ~(MSIZE_BITMASK)); (DMA1->S7CR |= (MEM_size << MSIZE_BITS_OFFSET));}
#define DMA1_Stream7_PeriphSize(MEM_size)       {(DMA1->S7CR &= ~(PSIZE_BITMASK)); (DMA1->S7CR |= (MEM_size << PSIZE_BITS_OFFSET));}
#define DMA1_Stream7_MemPointerFixed()          (DMA1->S7CR &= ~MINC_BIT)
#define DMA1_Stream7_MemPointerIncr()           (DMA1->S7CR |= MINC_BIT)
#define DMA1_Stream7_PeriphPointerFixed()       (DMA1->S7CR &= ~PINC_BIT)
#define DMA1_Stream7_PeriphPointerIncr()        (DMA1->S7CR |= PINC_BIT)
#define DMA1_Stream7_DataDirection(DATA_dir)    {(DMA1->S7CR &= ~DIR_BITMASK); (DMA1->S7CR |= (DATA_dir << DIR_BITS_OFFSET));}
#define DMA1_Stream7_DMAControlFlow()           (DMA1->S7CR &= ~PFCTRL_BIT)
#define DMA1_Stream7_PeriphControlFlow()        (DMA1->S7CR |= PFCTRL_BIT)
#define DMA1_Stream7_TransfCompleteIntEn()      (DMA1->S7CR |= TCIE_DMA_BIT)
#define DMA1_Stream7_TransfCompleteIntDis()     (DMA1->S7CR &= ~TCIE_DMA_BIT)
#define DMA1_Stream7_HaltTransfIntEn()          (DMA1->S7CR |= HTIE_BIT)
#define DMA1_Stream7_HaltTransfIntDis()         (DMA1->S7CR &= ~HTIE_BIT)
#define DMA1_Stream7_TransfErrIntEn()           (DMA1->S7CR |= TEIE_BIT)
#define DMA1_Stream7_TransfErrIntDis()          (DMA1->S7CR &= ~TEIE_BIT)
#define DMA1_Stream7_DirectModeIntEn()          (DMA1->S7CR |= DMEIE_BIT)
#define DMA1_Stream7_DirectModeIntDis()         (DMA1->S7CR &= ~DMEIE_BIT)
#define DMA1_Stream7_Enable()                   (DMA1->S7CR |= EN_BIT)
#define DMA1_Stream7_Disable()                  (DMA1->S7CR &= ~EN_BIT)
#define DMA1_Stream7_NrOfData(NR_data)          (DMA1->S7NDTR = (NR_data & NDT_BITMASK))
#define DMA1_Stream7_PeriphAddress(Paddr)       (DMA1->S7PAR = (Paddr & PAR_BITMASK))
#define DMA1_Stream7_Mem0Address(Mem0addr)      (DMA1->S7M0AR = (Mem0addr & M0AR_BITMASK))
#define DMA1_Stream7_Mem1Address(Mem1addr)      (DMA1->S7M1AR = (Mem1addr & M1AR_BITMASK))
#define DMA1_Stream7_ReadTCFlag()               (DMA1->HISR & TCIF7_BIT)

void DMA1_Init(void);

#endif /* _DMA1_DRIVER_H */