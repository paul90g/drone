#ifndef _STRING_H
#define _STRING_H

#include "Type.h"

#define NULL_CHAR  '\0'

uint16  strlen(const uint8 *string);
void    strcpy(uint8 *dest, const uint8 *source);
void    strncpy(uint8 *dest, const uint8 *source, const uint16 size);
uint8  *strstr(const uint8 *haystack, const uint8 *needle);
uint8  *strncat(uint8 *str1, const uint16 destSize, const uint8 *str2);
int8    strcmp(const uint8 *str1, const uint8 *str2);
void    reverse(uint8 *str);
void    itoa(uint8 *str, int32 nr);
void    ftoa(uint8 *str, float nr);

#endif /* _STRING_H */