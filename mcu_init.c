#include "mcu_init.h"
#include "stm32f411re.h"

void mcu_init(){
  
/******************************************
  *   if DEBUG_MODE is defined
  * the clock from the debugger
  * acts as the clock for the peripherals
*******************************************/
#ifdef DEBUG_MODE
  DBGMCU->APB1_FZ = (uint32)0x02;
#endif /* DEBUG_MODE */
  
  FPU_Init();
  SYSTICK_Init();
  FlashIntf_Init();
  RCC_Init();
  GPIOA_Init();
  GPIOB_Init();
  GPIOC_Init();
  TIM3_Init();
  ADC_Init();
  DMA1_Init();
  DMA2_Init();
  USART1_Init();
  USART6_Init();
  I2C1_Init();
  
#if (INTERRUP_EN == 1)
  _ENABLE_INTERRUPT();
#else
  _DISABLE_INTERRUPT();
#endif

  NVIC_EnableIRQ(SysTick_IRQn);
  NVIC_EnableIRQ(TIM3_IRQn);
  NVIC_EnableIRQ(TIM4_IRQn);
  NVIC_EnableIRQ(ADC_IRQn);
  NVIC_EnableIRQ(DMA2_Stream7_IRQn);
  NVIC_EnableIRQ(DMA2_Stream6_IRQn);
  NVIC_EnableIRQ(DMA1_Stream0_IRQn);
  NVIC_EnableIRQ(USART1_IRQn);
  NVIC_EnableIRQ(I2C1_EV_IRQn);

  /** Peripheral devices initialisation **/
  //L3G4200D_Init();
  //ADXL345_Init();
  //ESP8266_Init();
  //BMP085_Init();
  MPU6050_Init();
  
  TIM4_Init();
  
  return;
}
