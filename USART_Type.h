#ifndef _USART_TYPE
#define _USART_TYPE

#include "Type.h"

typedef struct {
  uint32 SR;    // Status Register
  uint32 DR;    // Data Register
  uint32 BRR;   // Baud Rate Register
  uint32 CR1;   // Control Register 1
  uint32 CR2;   // Control Register 2
  uint32 CR3;   // Control Register 3
  uint32 GTPR;  // Guard Time and Prescaler Register
} USART_Type;

/* Status Register */
#define CTS_BIT         (1 << 9)
#define LBD_BIT         (1 << 8)
#define TXE_BIT         (1 << 7)
#define TC_BIT          (1 << 6)
#define BXNE_BIT        (1 << 5)
#define IDLE_BIT        (1 << 4)
#define ORE_BIT         (1 << 3)
#define NF_BIT          (1 << 2)
#define FE_BIT          (1 << 1)
#define PE_BIT          (1 << 0)

/* Data Register */
#define DR_BITS_OFFSET  0
#define DR_BITMASK      0x1FF

/* Baud Rate Register */
#define DIV_Mantissa_BITS_OFFSET        4
#define DIV_Mantissa_BITMASK            0xFFF0
#define DIV_Fraction_BITS_OFFSET        0
#define DIV_Fraction_BITMASK            0xF

/* Control Register 1 */
#define OVER8_BIT       (1 << 15)
#define UE_BIT          (1 << 13)
#define M_BIT           (1 << 12)
#define WAKE_BIT        (1 << 11)
#define PCE_BIT         (1 << 10)
#define PS_BIT          (1 << 9)
#define PEIE_BIT        (1 << 8)
#define TXEIE_BIT       (1 << 7)
#define TCIE_BIT        (1 << 6)
#define RXNEIE_BIT      (1 << 5)
#define IDLEIE_BIT      (1 << 4)
#define TE_BIT          (1 << 3)
#define RE_BIT          (1 << 2)
#define RWU_BIT         (1 << 1)
#define SBK_BIT         (1 << 0)

/* Control Register 2 */
#define LINEN_BIT       (1 << 14)
#define STOP_BITS_OFFSET 12
#define STOP_BITMASK    0x3000
#define CLKEN_BIT       (1 << 11)
#define CPOL_BIT        (1 << 10)
#define CPHA_BIT        (1 << 9)
#define LBCL_BIT        (1 << 8)
#define LBDIE_BIT       (1 << 6)
#define LBDL_BIT        (1 << 5)
#define ADD_BITS_OFFSET 0
#define ADD_BITMASK     0x7

/* Control Register 3 */
#define ONEBIT_BIT      (1 << 11)
#define CTSIE_BIT       (1 << 10)
#define CTSE_BIT        (1 << 9)
#define RTSE_BIT        (1 << 8)
#define DMAT_BIT        (1 << 7)
#define DMAR_BIT        (1 << 6)
#define SCEN_BIT        (1 << 5)
#define NACK_BIT        (1 << 4)
#define HDSEL_BIT       (1 << 3)
#define IRLP_BIT        (1 << 2)
#define IREN_BIT        (1 << 1)
#define EIE_BIT         (1 << 0)

/* Guard Time and Prescaler Register */
#define GT_BITS_OFFSET  8
#define GT_BITMASK      0xFF00
#define PSC_BITS_OFFSET 0
#define PSC_BITMASK     0xFF

#endif /* USART_TYPE */