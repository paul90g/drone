#include "Drivers.h"
#include "GPIOA_Driver.h"

void GPIOA_Init(){
  Enable_GPIOA_Clk();
  
  //enable ADC pin (ADC1_1 pin)
  GPIOA_PinMode(PIN0 , PIN_MODE_ANALOG);

  // enable USART1
  GPIOA_PinMode(PIN9, PIN_MODE_ALT_FUNC); // TX
  GPIOA_Pin_SetAltFunc(PIN9, AF7);
  GPIOA_Pin_Speed(PIN9, HIGH_SPEED);
  GPIOA_PinMode(PIN10, PIN_MODE_ALT_FUNC); // RX
  GPIOA_Pin_SetAltFunc(PIN10, AF7);
  GPIOA_Pin_Speed(PIN10, HIGH_SPEED);
  
  return;
}