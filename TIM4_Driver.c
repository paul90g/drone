#include "TIM4_Driver.h"
#include "Drivers.h"

void TIM4_Init(){
  Enable_TIM4_Clk();
  
  TIM4_SelectOutputCompare();
  TIM4_SetPrescaler(0x270F);
  TIM4_SetAutoReload(0x64);
  TIM4_AutoReloadBuffered();
  
  TIM4_Channel1PreloadEnable();
  TIM4_CC1IntEnable();
  
  TIM4_UpdateIntEnable();
  
  TIM4_UpdateEvent();
  TIM4_CounterEnable();
  
  return;
}

void TIM4_IRQHandler()
{
  TIM4_ClearStatusReg();
  //ADXL345_IssueRead();
  //L3G4200D_IssueRead();
  MPU6050_IssueRead();
  return;
}