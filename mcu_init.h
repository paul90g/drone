#ifndef _MCU_INIT_H
#define _MCU_INIT_H

#include "config.h"
#include "Drivers.h"

void mcu_init(void);

#endif /* _MCU_INIT_H */