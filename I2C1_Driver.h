#ifndef _I2C1_DRIVER_H
#define _I2C1_DRIVER_H

#include "I2C_Driver_Type.h"

#define I2C1    ((I2C_TypeDef *)I2C1_BASE)

#define I2C1_Enable()                   (I2C1->CR1 |= I2C_PE_BIT)
#define I2C1_Disable()                  (I2C1->CR1 &= ~I2C_PE_BIT)
#define I2C1_StartBit()                 (I2C1->CR1 |= START_BIT)
#define I2C1_StopBit()                  (I2C1->CR1 |= STOP_BIT)
#define I2C1_AcknowledgeEnable()        (I2C1->CR1 |= ACK_BIT)
#define I2C1_AcknowledgeDisable()       (I2C1->CR1 &= ~ACK_BIT)
#define I2C1_ClockStretchEnable()       (I2C1->CR1 &= ~NOSTRETCH_BIT)
#define I2C1_ClockStretchDisable()      (I2C1->CR1 |= NOSTRETCH_BIT)

#define I2C1_DMALastBitEnable()         (I2C1->CR2 |= LAST_BIT)
#define I2C1_DMALastBitDisable()        (I2C1->CR2 &= ~LAST_BIT)
#define I2C1_BuffIntEnable()            (I2C1->CR2 |= ITBUFEN_BIT)
#define I2C1_BuffIntDisable()           (I2C1->CR2 &= ~ITBUFEN_BIT)
#define I2C1_EvntIntEnable()            (I2C1->CR2 |= ITEVTEN_BIT)
#define I2C1_EvntIntDisable()           (I2C1->CR2 &= ~ITEVTEN_BIT)
#define I2C1_ErrorIntEnable()           (I2C1->CR2 |= ITERREN_BIT)
#define I2C1_ErrorIntDisable()          (I2C1->CR2 &= ~ITERREN_BIT)
#define I2C1_DmaEnable()                (I2C1->CR2 |= DMAEN_BIT)
#define I2C1_DmaDisable()               (I2C1->CR2 &= ~DMAEN_BIT)

#define I2C1_SetPeriphClock(CLK)        {(I2C1->CR2 &= ~(0x3F)); (I2C1->CR2 |= (CLK & 0x3F));}

#define I2C1_WriteData(DATA)            (I2C1->DR = DATA)
#define I2C1_ReadData()                 (I2C1->DR)

#define I2C1_FmEnable()                 (I2C1->CCR |= FS_BIT)
#define I2C1_SmEnable()                 (I2C1->CCR &= ~FS_BIT)
#define I2C1_SetDuty16_9()              (I2C1->CCR |= DUTY_BIT)
#define I2C1_SetDuty2()                 (I2C1->CCR &= ~DUTY_BIT)
#define I2C1_SetCCR(CCR_Val)            {(I2C1->CCR &= ~(0xFFF)); (I2C1->CCR |= (CCR_Val & 0xFFF));}

#define I2C1_SetAddMode(ADDMODE)        {(I2C1->OAR1 &= ~(ADDMODE_BITMASK<<ADDMODE_BITS)); \
                                        (I2C1->OAR1 |= (ADDMODE & ADDMODE_BITMASK)<<ADDMODE_BITS);}

#define I2C1_AnalogFilterEnable()       (I2C1->FLTR |= ANOFF_BIT)

#define I2C1_SetTrise(TIME)             (I2C1->TRISE = TIME & 0x3F)

#define I2C1_ClearAddrFlag()                    \
  do{                                           \
    volatile uint32 tmpreg;                     \
    tmpreg = I2C1->SR1;                         \
    tmpreg = I2C1->SR2;                         \
  } while(0)
    
#define MAX_QUEUE       10

void I2C1_Init(void);
static void I2C1_InitQueue(void);
void I2C1_Tick(void);

boolean I2C1_IsBusy(void);
boolean I2C1_ReserveReq(uint8 nr_of_reserved_requests);

boolean I2C1_WriteReg(uint8 addr, uint8 reg, uint8 data);
boolean I2C1_WriteByte(uint8 addr, uint8 data);
boolean I2C1_ReadByte(uint8 addr, void (*I2C_Handler)(uint8*, uint8));
boolean I2C1_ReadByteStream(uint8 addr, uint8 nr_of_bytes, void (*I2C_Handler)(uint8*, uint8));

#endif /* _I2C1_DRIVER_H */