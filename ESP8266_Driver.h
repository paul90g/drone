#ifndef _ESP8266_DRIVER_H
#define _ESP8266_DRIVER_H

#include "Type.h"
#include "String.h"

/* Driver implements only multiple TCP/UDP connection, needed for server */


/******************************************************/
/***************** ESP8266 AT Commands ****************/
#define ESP_TEST_COMMAND        "AT"
#define ESP_BASE_COMMAND        "AT+"
#define ESP_RESET               "RST"

/* WiFi Commands */
#define ESP_WIFI_MODE           "CWMODE"
#define ESP_JOIN_AP             "CWJAP"
#define ESP_LIST_AP             "CWLAP"
#define ESP_QUIT_AP             "CWQAP"
#define ESP_SET_AP              "CWSAP"

/* TCP_IP Commands */
#define ESP_GET_CONN_STATUS             "CIPSTATUS"
#define ESP_SET_UDP_TCP_CONN            "CIPSTART"
#define ESP_SEND_DATA                   "CIPSEND"
#define ESP_CLOSE_UDP_TCP_CONN          "CIPCLOSE"
#define ESP_GET_IP_ADDRESS              "CIFSR"
#define ESP_SET_MULTIPLE_CONN           "CIPMUX"
#define ESP_SET_SERVER                  "CIPSERVER"

#define ESP_COMMAND_END                 "\r\n"

#define ESP_RECEIVE_DATA_HEAD           "+IPD"
/******************************************************/
/***************** ESP8266 AT Commands ****************/


typedef struct{
  boolean isConnected;
  uint8 ID;
} clientType;

typedef enum{
  ESP_Init,
  ESP_Check,
  ESP_Start,
  ESP_SetUp,
} ESP_StateType;

typedef enum{
  ESP_WiFiMode,
  ESP_SetAP,
  ESP_SetMultipleConn,
  ESP_SetServer,
  ESP_CheckServer
} ESP_SetUpState;

typedef enum{
  Init,
  WaitClient,
  Idle,
  SendResponse
} ServerState;

#define ESP_Command(COMM)       USART1_SendData(COMM)

void ESP8266_Init(void);
void ESP8266_Deinit(void);

void ESP8266_Reset(void);

void ESP8266_Tick(void);

static inline void ESP_InitServer(void);
static inline void ESP_SetupFSM(void);

static uint8 *getMessage(uint8 *data);

void clearBuffer(void);

#endif /* _ESP8266_DRIVER_H */