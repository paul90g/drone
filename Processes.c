#include "Type.h"
#include "Timers.h"
#include "Processes.h"
#include "Drivers.h"
#include "String.h"

#include "StabilityControl.h"

volatile uint32 sysTimer = 0;

Timer32bit timer200us;
Timer32bit timer1ms;
Timer32bit timer10ms;
Timer32bit timer33ms;
Timer32bit timer100ms;
Timer32bit timer1s;

volatile uint32 ticks, angle_dt;
extern gyro_type gyro_data;
extern accel_type accel_data;

angle_type angle;

void init_processes(void){
  init32bitTimer(&timer200us);
  init32bitTimer(&timer1ms);
  init32bitTimer(&timer10ms);
  init32bitTimer(&timer33ms);
  init32bitTimer(&timer100ms);
  init32bitTimer(&timer1s);
  ticks = 0;
  angle_dt = 0.0;
  angle.x = angle.y = angle.z = 0.0;
  return;
}

void run_processes(void){
  while(1){
    if(Timer_Passed == getState32bitTimer(&timer200us, TIMER_200US_TIMEOUT)){
      uint32 ticks_l = SYSTICK_GetTicks();
      run_processes_200us();
      ticks_l -= SYSTICK_GetTicks();
      ticks += ticks_l;
      init32bitTimer(&timer200us);
    }
    if(Timer_Passed == getState32bitTimer(&timer1ms, TIMER_1MS_TIMEOUT)){
      uint32 ticks_l = SYSTICK_GetTicks();
      run_processes_1ms();
      ticks_l -= SYSTICK_GetTicks();
      ticks += ticks_l;
      init32bitTimer(&timer1ms);
    }
    if(Timer_Passed == getState32bitTimer(&timer10ms, TIMER_10MS_TIMEOUT)){
      uint32 ticks_l = SYSTICK_GetTicks();
      run_processes_10ms();
      
      StabilityControl_calcAngle(&angle, gyro_data, accel_data, 0.01);
      
      ticks_l -= SYSTICK_GetTicks();
      ticks += ticks_l;
      init32bitTimer(&timer10ms);
    }
    if(Timer_Passed == getState32bitTimer(&timer33ms, TIMER_33MS_TIMEOUT)){
      uint32 ticks_l = SYSTICK_GetTicks();
      run_processes_33ms();
      ticks_l -= SYSTICK_GetTicks();
      ticks += ticks_l;
      init32bitTimer(&timer33ms);
    }
    if(Timer_Passed == getState32bitTimer(&timer100ms, TIMER_100MS_TIMEOUT)){
      uint32 ticks_l = SYSTICK_GetTicks();
      run_processes_100ms();
      ticks_l -= SYSTICK_GetTicks();
      ticks += ticks_l;
      init32bitTimer(&timer100ms);
    }
    if(Timer_Passed == getState32bitTimer(&timer1s, TIMER_1S_TIMEOUT)){
      uint8 str_buff[64];
      strncpy(str_buff, "Ticks: ", 64);
      uint8 str_nr[12];
      itoa(str_nr, ticks);
      strncat(str_buff, 64, str_nr);
      strncat(str_buff, 64, "\n\r");
      USART6_SendData((const uint8 *)str_buff);
      ticks = 0;
      init32bitTimer(&timer1s);
    }
  }
}

void SysTick_Handler(){ // every 100us
  sysTimer++;
  return;
}