#ifndef _SYSTICK_DRIVER_H
#define _SYSTICK_DRIVER_H

#include "mem_map.h"
#include "Type.h"

typedef struct {
  uint32 CTRL;          /* SYSTICK Timer Control and Status Register    Address offset: 0x00    */
  uint32 LOAD;          /* SYSTICK Timer Reload Value Register          Address offset: 0x04    */
  uint32 VAL;           /* SYSTICK Timer Current Value Register         Address offset: 0x08    */
  uint32 CALIB;         /* SYSTICK Timer Calibration Value Register     Address offset: 0x0C    */
} SYSTICK_TypeDef;

#define SYSTICK         ((SYSTICK_TypeDef *) SYSTICK_BASE)

/* Control and Status Register */
#define CLKSOURCE_BIT   (1 << 2)
#define TICKINT_BIT     (1 << 1)
#define ENABLE_BIT      (1 << 0)

#define SYSTICK_Enable()                (SYSTICK->CTRL |= ENABLE_BIT)
#define SYSTICK_Disable()               (SYSTICK->CTRL &= ~ENABLE_BIT)
#define SYSTICK_EnableInt()             (SYSTICK->CTRL |= TICKINT_BIT)
#define SYSTICK_DisableInt()            (SYSTICK->CTRL &= ~TICKINT_BIT)
#define SYSTICK_AHBClock()              (SYSTICK->CTRL |= CLKSOURCE_BIT)
#define SYSTICK_AHB8Clock()             (SYSTICK->CTRL &= ~CLKSOURCE_BIT)
#define SYSTICK_ReloadValue(VALUE)      (SYSTICK->LOAD = (0xFFFFFF & VALUE))
#define SYSTICK_ClearValue()            (SYSTICK->VAL = 0x00)
#define SYSTICK_GetTicks()              (SYSTICK->VAL)

void SYSTICK_Init(void);

#endif /* _SYSTICK_DRIVER_H */