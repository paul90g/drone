#include "Process_10ms.h"
#include "Processes.h"
#include "Type.h"

#include "ESP8266_Driver.h"
#include "ADXL345_Driver.h"
#include "L3G4200D_Driver.h"
#include "MPU6050_Driver.h"

extern accel_type accel_data;
extern gyro_type gyro_data;

void run_processes_10ms(void){
  //ESP8266_Tick();
  //ADXL345_ReadAccel(&accel_data);
  //L3G4200D_ReadGyro(&gyro_data);
  MPU6050_ReadAccel_Gyro(&accel_data, &gyro_data);
  return;
}