#include "USART6_Driver.h"
#include "Drivers.h"
#include "Type.h"
#include "String.h"
#include "config.h"

void USART6_Init(){
  Enable_USART6_Clk();
  USART6_SetOversampling16();
  USART6_WordLength8Bit();
  USART6_EnableTransmitter();
  USART6_EnableReceiver();
  USART6_EnableDMATransm();
  USART6_SetStopBits(ONE_STOP_BIT);
  USART6_SetBaud(USART6_BAUD);
  USART6_EnableRXNEInterrupt();
  USART6_Enable();
  USART6_ClearStatusFlags();
  return;
}

void USART6_SendData(const uint8 *data){
  DMA2_Stream6_DataDirection(MEM_TO_PERIPH);
  DMA2_Stream6_PeriphAddress((uint32)&USART6->DR);
  DMA2_Stream6_Mem0Address((uint32)data);
  DMA2_Stream6_NrOfData(strlen(data));
  DMA2_Stream6_Enable();
  return;
}

static void USART6_SetBaud(uint32 baud){
#ifdef F_CPU
  float32 udivf;
  uint32 udivm;
  USART6_ResetBaudRegister();
  if(0 < (USART6->CR1 & OVER8_BIT)){
    // oversampling by 8
    udivf = (float32)F_CPU / (float32)(baud * 8);
    udivm = F_CPU / (baud * 8);
    udivf -= udivm;
    USART6_SetBaud_Register(((udivm << DIV_Mantissa_BITS_OFFSET) & DIV_Mantissa_BITMASK) | (uint8)(udivf * 8.0));
  } else {
    // oversampling by 16
    udivf = (float32)F_CPU / (float32)(baud * 16);
    udivm = F_CPU / (baud * 16);
    udivf -= udivm;
    USART6_SetBaud_Register(((udivm << DIV_Mantissa_BITS_OFFSET) & DIV_Mantissa_BITMASK) | (uint8)(udivf * 16.0));
  }
#else
#error "F_CPU not defined! Needed to set baud rate"
#endif
  return;
}