#ifndef _PROT_PARSER_H
#define _PROT_PARSER_H

void decodeMessage(const uint8 *message);
void encodeMessage(uint8 *message);

static uint8 *to_string(uint8 nr);

#endif /* _PROT_PARSER_H */