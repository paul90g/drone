#include "StabilityControl.h"
#include "Type.h"
#include "math.h"

void StabilityControl_calcAngle(angle_type *angle, gyro_type gyro, accel_type accel, float dt)
{
  // compute angles
  
  float32 xAngle = atan( accel.x / (sqrt(accel.y * accel.y + accel.z * accel.z))) * 180.0 / 3.141592;
  float32 yAngle = atan( accel.y / (sqrt(accel.x * accel.x + accel.z * accel.z))) * 180.0 / 3.141592;
  /*
  xAngle *= 180.00;   yAngle *= 180.00;
  xAngle /= 3.141592; yAngle /= 3.141592;
  */
  angle->x = ANGLE_FILTER * (angle->x + gyro.x * dt) + (1.0 - ANGLE_FILTER) * (yAngle);
  angle->y = ANGLE_FILTER * (angle->y + gyro.y * dt) + (1.0 - ANGLE_FILTER) * (-xAngle);
  angle->z = (angle->z + gyro.z * dt);
  /*
  angle->x = (angle->x + gyro.x * dt);
  angle->y = (angle->y + gyro.y * dt);
  angle->z = (angle->z + gyro.z * dt);
  */
  return;
}
