#ifndef _GPIOC_DRIVER_H
#define _GPIOC_DRIVER_H

#include "mem_map.h"
#include "GPIO_Type.h"
#include "Drivers.h"

#define GPIOC   ((GPIO_TypeDef *)GPIOC_BASE)

#define PIN_MODE_INPUT          0
#define PIN_MODE_OUTPUT         1
#define PIN_MODE_ALT_FUNC       2
#define PIN_MODE_ANALOG         3

#define PIN_OUTPUT_PUSH_PULL    0
#define PIN_OUTPUT_OPEN_DRAIN   1

#define PIN_NO_PUPD             0
#define PIN_PULL_UP             1
#define PIN_PULL_DOWN           2

#define AF_MASK                 0xF

#define GPIOC_PinMode(PIN_NR, MODE)             {GPIOC->MODER &= ~(11 << (PIN_NR * 2)); GPIOC->MODER |= (MODE << (PIN_NR * 2));}
#define GPIOC_PinOutputType(PIN_NR, TYPE)       {GPIOC->OTYPER &= ~(1 << PIN_NR); GPIOC->OTYPER |= (TYPE << PIN_NR);}
#define GPIOC_Pin_PullUp_PullDown(PIN_NR, PUPD) {GPIOC->PUPDR &= ~(11 << (PIN_NR * 2)); GPIOC->PUPDR |= (PUPD << (PIN_NR * 2));}
#define GPIOC_Pin_Speed(PIN_NR, SPD)            {(GPIOC->OSPEEDR &= ~(SPEED_BITMASK << (PIN_NR * 2))); (GPIOC->OSPEEDR |= (SPD << (PIN_NR * 2)));}
#define GPIOC_Pin_ReadData(PIN_NR)              (GPIOC->IDR & (1 << PIN_NR))
#define GPIOC_Pin_WriteData(PIN_NR, DATA)       {GPIOC->ODR &= ~(1 << PIN_NR); GPIOC->ODR |= (DATA << PIN_NR);}
#define GPIOC_Pin_Set(PIN_NR)                   (GPIOC->BSRR |= (1 << PIN_NR))
#define GPIOC_Pin_Reset(PIN_NR)                 (GPIOC->BSRR |= (1 << (PIN_NR + 16)))
#define GPIOC_Pin_SetAltFunc(PIN_NR, AltFunc)   {if(PIN_NR < 8) {                                       \
                                                  GPIOC->AFRL &= ~(AF_MASK << (PIN_NR * 4));            \
                                                  GPIOC->AFRL |= (AltFunc << (PIN_NR * 4));             \
                                                 } else {                                               \
                                                  GPIOC->AFRH &= ~(AF_MASK << ((PIN_NR - 8) * 4));      \
                                                  GPIOC->AFRH |= (AltFunc << ((PIN_NR - 8) * 4));       \
                                                 }}

void GPIOC_Init(void);

#endif /* _GPIOC_DRIVER_H */