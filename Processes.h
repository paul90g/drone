#ifndef _PROCESSES_H
#define _PROCESSES_H

#include "Process_1ms.h"
#include "Process_200us.h"
#include "Process_10ms.h"
#include "Process_33ms.h"
#include "Process_100ms.h"

#define TIMER_200US_TIMEOUT     2
#define TIMER_1MS_TIMEOUT       10
#define TIMER_10MS_TIMEOUT      100
#define TIMER_33MS_TIMEOUT      333
#define TIMER_100MS_TIMEOUT     1000
#define TIMER_1S_TIMEOUT        10000

void init_processes(void);

void run_processes(void);

#endif /* _PROCESSES_H */