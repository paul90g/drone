#include "DMA2_Driver.h"
#include "Type.h"
#include "Drivers.h"

void DMA2_Init(){
  Enable_DMA2_Clk();
  
  /* Set Stream7 to USART1 communication */
  DMA2_Stream7_Channel(4);
  DMA2_Stream7_Priority(3);
  DMA2_Stream7_MemPointerIncr();
  DMA2_Stream7_PeriphSize(BYTE_SIZE);
  DMA2_Stream7_MemSize(BYTE_SIZE);
  DMA2_ClearStream7Flags();
  DMA2_Stream7_TransfCompleteIntEn();
  
  /* Set Stream6 to USART6 communication */
  DMA2_Stream6_Channel(5);
  DMA2_Stream6_Priority(3);
  DMA2_Stream6_MemPointerIncr();
  DMA2_Stream6_DataDirection(MEM_TO_PERIPH);
  DMA2_Stream6_PeriphSize(BYTE_SIZE);
  DMA2_Stream6_MemSize(BYTE_SIZE);
  DMA2_ClearStream6Flags();
  DMA2_Stream6_TransfCompleteIntEn();
  
  return;
}