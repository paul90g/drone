#ifndef _RCC_DRIVER_H
#define _RCC_DRIVER_H

#include "mem_map.h"

typedef struct {
  uint32 CR;
  uint32 PLLCFGR;
  uint32 CFGR;
  uint32 CIR;
  uint32 AHB1RSTR;
  uint32 AHB2RSTR;
  uint32 AHB3RSTR;
  uint32 RESERVED1;
  uint32 APB1RSTR;
  uint32 APB2RSTR;
  uint32 RESERVED2[2];
  uint32 AHB1ENR;
  uint32 AHB2ENR;
  uint32 AHB3ENR;
  uint32 RESERVED3;
  uint32 APB1ENR;
  uint32 APB2ENR;
  uint32 RESERVED4[2];
  uint32 AHB1LPENR;
  uint32 AHB2LPENR;
  uint32 AHB3LPENR;
  uint32 RESERVED5;
  uint32 APB1LPENR;
  uint32 APB2LPENR;
  uint32 RESERVED6[2];
  uint32 BDCR;
  uint32 CSR;
  uint32 RESERVED7[2];
  uint32 SSCGR;
  uint32 PLLI2SCFGR;
} RCC_Type;  

#define RCC             ((RCC_Type *) RCC_BASE)

#define PLLRDY_BIT              25
#define PLLON_BIT               24

#define PLLSRC_BIT              22

/* PLL Clock Calculation:
* f(VCO clock) = f(PLL clock input) � (PLLN / PLLM)   = 400MHz
* f(PLL general clock output) = f(VCO clock) / PLLP   = 100MHz
* f(USB OTG FS, SDIO, RNG clock output) = f(VCO clock) / PLLQ   = 50MHz*/

/* PLLQ - Main PLL division factor for USB OTG FS, SDIO and random number generator clocks
* The USB OTG FS requires a 48 MHz clock to work correctly. The SDIO and the
* random number generator need a frequency lower than or equal to 48 MHz to work correctly.
* Value phys: 4 bit
* Value int: 2 - 15 */
#define PLLQ                    4
#define PLLQ_BITMASK            0xF000000
#define PLLQ_BITS_OFFSET        24

/* PLLP - Main PLL (PLL) division factor for main system clock
* The software has to set these bits correctly not to exceed 100 MHz on this domain.
* Value phys: 2 bit
* Value int: 0 => 2 (PLLP division factor)
             1 => 4
             2 => 6
             3 => 8 */
#define PLLP                    1
#define PLLP_BITMASK            0x30000
#define PLLP_BITS_OFFSET        16

/* PLLN - Main PLL multiplication factor for VCO
* These bits can be written only when PLL is disabled.
* The software has to set these bits correctly to ensure that the VCO output frequency is between 100 and 432 MHz.
* Value phys: 9 bit
* Value int: 2 - 432 */
#define PLLN                    200
#define PLLN_BITMASK            0x7FC0
#define PLLN_BITS_OFFSET        6

/* PLLM - Division factor for the main PLL input clock
*  The software has to set these bits correctly to ensure that the VCO input frequency
* ranges from 1 to 2 MHz. It is recommended to select a frequency of 2 MHz to limit PLL jitter.
* Value phys: 6 bit
* Value int: 2 - 63 */
#define PLLM                    8
#define PLLM_BITMASK            0x3F
#define PLLM_BITS_OFFSET        0

/* PPRE2 - APB high-speed prescaler (APB2)
* The software has to set these bits correctly not to exceed 100 MHz on this domain.
0xx: AHB clock not divided
100: AHB clock divided by 2
101: AHB clock divided by 4
110: AHB clock divided by 8
111: AHB clock divided by 16 */
#define PPRE2                   0
#define PPRE2_BITMASK           0xE000
#define PPRE2_BITS_OFFSET       13

/* PPRE1: APB Low speed prescaler (APB1)
* The software has to set these bits correctly not to exceed 50 MHz on this domain.
0xx: AHB clock not divided
100: AHB clock divided by 2
101: AHB clock divided by 4
110: AHB clock divided by 8
111: AHB clock divided by 16 */
#define PPRE1                   4
#define PPRE1_BITMASK           0x1C00
#define PPRE1_BITS_OFFSET       10

/* HPRE - AHB prescaler
* Set and cleared by software to control AHB clock division factor.
0xxx: system clock not divided
1000: system clock divided by 2
1001: system clock divided by 4
1010: system clock divided by 8
1011: system clock divided by 16
1100: system clock divided by 64
1101: system clock divided by 128
1110: system clock divided by 256
1111: system clock divided by 512 */
#define HPRE                    0
#define HPRE_BITMASK            0xF0
#define HPRE_BITS_OFFSET        4

#define SWS_BITS_OFFSET         2  //SWS: System clock switch status
#define SWS_BITMASK             0x0C
#define SW_HSI                  0
#define SW_HSE                  1
#define SW_PLL                  2
#define SW_BITS_OFFSET          0  //SW: System clock switch
#define SW_BITMASK              0x03

/* Function like Macros - bitwise register operations */
#define PLL_Ready()             (RCC->CR & (1 << PLLRDY_BIT))          // check if PLL config has been set
#define PLL_On()                (RCC->CR |= (1 << PLLON_BIT))          // Enable PLL Clock config
#define PLL_Off()               (RCC->CR &= ~(1 << PLLON_BIT))          // Disable PLL Clock config

#define Set_PLL_Source_ExtClk()    (RCC->PLLCFGR |= (1 << PLLSRC_BIT))
#define Set_PLL_Source_IntClk()    (RCC->PLLCFGR &= ~(1 << PLLSRC_BIT))

#define Set_PLLQ()              {RCC->PLLCFGR &= (~PLLQ_BITMASK); RCC->PLLCFGR |= (PLLQ << PLLQ_BITS_OFFSET);}
#define Set_PLLP()              {RCC->PLLCFGR &= (~PLLP_BITMASK); RCC->PLLCFGR |= (PLLP << PLLP_BITS_OFFSET);}
#define Set_PLLN()              {RCC->PLLCFGR &= (~PLLN_BITMASK); RCC->PLLCFGR |= (PLLN << PLLN_BITS_OFFSET);}
#define Set_PLLM()              {RCC->PLLCFGR &= (~PLLM_BITMASK); RCC->PLLCFGR |= (PLLM << PLLM_BITS_OFFSET);}

#define Set_APB_High_Prsc()     {RCC->CFGR &= (~PPRE2_BITMASK); RCC->CFGR |= (PPRE2 << PPRE2_BITS_OFFSET);}
#define Set_APB_Low_Prsc()      {RCC->CFGR &= (~PPRE1_BITMASK); RCC->CFGR |= (PPRE1 << PPRE1_BITS_OFFSET);}
#define Set_AHB_Prsc()          {RCC->CFGR &= (~HPRE_BITMASK); RCC->CFGR |= (HPRE << HPRE_BITS_OFFSET);}

#define Set_HSI_Osc_SysClk()    {RCC->CFGR &= (~SW_BITMASK); RCC->CFGR |= (SW_HSI << SW_BITS_OFFSET);}
#define Set_HSE_Osc_SysClk()    {RCC->CFGR &= (~SW_BITMASK); RCC->CFGR |= (SW_HSE << SW_BITS_OFFSET);}
#define Set_PLL_SysClk()        {RCC->CFGR &= (~SW_BITMASK); RCC->CFGR |= (SW_PLL << SW_BITS_OFFSET);}

#define Get_SysClkSource()      ((RCC->CFGR & SWS_BITMASK) >> SWS_BITS_OFFSET)



#define DMA2EN_BIT      22
#define DMA1EN_BIT      21
#define CRCEN_BIT       12
#define GPIOAEN_BIT     0
#define GPIOBEN_BIT     1
#define GPIOCEN_BIT     2
#define GPIODEN_BIT     3
#define GPIOEEN_BIT     4
#define GPIOHEN_BIT     7

#define PWREN_BIT       28
#define I2C3EN_BIT      23
#define I2C2EN_BIT      22
#define I2C1EN_BIT      21
#define USART2EN_BIT    17
#define SPI3EN_BIT      15
#define SPI2EN_BIT      14
#define WWDGEN_BIT      11
#define TIM5EN_BIT      3
#define TIM4EN_BIT      2
#define TIM3EN_BIT      1
#define TIM2EN_BIT      0

#define SPI5EN_BIT      20
#define TIM11EN_BIT     18
#define TIM10EN_BIT     17
#define TIM9EN_BIT      16
#define SYSCFGEN_BIT    14
#define SPI4EN_BIT      13
#define SPI1EN_BIT      12
#define SDIOEN_BIT      11
#define ADC1EN_BIT      8
#define USART6EN_BIT    5
#define USART1EN_BIT    4
#define TIM1EN_BIT      0

/* Function like Macros - enable peripheral clocks */
#define Enable_DMA2_Clk()       (RCC->AHB1ENR |= (1 << DMA2EN_BIT))
#define Disable_DMA2_Clk()      (RCC->AHB1ENR &= ~(1 << DMA2EN_BIT))
#define Enable_DMA1_Clk()       (RCC->AHB1ENR |= (1 << DMA1EN_BIT))
#define Disable_DMA1_Clk()      (RCC->AHB1ENR &= ~(1 << DMA1EN_BIT))

#define Enable_CRC_Clk()        (RCC->AHB1ENR |= (1 << CRCEN_BIT))
#define Disable_CRC_Clk()       (RCC->AHB1ENR &= ~(1 << CRCEN_BIT))

#define Enable_GPIOA_Clk()      (RCC->AHB1ENR |= (1 << GPIOAEN_BIT))
#define Disable_GPIOA_Clk()     (RCC->AHB1ENR &= ~(1 << GPIOAEN_BIT))
#define Enable_GPIOB_Clk()      (RCC->AHB1ENR |= (1 << GPIOBEN_BIT))
#define Disable_GPIOB_Clk()     (RCC->AHB1ENR &= ~(1 << GPIOBEN_BIT))
#define Enable_GPIOC_Clk()      (RCC->AHB1ENR |= (1 << GPIOCEN_BIT))
#define Disable_GPIOC_Clk()     (RCC->AHB1ENR &= ~(1 << GPIOCEN_BIT))
#define Enable_GPIOD_Clk()      (RCC->AHB1ENR |= (1 << GPIODEN_BIT))
#define Disable_GPIOD_Clk()     (RCC->AHB1ENR &= ~(1 << GPIODEN_BIT))
#define Enable_GPIOE_Clk()      (RCC->AHB1ENR |= (1 << GPIOEEN_BIT))
#define Disable_GPIOE_Clk()     (RCC->AHB1ENR &= ~(1 << GPIOEEN_BIT))
#define Enable_GPIOH_Clk()      (RCC->AHB1ENR |= (1 << GPIOHEN_BIT))
#define Disable_GPIOH_Clk()     (RCC->AHB1ENR &= ~(1 << GPIOHEN_BIT))

#define Enable_PWR_Clk()        (RCC->APB1ENR |= (1 << PWREN_BIT))
#define Disable_PWR_Clk()       (RCC->APB1ENR &= ~(1 << PWREN_BIT))
#define Enable_I2C3_Clk()       (RCC->APB1ENR |= (1 << I2C3EN_BIT))
#define Disable_I2C3_Clk()      (RCC->APB1ENR &= ~(1 << I2C3EN_BIT))
#define Enable_I2C2_Clk()       (RCC->APB1ENR |= (1 << I2C2EN_BIT))
#define Disable_I2C2_Clk()      (RCC->APB1ENR &= ~(1 << I2C2EN_BIT))
#define Enable_I2C1_Clk()       (RCC->APB1ENR |= (1 << I2C1EN_BIT))
#define Disable_I2C1_Clk()      (RCC->APB1ENR &= ~(1 << I2C1EN_BIT))
#define Enable_USART2_Clk()     (RCC->APB1ENR |= (1 << USART2EN_BIT))
#define Disable_USART2_Clk()    (RCC->APB1ENR &= ~(1 << USART2EN_BIT))
#define Enable_SPI3_Clk()       (RCC->APB1ENR |= (1 << SPI3EN_BIT))
#define Disable_SPI3_Clk()      (RCC->APB1ENR &= ~(1 << SPI3EN_BIT))
#define Enable_SPI2_Clk()       (RCC->APB1ENR |= (1 << SPI2EN_BIT))
#define Disable_SPI2_Clk()      (RCC->APB1ENR &= ~(1 << SPI2EN_BIT))
#define Enable_WWDG_Clk()       (RCC->APB1ENR |= (1 << WWDGEN_BIT))
#define Disable_WWDG_Clk()      (RCC->APB1ENR &= ~(1 << WWDGEN_BIT))
#define Enable_TIM5_Clk()       (RCC->APB1ENR |= (1 << TIM5EN_BIT))
#define Disable_TIM5_Clk()      (RCC->APB1ENR &= ~(1 << TIM5EN_BIT))
#define Enable_TIM4_Clk()       (RCC->APB1ENR |= (1 << TIM4EN_BIT))
#define Disable_TIM4_Clk()      (RCC->APB1ENR &= ~(1 << TIM4EN_BIT))
#define Enable_TIM3_Clk()       (RCC->APB1ENR |= (1 << TIM3EN_BIT))
#define Disable_TIM3_Clk()      (RCC->APB1ENR &= ~(1 << TIM3EN_BIT))
#define Enable_TIM2_Clk()       (RCC->APB1ENR |= (1 << TIM2EN_BIT))
#define Disable_TIM2_Clk()      (RCC->APB1ENR &= ~(1 << TIM2EN_BIT))

#define Enable_SPI5_Clk()        (RCC->APB2ENR |= (1 << SPI5EN_BIT))
#define Disable_SPI5_Clk()       (RCC->APB2ENR &= ~(1 << SPI5EN_BIT))
#define Enable_TIM11_Clk()       (RCC->APB2ENR |= (1 << TIM11EN_BIT))
#define Disable_TIM11_Clk()      (RCC->APB2ENR &= ~(1 << TIM11EN_BIT))
#define Enable_TIM10_Clk()       (RCC->APB2ENR |= (1 << TIM10EN_BIT))
#define Disable_TIM10_Clk()      (RCC->APB2ENR &= ~(1 << TIM10EN_BIT))
#define Enable_TIM9_Clk()        (RCC->APB2ENR |= (1 << TIM9EN_BIT))
#define Disable_TIM9_Clk()       (RCC->APB2ENR &= ~(1 << TIM9EN_BIT))
#define Enable_SYSCFGEN_Clk()    (RCC->APB2ENR |= (1 << SYSCFGEN_BIT))
#define Disable_SYSCFGEN_Clk()   (RCC->APB2ENR &= ~(1 << SYSCFGEN_BIT))
#define Enable_SPI4_Clk()        (RCC->APB2ENR |= (1 << SPI4EN_BIT))
#define Disable_SPI4_Clk()       (RCC->APB2ENR &= ~(1 << SPI4EN_BIT))
#define Enable_SPI1_Clk()        (RCC->APB2ENR |= (1 << SPI1EN_BIT))
#define Disable_SPI1_Clk()       (RCC->APB2ENR &= ~(1 << SPI1EN_BIT))
#define Enable_SDIO_Clk()        (RCC->APB2ENR |= (1 << SDIOEN_BIT))
#define Disable_SDIO_Clk()       (RCC->APB2ENR &= ~(1 << SDIOEN_BIT))
#define Enable_ADC1_Clk()        (RCC->APB2ENR |= (1 << ADC1EN_BIT))
#define Disable_ADC1_Clk()       (RCC->APB2ENR &= ~(1 << ADC1EN_BIT))
#define Enable_USART6_Clk()      (RCC->APB2ENR |= (1 << USART6EN_BIT))
#define Disable_USART6_Clk()     (RCC->APB2ENR &= ~(1 << USART6EN_BIT))
#define Enable_USART1_Clk()      (RCC->APB2ENR |= (1 << USART1EN_BIT))
#define Disable_USART1_Clk()     (RCC->APB2ENR &= ~(1 << USART1EN_BIT))
#define Enable_TIM1_Clk()        (RCC->APB2ENR |= (1 << TIM1EN_BIT))
#define Disable_TIM1_Clk()       (RCC->APB2ENR &= ~(1 << TIM1EN_BIT))

void RCC_Init(void);

#endif /* _RCC_DRIVER_H */