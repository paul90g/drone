#ifndef _GPIO_TYPE_H
#define _GPIO_TYPE_H

#include "type.h"

typedef struct {
  uint32 MODER;    /*!> GPIO port mode register,               Address offset: 0x00      */
  uint32 OTYPER;   /*!> GPIO port output type register,        Address offset: 0x04      */
  uint32 OSPEEDR;  /*!> GPIO port output speed register,       Address offset: 0x08      */
  uint32 PUPDR;    /*!> GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
  uint32 IDR;      /*!> GPIO port input data register,         Address offset: 0x10      */
  uint32 ODR;      /*!> GPIO port output data register,        Address offset: 0x14      */
  uint32 BSRR;     /*!> GPIO port bit set/reset low register,  Address offset: 0x18      */
  uint32 LCKR;     /*!> GPIO port configuration lock register, Address offset: 0x1C      */
  uint32 AFRL;     /*!> GPIO alternate function register low,  Address offset: 0x20      */
  uint32 AFRH;     /*!> GPIO alternate function register high, Address offset: 0x24      */
} GPIO_TypeDef;

/* Port pins */
#define PIN0     0
#define PIN1     1
#define PIN2     2
#define PIN3     3
#define PIN4     4
#define PIN5     5
#define PIN6     6
#define PIN7     7
#define PIN8     8
#define PIN9     9
#define PIN10    10 
#define PIN11    11
#define PIN12    12
#define PIN13    13
#define PIN14    14
#define PIN15    15
/* end Port pins */

/* Alternate functions */
#define AF0     0       // system
#define AF1     1       // TIM1/TIM2
#define AF2     2       // TIM3..5
#define AF3     3       // TIM9..11
#define AF4     4       // I2C1..3
#define AF5     5       // SPI1..4
#define AF6     6       // SPI3..5
#define AF7     7       // USART1..2
#define AF8     8       // USART6
#define AF9     9       // I2C2..3
#define AF10    10      // OTG_FS
#define AF11    11      // 
#define AF12    12      // SDIO
#define AF13    13      //
#define AF14    14      //
#define AF15    15      // EVENTOUT
/* end Alt functions   */

#define LOW_SPEED       0
#define MEDIUM_SPEED    1
#define FAST_SPEED      2
#define HIGH_SPEED      3
#define SPEED_BITMASK   0x3

#endif /* GPIO_TYPE_H */