#include "type.h"
#include "stm32f411re.h"
#include "mcu_init.h"
#include "Drivers.h"
#include "String.h"
#include "Processes.h"

gyro_type gyro_data;
accel_type accel_data;

int main() {
  mcu_init();
  
  init_processes();
  run_processes();
}

void TIM3_IRQHandler(){
  TIM3_ClearStatusReg();
  //ADC_StartConversion();
  return;
}

void ADC1_2_IRQHandler(){
  ADC_Clear_IntFlag();
  //TIM3_SetCaptureCompare1((ADC_Read() * 0xC0) / (0x1007));
  return;
}

void DMA2_Stream7_IRQHandler(){
  DMA2_ClearStream7Flags();
  return;
}

void DMA2_Stream6_IRQHandler(){
  DMA2_ClearStream6Flags();
  return;
}