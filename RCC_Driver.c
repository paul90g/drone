#include "Drivers.h"
#include "RCC_Driver.h"

void RCC_Init(){
  
  PLL_Off();
  Set_PLL_Source_IntClk();
  Set_PLLQ();
  Set_PLLP();
  Set_PLLN();
  Set_PLLM();
  
  Set_Flash_Latency(3);         // set flash read latency according to the high speed sys clk
  
  Set_AHB_Prsc();
  Set_APB_High_Prsc();
  Set_APB_Low_Prsc();
  Set_PLL_SysClk();
  
  PLL_On();

  while(!PLL_Ready());                  // wait until the PLL is ready
  while(SW_PLL != Get_SysClkSource());  // wait until the system clk source is set to PLL
  
  return;
}