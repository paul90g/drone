#include "USART1_Driver.h"
#include "Drivers.h"
#include "Type.h"
#include "String.h"
#include "config.h"

void USART1_Init(){
  Enable_USART1_Clk();
  USART1_SetOversampling16();
  USART1_WordLength8Bit();
  USART1_EnableTransmitter();
  USART1_EnableReceiver();
  USART1_EnableDMATransm();
  USART1_SetStopBits(ONE_STOP_BIT);
  USART1_SetBaud(USART1_BAUD);
  USART1_EnableRXNEInterrupt();
  USART1_Enable();
  USART1_ClearStatusFlags();
  return;
}

void USART1_SendData(const uint8 *data){
  DMA2_Stream7_DataDirection(MEM_TO_PERIPH);
  DMA2_Stream7_PeriphAddress((uint32)&USART1->DR);
  DMA2_Stream7_Mem0Address((uint32)data);
  DMA2_Stream7_NrOfData(strlen(data));
  DMA2_Stream7_Enable();
  return;
}

static void USART1_SetBaud(uint32 baud){
#ifdef F_CPU
  float32 udivf;
  uint32 udivm;
  USART1_ResetBaudRegister();
  if(0 < (USART1->CR1 & OVER8_BIT)){
    // oversampling by 8
    udivf = (float32)F_CPU / (float32)(baud * 8);
    udivm = F_CPU / (baud * 8);
    udivf -= udivm;
    USART1_SetBaud_Register(((udivm << DIV_Mantissa_BITS_OFFSET) & DIV_Mantissa_BITMASK) | (uint8)(udivf * 8.0));
  } else {
    // oversampling by 16
    udivf = (float32)F_CPU / (float32)(baud * 16);
    udivm = F_CPU / (baud * 16);
    udivf -= udivm;
    USART1_SetBaud_Register(((udivm << DIV_Mantissa_BITS_OFFSET) & DIV_Mantissa_BITMASK) | (uint8)(udivf * 16.0));
  }
#else
#error "F_CPU not defined! Needed to set baud rate"
#endif
  return;
}