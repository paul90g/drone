#include "Type.h"
#include "String.h"
#include "Drivers.h"
#include "ESP8266_Driver.h"
#include "ESP8266_Config.h"

#include "Timers.h"
#include "PROT_Parser.h"

uint8 command_buff[COMMAND_BUFFER_SIZE];
static uint8 data_buff[DATA_BUFFER_SIZE];
static volatile uint8 it;

clientType client;

static ESP_StateType ESP_fsm;
static ESP_SetUpState ESP_SetupFsm;
ServerState Server_fsm;

Timer16bit espTimer;
Timer16bit espRxTimer;

void ESP8266_Init(){
  // reset pin, active LOW
  GPIOA_PinMode(PIN5 , PIN_MODE_OUTPUT);
  GPIOA_Pin_Speed(PIN5, FAST_SPEED);
  GPIOA_Pin_Reset(PIN5);

  client.ID = 0u;
  client.isConnected = FALSE;
  ESP_fsm = ESP_Init;
  ESP_SetupFsm = ESP_WiFiMode;
  Server_fsm = Init;
  init16bitTimer(&espTimer);
  init16bitTimer(&espRxTimer);
  
  return;
}

void ESP8266_Reset(){
  GPIOA_Pin_Reset(PIN5);

  ESP_fsm = ESP_Init;
  ESP_SetupFsm = ESP_WiFiMode;
  Server_fsm = Init;
  init16bitTimer(&espTimer);
  init16bitTimer(&espRxTimer);
  
  return;
}

void ESP8266_Deinit(){
  GPIOA_Pin_Reset(PIN5);
  it = 0;
  ESP_fsm = ESP_Init;
  ESP_SetupFsm = ESP_WiFiMode;
  Server_fsm = Idle;
  return;
}

void ESP8266_Tick(){
  switch(Server_fsm){
  case Init:
    {
      ESP_InitServer();
    }
    break;
  case WaitClient:
    {
      if(Timer_Passed == getState16bitTimer(&espTimer, (uint16)ESP_WaitClientDelay)){
        if(0 < it){
          uint8 *temp = strstr(data_buff, "CONNECT");
          if(NULL_PTR != temp){
            Server_fsm = Idle;
            init16bitTimer(&espRxTimer);
            client.ID = *(temp-2) - 48;
            client.isConnected = TRUE;
          }
          clearBuffer();
        } else {
          init16bitTimer(&espTimer);
        }
      }
    }
    break;
  case Idle:
    {
      if(Timer_Passed == getStateNoStart16bitTimer(&espRxTimer)){
        if(0 < it){
          if(NULL_PTR == strstr(data_buff, "OK\r\n>")){
            if(NULL_PTR == strstr(data_buff, "Recv")){
              uint8 *message = getMessage(data_buff);
              /* parser ! TODO */
              decodeMessage(message);
              clearBuffer();
            } else {
              clearBuffer();
              init16bitTimer(&espRxTimer);
            }
          } else {
            // send data to client
            clearBuffer();
            ESP_Command(command_buff);
          }
        }
        init16bitTimer(&espRxTimer);
      }
    }
    break;
  case SendResponse:
    {
      if(Timer_Passed == getStateNoStart16bitTimer(&espRxTimer)){
        if(NULL_PTR != strstr(data_buff, "OK")){
          ESP_Command(command_buff);
          Server_fsm = Idle;
          clearBuffer();
        } else {
          Server_fsm = Idle;
        }
      }
    }
    break;
  default:
    {
      Server_fsm = Init;
    }
  }
  return;
}

static inline void ESP_InitServer(){
  if(Timer_Passed == getState16bitTimer(&espTimer, (uint16)ESP_InitCommDelay)){
    switch(ESP_fsm){
    case ESP_Init:
      {
        GPIOA_Pin_Set(PIN5);
        clearBuffer();
        ESP_fsm = ESP_Check;
        init16bitTimer(&espTimer);
        init16bitTimer(&espRxTimer);
      }
      break;
    case ESP_Check:
      {
        uint8 *str_buff;
        *command_buff = '\0';
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_TEST_COMMAND);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
        ESP_Command(str_buff);
        init16bitTimer(&espRxTimer);
        clearBuffer();
        ESP_fsm = ESP_Start;
        init16bitTimer(&espTimer);
      }
      break;
    case ESP_Start:
      {
        if(NULL_PTR != strstr(data_buff, "OK")){
          clearBuffer();
          ESP_fsm = ESP_SetUp;
          init16bitTimer(&espTimer);
        } else {
          ESP8266_Reset();
        }
      }
      break;
    case ESP_SetUp:
      {
        ESP_SetupFSM();
      }
      break;
    default:
      {
        ESP_fsm = ESP_Init;
        init16bitTimer(&espTimer);
      }
    }
  }
  return;
}

static inline void ESP_SetupFSM(){
  switch(ESP_SetupFsm){
  case ESP_WiFiMode:
    {
      uint8 *str_buff;
      *command_buff = '\0';
      str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_BASE_COMMAND);
      str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_WIFI_MODE);
      str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, "=");
      str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_MODE);
      str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
      ESP_Command(str_buff);
      init16bitTimer(&espRxTimer);
      ESP_SetupFsm = ESP_SetAP;
    }
    break;
  case ESP_SetAP:
    {
      if(NULL_PTR != strstr(data_buff, "OK")){
        uint8 *str_buff;
        *command_buff = '\0';
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_BASE_COMMAND);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_SET_AP);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, "=");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, SSID);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ",");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, PASSWORD);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ",");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_CHANNEL);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ",");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_ENCRYPTION);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
        ESP_Command(str_buff);
        init16bitTimer(&espRxTimer);
        ESP_SetupFsm = ESP_SetMultipleConn;
      } else {
        ESP8266_Reset();
      }
    }
    break;
  case ESP_SetMultipleConn:
    {
      if(NULL_PTR != strstr(data_buff, "OK")){
        uint8 *str_buff;
        *command_buff = '\0';
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_BASE_COMMAND);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_SET_MULTIPLE_CONN);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, "=");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_MULTIPLE_CONNECTION);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
        ESP_Command(str_buff);
        init16bitTimer(&espRxTimer);
        ESP_SetupFsm = ESP_SetServer;
      } else {
        ESP8266_Reset();
      }
    }
    break;
  case ESP_SetServer:
    {
      if(NULL_PTR != strstr(data_buff, "OK")){
        uint8 *str_buff;
        *command_buff = '\0';
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_BASE_COMMAND);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_SET_SERVER);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, "=");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_SERVER_ON);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ",");
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, WIFI_SERVER_PORT);
        str_buff = strncat(command_buff, COMMAND_BUFFER_SIZE, ESP_COMMAND_END);
        ESP_Command(str_buff);
        init16bitTimer(&espRxTimer);
        ESP_SetupFsm = ESP_CheckServer;
      } else {
        ESP8266_Reset();
      }
    }
    break;
  case ESP_CheckServer:
    {
      if(NULL_PTR != strstr(data_buff, "OK")){
        init16bitTimer(&espRxTimer);
        Server_fsm = WaitClient;
      } else {
        ESP8266_Reset();
      }
    }
    break;
  default:
    {
      ESP8266_Reset();
    }
  }
  clearBuffer();
  init16bitTimer(&espTimer);
  return;
}

void clearBuffer(){
  for(it=0;it<DATA_BUFFER_SIZE;it++){
    data_buff[it] = 0x00;
  }
  it = 0;
  return;
}

static uint8 *getMessage(uint8 *data){
  uint8 *message = strstr(data, ESP_RECEIVE_DATA_HEAD);
  if(NULL_PTR != message)
    while(':' != *message)
      message++;
  message++;
  return message;
}

void USART1_IRQHandler(){
  
  if(Timer_Running == getState16bitTimer(&espRxTimer, (uint16)ESP_RxDelay)){
    if(it < DATA_BUFFER_SIZE){
      data_buff[it++] = USART1_ReadData();
    }
  }

  USART1_ClearStatusFlags();
  return;
}