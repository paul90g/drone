#include "TIM3_Driver.h"

void TIM3_Init(){
  Enable_TIM3_Clk();
  
  // enable PWM pin
  GPIOA_PinMode(PIN6 , PIN_MODE_ALT_FUNC);
  GPIOA_Pin_SetAltFunc(PIN6, AF2);
  GPIOA_Pin_Speed(PIN6, HIGH_SPEED);
  GPIOA_PinMode(PIN7 , PIN_MODE_ALT_FUNC);
  GPIOA_Pin_SetAltFunc(PIN7, AF2);
  GPIOA_Pin_Speed(PIN7, HIGH_SPEED);
  
  TIM3_SelectOutputCompare();
  TIM3_SetPrescaler(0x270F);
  TIM3_SetAutoReload(0xC7);
  TIM3_AutoReloadBuffered();
  
  TIM3_Channel1PreloadEnable();
  TIM3_Ch1OutputEnable();
  TIM3_Channel1OutputCompareMode(OC_PWM1);
  TIM3_SetCaptureCompare1(0x63);
  
  TIM3_Channel2PreloadEnable();
  TIM3_Ch2OutputEnable();
  TIM3_Channel2OutputCompareMode(OC_PWM1);
  TIM3_SetCaptureCompare2(0xA0);
  
  TIM3_UpdateIntEnable();
  
  TIM3_UpdateEvent();
  TIM3_CounterEnable();
  
  return;
}