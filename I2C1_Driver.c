#include "I2C1_Driver.h"
#include "Type.h"
#include "Drivers.h"

enum I2C_state{
  START,
  ADDR_SENT,
  TRANSMIT_REG_SENT,
  TRANSMIT_DATA_SENT
} send_reg_data_fsm;

enum I2C_write_byte_state{
  START_BYTE,
  ADDR_SENT_BYTE
} send_byte_fsm;

enum I2C_read_DMA_state{
  START_DMA,
  ADDR_DMA_SENT
} read_dma_fsm;

enum I2C_read_byte_state{
  START_RD_BYTE,
  ADDR_RD_BYTE_SENT,
  DATA_RD_BYTE
} read_byte_fsm;
  

enum I2C_request{
  IDLE,
  WRITE_BYTE,
  WRITE_REG_DATA,
  READ_DMA,
  READ_BYTE
} request;

struct {
  boolean isEmpty;
  enum I2C_request request;
  uint8 addr_m, reg_m, data_m, nr_of_bytes_m;
  uint8 data_buff[16];
  void (*I2C_Handler_m)(uint8*, uint8);
} I2C_queue[MAX_QUEUE];


static uint8 addr_m, reg_m, data_m, nr_of_bytes_m;
static uint8 curr_pos;
static volatile uint8 free_req;

uint8 *data_buff;

boolean I2C1_ReserveReq(uint8 nr_of_reserved_requests)
{
  if(free_req >= nr_of_reserved_requests)
  {
    return TRUE;
  }
  return FALSE;
}

boolean I2C1_WriteReg(uint8 addr, uint8 reg, uint8 data)
{
  for(uint8 i=0; i<MAX_QUEUE; i++)
  {
    if(I2C_queue[i].isEmpty)
    {
      I2C_queue[i].isEmpty = FALSE;
      I2C_queue[i].request = WRITE_REG_DATA;
      I2C_queue[i].addr_m = addr;
      I2C_queue[i].reg_m = reg;
      I2C_queue[i].data_m = data;
      free_req--;
      return TRUE;
    }
  }
  return FALSE;
}

boolean I2C1_ReadByteStream(uint8 addr, uint8 nr_of_bytes, void (*I2C_Handler)(uint8*, uint8))
{
  for(uint8 i=0; i<MAX_QUEUE; i++)
  {
    if(I2C_queue[i].isEmpty)
    {
      I2C_queue[i].isEmpty = FALSE;
      I2C_queue[i].request = READ_DMA;
      I2C_queue[i].addr_m = addr;
      I2C_queue[i].nr_of_bytes_m = nr_of_bytes;
      I2C_queue[i].I2C_Handler_m = I2C_Handler;
      free_req--;
      return TRUE;
    }
  }
  return FALSE;
}

boolean I2C1_WriteByte(uint8 addr, uint8 data)
{
  for(uint8 i=0; i<MAX_QUEUE; i++)
  {
    if(I2C_queue[i].isEmpty)
    {
      I2C_queue[i].isEmpty = FALSE;
      I2C_queue[i].request = WRITE_BYTE;
      I2C_queue[i].addr_m = addr;
      I2C_queue[i].data_m = data;
      free_req--;
      return TRUE;
    }
  }
  return FALSE;
}

boolean I2C1_ReadByte(uint8 addr, void (*I2C_Handler)(uint8*, uint8))
{
  for(uint8 i=0; i<MAX_QUEUE; i++)
  {
    if(I2C_queue[i].isEmpty)
    {
      I2C_queue[i].isEmpty = FALSE;
      I2C_queue[i].request = READ_BYTE;
      I2C_queue[i].addr_m = addr;
      I2C_queue[i].I2C_Handler_m = I2C_Handler;
      free_req--;
      return TRUE;
    }
  }
  return FALSE;
}

void I2C1_Init(void){

  // SCL
  GPIOB_PinMode(PIN6, PIN_MODE_ALT_FUNC);
  GPIOB_Pin_SetAltFunc(PIN6, AF4);
  GPIOB_Pin_Speed(PIN6, FAST_SPEED);
  GPIOB_PinOutputType(PIN6, PIN_OUTPUT_OPEN_DRAIN);
  GPIOB_Pin_PullUp_PullDown(PIN6, PIN_PULL_UP);
  // SDA
  GPIOB_PinMode(PIN9, PIN_MODE_ALT_FUNC);
  GPIOB_Pin_SetAltFunc(PIN9, AF4);
  GPIOB_Pin_Speed(PIN9, FAST_SPEED);
  GPIOB_PinOutputType(PIN9, PIN_OUTPUT_OPEN_DRAIN);
  GPIOB_Pin_PullUp_PullDown(PIN9, PIN_PULL_UP);
  
  Enable_I2C1_Clk();
  I2C1_Disable();
  I2C1_ClockStretchEnable();
  I2C1_BuffIntEnable();
  I2C1_EvntIntEnable();
  I2C1_SetPeriphClock(0x32);
  I2C1_FmEnable();
  I2C1_SetDuty16_9();
  I2C1_AnalogFilterEnable();
  I2C1_SetCCR(0x05);
  I2C1_SetTrise(0x10);
  
  I2C1_SetAddMode(ADDMODE_7BIT);
  I2C1_DmaEnable();
  
  I2C1_Enable();
  
  free_req = MAX_QUEUE;
  request = IDLE;
  I2C1_InitQueue();
  
  return;
}

static void I2C1_InitQueue()
{
  for(uint8 i=0; i < MAX_QUEUE; i++)
  {
    I2C_queue[i].isEmpty = TRUE;
    I2C_queue[i].addr_m = 0;
    I2C_queue[i].data_m = 0;
    I2C_queue[i].nr_of_bytes_m = 0;
    I2C_queue[i].reg_m = 0;
    I2C_queue[i].request = IDLE;
  }
  return;
}

boolean I2C1_IsBusy()
{
  if(IDLE == request)
    return FALSE;
  else
    return TRUE;
}

// to be called every 1ms
void I2C1_Tick()
{
  if(IDLE == request)
  {
    for(uint8 i=0; i<MAX_QUEUE; i++)
    {
      if ( !I2C_queue[i].isEmpty )
      {
        request = I2C_queue[i].request;
        switch(request)
        {
        case WRITE_BYTE:
          {
            addr_m = I2C_queue[i].addr_m;
            data_m = I2C_queue[i].data_m;
            send_byte_fsm = START_BYTE;
            I2C1_StartBit();
            break;
          }
        case WRITE_REG_DATA:
          {
            addr_m = I2C_queue[i].addr_m;
            reg_m = I2C_queue[i].reg_m;
            data_m = I2C_queue[i].data_m;
            send_reg_data_fsm = START;
            I2C1_StartBit();
            break;
          }
        case READ_BYTE:
          {
            curr_pos = i;
            addr_m = I2C_queue[i].addr_m;
            read_byte_fsm = START_RD_BYTE;
            I2C1_AcknowledgeEnable();
            I2C1_StartBit();
            break;
          }
        case READ_DMA:
          {
            curr_pos = i;
            addr_m = I2C_queue[i].addr_m;
            nr_of_bytes_m = I2C_queue[i].nr_of_bytes_m;
            data_buff = I2C_queue[i].data_buff;
            read_dma_fsm = START_DMA;
            I2C1_AcknowledgeEnable();
            I2C1_StartBit();
            break;
          }
        }
        I2C_queue[i].isEmpty = TRUE;
        return;
      }
    }
  }
  return;
}

void I2C1_EV_IRQHandler(){
  switch(request)
  {
  case WRITE_REG_DATA:
    {
      switch(send_reg_data_fsm)
      {
      case START:
        {
          if (I2C1->SR1 & I2C_SB_BIT)
          {
            I2C1_WriteData(RequestWrite7BitAddr(addr_m));
            send_reg_data_fsm = ADDR_SENT;
          }
          break;
        }
      case ADDR_SENT:
        {
          if (I2C1->SR1 & (I2C_ADDR_BIT | I2C_TXE_BIT))
          {
            I2C1_ClearAddrFlag();
            I2C1_WriteData(reg_m);
            send_reg_data_fsm = TRANSMIT_REG_SENT;
          }
          break;
        }
      case TRANSMIT_REG_SENT:
        {
          if (I2C1->SR1 & I2C_TXE_BIT)
          {
            I2C1_WriteData(data_m);
            send_reg_data_fsm = TRANSMIT_DATA_SENT;
          }
          break;
        }
      case TRANSMIT_DATA_SENT:
        {
          if (I2C1->SR1 & I2C_TXE_BIT)
          {
            I2C1_StopBit();
            request = IDLE;
            free_req++;
          }
          break;
        }
      }
      break;
    }
  case WRITE_BYTE:
    {
      switch(send_byte_fsm)
      {
      case START_BYTE:
        {
          if (I2C1->SR1 & I2C_SB_BIT)
          {
            I2C1_WriteData(RequestWrite7BitAddr(addr_m));
            send_byte_fsm = ADDR_SENT_BYTE;
          }
          break;
        }
      case ADDR_SENT_BYTE:
        {
          if (I2C1->SR1 & (I2C_ADDR_BIT | I2C_TXE_BIT))
          {
            I2C1_ClearAddrFlag();
            I2C1_WriteData(data_m);
            I2C1_StopBit();
            request = IDLE;
            free_req++;
          }
          break;
        }
      }
      break;
    }
  case READ_BYTE:
    {
      switch(read_byte_fsm)
      {
      case START_RD_BYTE:
        {
          if(I2C1->SR1 & I2C_SB_BIT)
          {
            I2C1_WriteData(RequestRead7BitAddr(addr_m));
            read_byte_fsm = ADDR_RD_BYTE_SENT;
          }
          break;
        }
      case ADDR_RD_BYTE_SENT:
        {
          if(I2C1->SR1 & I2C_ADDR_BIT)
          {
            I2C1_AcknowledgeDisable();
            I2C1_ClearAddrFlag();
            read_byte_fsm = DATA_RD_BYTE;
          }
          break;
        }
      case DATA_RD_BYTE:
        {
          if(I2C1->SR1 & I2C_RXNE_BIT)
          {
            I2C_queue[curr_pos].data_m = I2C1_ReadData();
            I2C1_StopBit();
            (*I2C_queue[curr_pos].I2C_Handler_m)(&I2C_queue[curr_pos].data_m, 1);
            request = IDLE;
            free_req++;
          }
          break;
        }
      }
      break;
    }
  case READ_DMA:
    {
      switch(read_dma_fsm)
      {
      case START_DMA:
        {
          if (I2C1->SR1 & I2C_SB_BIT)
          {
            I2C1_WriteData(RequestRead7BitAddr(addr_m));
            read_dma_fsm = ADDR_DMA_SENT;
          }
          break;
        }
      case ADDR_DMA_SENT:
        {
          if (I2C1->SR1 & (I2C_ADDR_BIT | I2C_TXE_BIT))
          {
            I2C1_ClearAddrFlag();
            I2C1_BuffIntDisable();
            I2C1_DMALastBitEnable();
            DMA1_Stream0_Mem0Address((uint32)data_buff);
            DMA1_Stream0_NrOfData(nr_of_bytes_m);
            DMA1_Stream0_Enable();
          }
          break;
        }
      }
      break;
    }
  }
  return;
}

void DMA1_Stream0_IRQHandler()
{
  DMA1_ClearStream0Flags();
  (*I2C_queue[curr_pos].I2C_Handler_m)(I2C_queue[curr_pos].data_buff, I2C_queue[curr_pos].nr_of_bytes_m);
  request = IDLE;
  free_req++;
  I2C1_BuffIntEnable();
  return;
}