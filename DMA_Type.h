#ifndef _DMA_TYPE_H
#define _DMA_TYPE_H

#include "Type.h"

typedef struct{
  uint32 LISR;          // Low Interrupt Status Register
  uint32 HISR;          // Hight Interrupt Status Register
  uint32 LIFCR;         // Low Interrupt Flag Clear Register
  uint32 HIFCR;         // High Interrupt Flag Clear Register
  uint32 S0CR;          // Stream 0 Configuration Register
  uint32 S0NDTR;        // Stream 0 Number of Data Register
  uint32 S0PAR;         // Stream 0 Peripheral Address Register
  uint32 S0M0AR;        // Stream 0 Memory 0 Address Register
  uint32 S0M1AR;        // Stream 0 Memory 1 Address Register
  uint32 S0FCR;         // Stream 0 FIFO Control Register
  uint32 S1CR;
  uint32 S1NDTR;
  uint32 S1PAR;
  uint32 S1M0AR;
  uint32 S1M1AR;
  uint32 S1FCR;
  uint32 S2CR;
  uint32 S2NDTR;
  uint32 S2PAR;
  uint32 S2M0AR;
  uint32 S2M1AR;
  uint32 S2FCR;
  uint32 S3CR;
  uint32 S3NDTR;
  uint32 S3PAR;
  uint32 S3M0AR;
  uint32 S3M1AR;
  uint32 S3FCR;
  uint32 S4CR;
  uint32 S4NDTR;
  uint32 S4PAR;
  uint32 S4M0AR;
  uint32 S4M1AR;
  uint32 S4FCR;
  uint32 S5CR;
  uint32 S5NDTR;
  uint32 S5PAR;
  uint32 S5M0AR;
  uint32 S5M1AR;
  uint32 S5FCR;
  uint32 S6CR;
  uint32 S6NDTR;
  uint32 S6PAR;
  uint32 S6M0AR;
  uint32 S6M1AR;
  uint32 S6FCR;
  uint32 S7CR;
  uint32 S7NDTR;
  uint32 S7PAR;
  uint32 S7M0AR;
  uint32 S7M1AR;
  uint32 S7FCR;
} DMA_Type;

#define BYTE_SIZE       0
#define HALF_WORD_SIZE  1
#define WORD_SIZE       2

#define PERIPH_TO_MEM   0
#define MEM_TO_PERIPH   1
#define MEM_TO_MEM      2

/* LISR */
#define TCIF0_BIT       (1 << 5)
#define HTIF0_BIT       (1 << 4)
#define TEIF0_BIT       (1 << 3)
#define DMEIF0_BIT      (1 << 2)
#define FEIF0_BIT       (1 << 0)
#define TCIF1_BIT       (1 << 11)
#define HTIF1_BIT       (1 << 10)
#define TEIF1_BIT       (1 << 9)
#define DMEIF1_BIT      (1 << 8)
#define FEIF1_BIT       (1 << 6)
#define TCIF2_BIT       (1 << 21)
#define HTIF2_BIT       (1 << 20)
#define TEIF2_BIT       (1 << 19)
#define DMEIF2_BIT      (1 << 18)
#define FEIF2_BIT       (1 << 16)
#define TCIF3_BIT       (1 << 27)
#define HTIF3_BIT       (1 << 26)
#define TEIF3_BIT       (1 << 25)
#define DMEIF3_BIT      (1 << 24)
#define FEIF3_BIT       (1 << 22)

/* HISR */
#define TCIF4_BIT       (1 << 5)
#define HTIF4_BIT       (1 << 4)
#define TEIF4_BIT       (1 << 3)
#define DMEIF4_BIT      (1 << 2)
#define FEIF4_BIT       (1 << 0)
#define TCIF5_BIT       (1 << 11)
#define HTIF5_BIT       (1 << 10)
#define TEIF5_BIT       (1 << 9)
#define DMEIF5_BIT      (1 << 8)
#define FEIF5_BIT       (1 << 6)
#define TCIF6_BIT       (1 << 21)
#define HTIF6_BIT       (1 << 20)
#define TEIF6_BIT       (1 << 19)
#define DMEIF6_BIT      (1 << 18)
#define FEIF6_BIT       (1 << 16)
#define TCIF7_BIT       (1 << 27)
#define HTIF7_BIT       (1 << 26)
#define TEIF7_BIT       (1 << 25)
#define DMEIF7_BIT      (1 << 24)
#define FEIF7_BIT       (1 << 22)

/* LIFCR */
#define STREAM0_CLEAR_BITMASK   0x000003D
#define STREAM1_CLEAR_BITMASK   0x0000F40
#define STREAM2_CLEAR_BITMASK   0x03D0000
#define STREAM3_CLEAR_BITMASK   0xF400000

/* HIFCR */
#define STREAM4_CLEAR_BITMASK   0x000003D
#define STREAM5_CLEAR_BITMASK   0x0000F40
#define STREAM6_CLEAR_BITMASK   0x03D0000
#define STREAM7_CLEAR_BITMASK   0xF400000

/* Stream x Configuration Register */
#define CHSEL_BITS_OFFSET       25
#define CHSEL_BITMASK           0x0E000000
#define PL_BITS_OFFSET          16
#define PL_BITMASK              0x30000
#define MSIZE_BITS_OFFSET       13
#define MSIZE_BITMASK           0x6000
#define PSIZE_BITS_OFFSET       11
#define PSIZE_BITMASK           0x1800
#define MINC_BIT                (1 << 10)
#define PINC_BIT                (1 << 9)
#define DIR_BITS_OFFSET         6
#define DIR_BITMASK             0xC0
#define PFCTRL_BIT              (1 << 5)
#define TCIE_DMA_BIT            (1 << 4)
#define HTIE_BIT                (1 << 3)
#define TEIE_BIT                (1 << 2)
#define DMEIE_BIT               (1 << 1)
#define EN_BIT                  (1 << 0)

/* Stream x Number of Data Register */
#define NDT_BITMASK             0xFFFF

/* Stream x Peripheral Address Register */
#define PAR_BITMASK             0xFFFFFFFF

/* Stream x Memory 0 Address Register */
#define M0AR_BITMASK            0xFFFFFFFF

/* Stream x Memory 1 Address Register */
#define M1AR_BITMASK            0xFFFFFFFF

/* Stream x FIFO Control Register */
#define FEIE_BIT                (1 << 7)
#define FS_BITS_OFFSET          3
#define FS_BITMASK              0x38
#define DMDIS_BIT               2
#define FTH_BITS_OFFSET         0
#define FTH_BITMASK             0x3

#endif /* _DMA_TYPE_H */