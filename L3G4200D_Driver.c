#include "L3G4200D_Driver.h"
#include "String.h"

static gyro_union gyro_local;
static gyro_union gyro_local_prev;
static gyro_type gyro_hp_local;
static boolean hasData;

static const float high_pass_const = 0.75;

struct xyz_calib{
  int16 x,y,z;
} xyz_offset;

void L3G4200D_Init()
{
  hasData = FALSE;
  I2C1_WriteReg(L3G4200D_ADDR, L3G4200D_CTRL_REG1, 0x0F);
  I2C1_WriteReg(L3G4200D_ADDR, L3G4200D_CTRL_REG2, 0x06);
  I2C1_WriteReg(L3G4200D_ADDR, L3G4200D_CTRL_REG3, 0x04);
  I2C1_WriteReg(L3G4200D_ADDR, L3G4200D_CTRL_REG4, 0x10);
  I2C1_WriteReg(L3G4200D_ADDR, L3G4200D_CTRL_REG5, 0x10);
  
  xyz_offset.x = 0;
  xyz_offset.y = 0;
  xyz_offset.z = 0;
  
  gyro_hp_local.x = 0.0;
  gyro_hp_local.y = 0.0;
  gyro_hp_local.z = 0.0;
  gyro_local_prev.xyz_data.x = 0;
  gyro_local_prev.xyz_data.y = 0;
  gyro_local_prev.xyz_data.z = 0;
  
  calibrate_gyro();
  
  return;
}

static void calibrate_gyro()
{
  int32 x,y,z;
  x = 0;
  y = 0;
  z = 0;
  for (uint8 i=0; i < CALIBRATION_SAMPLES; i++)
  {
    I2C1_WriteByte(L3G4200D_ADDR, L3G4200D_OUT_X_L | 0x80);
    I2C1_ReadByteStream(L3G4200D_ADDR, 6, &L3G4200D_Handler);
    while(!hasData)
    {
      I2C1_Tick();
    }
    hasData = FALSE;
    x += gyro_local.xyz_data.x;
    y += gyro_local.xyz_data.y;
    z += gyro_local.xyz_data.z;
  }
  xyz_offset.x = x / CALIBRATION_SAMPLES;
  xyz_offset.y = y / CALIBRATION_SAMPLES;
  xyz_offset.z = z / CALIBRATION_SAMPLES;
  
  return;
}

void L3G4200D_IssueRead()
{
  if(!I2C1_ReserveReq(2))
    return;
  if(FALSE == I2C1_WriteByte(L3G4200D_ADDR, L3G4200D_OUT_X_L | 0x80))
    return;
  if(FALSE == I2C1_ReadByteStream(L3G4200D_ADDR, 6, &L3G4200D_Handler))
    return;
}

void L3G4200D_ReadGyro(gyro_type *gyro_data)
{
  if(hasData)
  {
    hasData = FALSE;
    /*
    gyro_hp_local.x = high_pass_const * (gyro_hp_local.x + gyro_local.xyz_data.x - gyro_local_prev.xyz_data.x);
    gyro_hp_local.y = high_pass_const * (gyro_hp_local.y + gyro_local.xyz_data.y - gyro_local_prev.xyz_data.y);
    gyro_hp_local.z = high_pass_const * (gyro_hp_local.z + gyro_local.xyz_data.z - gyro_local_prev.xyz_data.z);
    
    gyro_local_prev.xyz_data.x = gyro_local.xyz_data.x;
    gyro_local_prev.xyz_data.y = gyro_local.xyz_data.y;
    gyro_local_prev.xyz_data.z = gyro_local.xyz_data.z;
    
    gyro_data->x = gyro_hp_local.x;
    gyro_data->y = gyro_hp_local.y;
    gyro_data->z = gyro_hp_local.z;
    */
    
    gyro_data->x = (gyro_local.xyz_data.x - xyz_offset.x) * L3G4200D_SCALE_FACTOR;
    gyro_data->y = (gyro_local.xyz_data.y - xyz_offset.y) * L3G4200D_SCALE_FACTOR;
    gyro_data->z = (gyro_local.xyz_data.z - xyz_offset.z) * L3G4200D_SCALE_FACTOR;
    
  }
  else
  {
    USART6_SendData("L3G4200D Comm Fault\n\r");
  }
  
  return;
}

void L3G4200D_Handler(uint8 *data, uint8 data_size)
{
  for(uint8 i=0; i<data_size; i++)
  {
    gyro_local.data[i] = data[i];
  }
  hasData = TRUE;
  return;
}
