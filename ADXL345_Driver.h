#ifndef _ADXL345_H
#define _ADXL345_H

#include "Drivers.h"
#include "Type.h"

#define ADXL345_ADDR    0x53

#define OFSX_REG                0x1E
#define OFSY_REG                0x1F
#define OFSZ_REG                0x20
#define BW_RATE_REG             0x2C
#define POWER_CTRL_REG          0x2D
#define DATA_FORMAT_REG         0x31
#define DATAX0_REG              0x32
#define DATAX1_REG              0x33
#define DATAY0_REG              0x34
#define DATAY1_REG              0x35
#define DATAZ0_REG              0x36
#define DATAZ1_REG              0x37

#define ADXL345_SCALE_FACTOR    0.0039     // for full resolution (16bit), 16g sensitivity

#define ADXL345_CALIBRATION_SAMPLES     200

void ADXL345_Init(void);
void ADXL345_ReadAccel(accel_type *accel_data);

static void calibrate_accel(void);

void ADXL345_IssueRead(void);

void ADXL345_Handle(uint8 *data, uint8 data_size);

#endif /* _ADXL345_H */