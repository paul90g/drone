#ifndef _GPIOA_DRIVER_H
#define _GPIOA_DRIVER_H

#include "mem_map.h"
#include "GPIO_Type.h"
#include "Drivers.h"

#define GPIOA   ((GPIO_TypeDef *)GPIOA_BASE)

#define PIN_MODE_INPUT          0
#define PIN_MODE_OUTPUT         1
#define PIN_MODE_ALT_FUNC       2
#define PIN_MODE_ANALOG         3

#define PIN_OUTPUT_PUSH_PULL    0
#define PIN_OUTPUT_OPEN_DRAIN   1

#define PIN_NO_PUPD             0
#define PIN_PULL_UP             1
#define PIN_PULL_DOWN           2

#define AF_MASK                 0xF

#define GPIOA_PinMode(PIN_NR, MODE)             {GPIOA->MODER &= ~(11 << (PIN_NR * 2)); GPIOA->MODER |= (MODE << (PIN_NR * 2));}
#define GPIOA_PinOutputType(PIN_NR, TYPE)       {GPIOA->OTYPER &= ~(1 << PIN_NR); GPIOA->OTYPER |= (TYPE << PIN_NR);}
#define GPIOA_Pin_PullUp_PullDown(PIN_NR, PUPD) {GPIOA->PUPDR &= ~(11 << (PIN_NR * 2)); GPIOA->PUPDR |= (PUPD << (PIN_NR * 2));}
#define GPIOA_Pin_Speed(PIN_NR, SPD)            {(GPIOA->OSPEEDR &= ~(SPEED_BITMASK << (PIN_NR * 2))); (GPIOA->OSPEEDR |= (SPD << (PIN_NR * 2)));}
#define GPIOA_Pin_ReadData(PIN_NR)              (GPIOA->IDR & (1 << PIN_NR))
#define GPIOA_Pin_WriteData(PIN_NR, DATA)       {GPIOA->ODR &= ~(1 << PIN_NR); GPIOA->ODR |= (DATA << PIN_NR);}
#define GPIOA_Pin_Set(PIN_NR)                   (GPIOA->BSRR |= (1 << PIN_NR))
#define GPIOA_Pin_Reset(PIN_NR)                 (GPIOA->BSRR |= (1 << (PIN_NR + 16)))
#define GPIOA_Pin_SetAltFunc(PIN_NR, AltFunc)   {if(PIN_NR < 8) {                                       \
                                                  GPIOA->AFRL &= ~(AF_MASK << (PIN_NR * 4));            \
                                                  GPIOA->AFRL |= (AltFunc << (PIN_NR * 4));             \
                                                 } else {                                               \
                                                  GPIOA->AFRH &= ~(AF_MASK << ((PIN_NR - 8) * 4));      \
                                                  GPIOA->AFRH |= (AltFunc << ((PIN_NR - 8) * 4));       \
                                                 }}

void GPIOA_Init(void);

#endif /* _GPIOA_DRIVER_H */