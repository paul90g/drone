#ifndef _USART6_DRIVER_H
#define _USART6_DRIVER_H

#include "USART_Type.h"

#define ONE_STOP_BIT    0
#define HALF_STOP_BIT   1
#define TWO_STOP_BIT    2

#define USART6  ((USART_Type *)USART6_BASE)

/* Status Register */
#define USART6_ClearStatusFlags()       (USART6->SR = 0x00C00000)

/* Data Register */
#define USART6_WriteData(DATA)          (USART6->DR = (DATA & 0x1FF))
#define USART6_ReadData()               (USART6->DR)

/* Baud Rate Register */
#define USART6_ResetBaudRegister()      (USART6->BRR = 0x00)
#define USART6_SetBaud_Register(BAUD)   (USART6->BRR = BAUD)

/* Control Register 1 */
#define USART6_SetOversampling8()       (USART6->CR1 |= OVER8_BIT)
#define USART6_SetOversampling16()      (USART6->CR1 &= ~OVER8_BIT)
#define USART6_Enable()                 (USART6->CR1 |= UE_BIT)
#define USART6_Disable()                (USART6->CR1 &= ~UE_BIT)
#define USART6_WordLength8Bit()         (USART6->CR1 &= ~M_BIT)
#define USART6_WordLength9Bit()         (USART6->CR1 |= M_BIT)
#define USART6_EnableParityControl()    (USART6->CR1 |= PCE_BIT)
#define USART6_DisableParityControl()   (USART6->CR1 &= ~PCE_BIT)
#define USART6_EnablePEInterrupt()      (USART6->CR1 |= PEIE_BIT)
#define USART6_DisablePEInterrupt()     (USART6->CR1 &= ~PEIE_BIT)
#define USART6_EnableTXEInterrupt()     (USART6->CR1 |= TXEIE_BIT)
#define USART6_DisableTXEInterrupt()    (USART6->CR1 &= ~TXEIE_BIT)
#define USART6_EnableTCInterrupt()      (USART6->CR1 |= TCIE_BIT)
#define USART6_DisableTCInterrupt()     (USART6->CR1 &= ~TCIE_BIT)
#define USART6_EnableRXNEInterrupt()    (USART6->CR1 |= RXNEIE_BIT)
#define USART6_DisableRXNEInterrupt()   (USART6->CR1 &= ~RXNEIE_BIT)
#define USART6_EnableIDLEInterrupt()    (USART6->CR1 |= IDLEIE_BIT
#define USART6_DisableIDLEInterrupt()   (USART6->CR1 &= ~IDLEIE_BIT)
#define USART6_EnableTransmitter()      (USART6->CR1 |= TE_BIT)
#define USART6_DisableTransmitter()     (USART6->CR1 &= ~TE_BIT)
#define USART6_EnableReceiver()         (USART6->CR1 |= RE_BIT)
#define USART6_DisableReceiver()        (USART6->CR1 &= ~RE_BIT)

/* Control Register 2 */
#define USART6_SetStopBits(STOP)        {(USART6->CR2 &= ~STOP_BITMASK); (USART6->CR2 |= (STOP << STOP_BITS_OFFSET));}
#define USART6_ClockEnable()            (USART6->CR2 |= CLKEN_BIT)
#define USART6_ClockDisable()           (USART6->CR2 &= ~CLKEN_BIT)

/* Control Register 3 */
#define USART6_EnableDMATransm()        (USART6->CR3 |= DMAT_BIT)
#define USART6_EnableDMAReceive()       (USART6->CR3 |= DMAR_BIT)

void USART6_Init(void);
void USART6_SendData(const uint8 *data);
static void USART6_SetBaud(uint32 baud);

#endif /* _USART6_DRIVER_H */