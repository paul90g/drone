#ifndef _FPU_DRIVER_H
#define _FPU_DRIVER_H

#include "Type.h"

typedef struct{
  uint32 CPACR;
  uint32 FPCCR;
  uint32 FPCAR;
  uint32 FPDSCR;
} FPU_Type;

#define FPU     ((FPU_Type *)FPU_BASE)

#define COPROC_ACCESS_DENIED            0
#define COPROC_ACCESS_PRIVILEGED        1
#define COPROC_ACCESS_FULL              3

#define COPROC0_BITS_OFFSET     20
#define COPROC0_BITMASK         0x300000
#define COPROC1_BITS_OFFSET     22
#define COPROC1_BITMASK         0xC00000

#define FPU_CoprocMode(MODE0 , MODE1)    {(FPU->CPACR &= ~(COPROC0_BITMASK | COPROC1_BITMASK));         \
                                          (FPU->CPACR |= ((MODE0 << COPROC0_BITS_OFFSET) | (MODE1 << COPROC1_BITS_OFFSET)));}

void FPU_Init(void);

#endif /* _FPU_DRIVER_H */