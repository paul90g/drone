#ifndef _ADC_DRIVER_H
#define _ADC_DRIVER_H

#include "type.h"
#include "mem_map.h"

/* ADC clock max 36MHz */

typedef struct {
  uint32 SR;
  uint32 CR1;
  uint32 CR2;
  uint32 SMPR1;
  uint32 SMPR2;
  uint32 JOFR1;
  uint32 JOFR2;
  uint32 JOFR3;
  uint32 JOFR4;
  uint32 HTR;
  uint32 LTR;
  uint32 SQR1;
  uint32 SQR2;
  uint32 SQR3;
  uint32 JSQR;
  uint32 JDR1;
  uint32 JDR2;
  uint32 JDR3;
  uint32 JDR4;
  uint32 DR;
} ADC_Type;

#define ADC     ((ADC_Type *)ADC1_BASE)
#define ADC_CCR ((uint32 *)(ADC1_BASE + 0x304))

#define ADON_BIT                0

#define EOC_BIT                 1

#define RES_BITMASK             0x3000000
#define RES_BITS_OFFSET         24
#define RES_12BIT               0
#define RES_10BIT               1
#define RES_8BIT                2
#define RES_6BIT                3

#define EOCIE_BIT               5

#define SWSTART_BIT             30

/* ADC Max Clk 36 MHz
* 00: PCLK2 divided by 2
* 01: PCLK2 divided by 4
* 10: PCLK2 divided by 6
* 11: PCLK2 divided by 8 */
#define ADC_CLK_PRSC            1
#define ADCPRE_BITMASK          0x30000
#define ADCPRE_BITS_OFFSET      16

#define L_BITMASK               0xF00000
#define L_BITS_OFFSET           20

#define SQ1_BITS_OFFSET         0
#define SQ1_BITMASK             0x1F
#define SQ2_BITS_OFFSET         5
#define SQ2_BITMASK             0x3E0
#define SQ3_BITS_OFFSET         10
#define SQ3_BITMASK             0x7C00
#define SQ4_BITS_OFFSET         15
#define SQ4_BITMASK             0xF8000
#define SQ5_BITS_OFFSET         20
#define SQ5_BITMASK             0x1F00000
#define SQ6_BITS_OFFSET         25
#define SQ6_BITMASK             0x3E000000
#define SQ7_BITS_OFFSET         0
#define SQ7_BITMASK             0x1F
#define SQ8_BITS_OFFSET         5
#define SQ8_BITMASK             0x3E0
#define SQ9_BITS_OFFSET         10
#define SQ9_BITMASK             0x7C00
#define SQ10_BITS_OFFSET        15
#define SQ10_BITMASK            0xF8000
#define SQ11_BITS_OFFSET        20
#define SQ11_BITMASK            0x1F00000
#define SQ12_BITS_OFFSET        25
#define SQ12_BITMASK            0x3E000000
#define SQ13_BITS_OFFSET        0
#define SQ13_BITMASK            0x1F
#define SQ14_BITS_OFFSET        5
#define SQ14_BITMASK            0x3E0
#define SQ15_BITS_OFFSET        10
#define SQ15_BITMASK            0x7C00
#define SQ16_BITS_OFFSET        15
#define SQ16_BITMASK            0xF8000


#define ADC_Enable()            (ADC->CR2 |= (1 << ADON_BIT))
#define ADC_Disable()           (ADC->CR2 &= ~(1 << ADON_BIT))

#define ADC_Clear_IntFlag()     (ADC->SR &= ~(1 << EOC_BIT))

#define ADC_SetResolution(RES)          {ADC->CR1 &= ~(RES_BITMASK); ADC->CR1 |= (RES << RES_BITS_OFFSET);}

#define ADC_EOCIntEnable()              (ADC->CR1 |= (1 << EOCIE_BIT))
#define ADC_EOCIntDisable()             (ADC->CR1 &= ~(1 << EOCIE_BIT))

#define ADC_StartConversion()           (ADC->CR2 |= (1 << SWSTART_BIT))
#define ADC_StopConversion()            (ADC->CR2 &= ~(1 << SWSTART_BIT))

#define ADC_Read()                      (ADC->DR & 0xFFFF)

#define ADC_SetClk()            {(*ADC_CCR) &= ~(ADCPRE_BITMASK); (*ADC_CCR) |= (ADC_CLK_PRSC << ADCPRE_BITS_OFFSET);}

#define ADC_NrOfChannels(NR_CH) {ADC->SQR1 &= ~(L_BITMASK); ADC->SQR1 |= (NR_CH << L_BITS_OFFSET);}

#define ADC_SetSEQ1(CHANNEL)    {ADC->SQR3 &= ~(SQ1_BITMASK); ADC->SQR3 |= (CHANNEL << SQ1_BITS_OFFSET);}
#define ADC_SetSEQ2(CHANNEL)    {ADC->SQR3 &= ~(SQ2_BITMASK); ADC->SQR3 |= (CHANNEL << SQ2_BITS_OFFSET);}
#define ADC_SetSEQ3(CHANNEL)    {ADC->SQR3 &= ~(SQ3_BITMASK); ADC->SQR3 |= (CHANNEL << SQ3_BITS_OFFSET);}
#define ADC_SetSEQ4(CHANNEL)    {ADC->SQR3 &= ~(SQ4_BITMASK); ADC->SQR3 |= (CHANNEL << SQ4_BITS_OFFSET);}
#define ADC_SetSEQ5(CHANNEL)    {ADC->SQR3 &= ~(SQ5_BITMASK); ADC->SQR3 |= (CHANNEL << SQ5_BITS_OFFSET);}
#define ADC_SetSEQ6(CHANNEL)    {ADC->SQR3 &= ~(SQ6_BITMASK); ADC->SQR3 |= (CHANNEL << SQ6_BITS_OFFSET);}
#define ADC_SetSEQ7(CHANNEL)    {ADC->SQR2 &= ~(SQ7_BITMASK); ADC->SQR2 |= (CHANNEL << SQ7_BITS_OFFSET);}
#define ADC_SetSEQ8(CHANNEL)    {ADC->SQR2 &= ~(SQ8_BITMASK); ADC->SQR2 |= (CHANNEL << SQ8_BITS_OFFSET);}
#define ADC_SetSEQ9(CHANNEL)    {ADC->SQR2 &= ~(SQ9_BITMASK); ADC->SQR2 |= (CHANNEL << SQ9_BITS_OFFSET);}
#define ADC_SetSEQ10(CHANNEL)   {ADC->SQR2 &= ~(SQ10_BITMASK); ADC->SQR2 |= (CHANNEL << SQ10_BITS_OFFSET);}
#define ADC_SetSEQ11(CHANNEL)   {ADC->SQR2 &= ~(SQ11_BITMASK); ADC->SQR2 |= (CHANNEL << SQ11_BITS_OFFSET);}
#define ADC_SetSEQ12(CHANNEL)   {ADC->SQR2 &= ~(SQ12_BITMASK); ADC->SQR2 |= (CHANNEL << SQ12_BITS_OFFSET);}
#define ADC_SetSEQ13(CHANNEL)   {ADC->SQR1 &= ~(SQ13_BITMASK); ADC->SQR1 |= (CHANNEL << SQ13_BITS_OFFSET);}
#define ADC_SetSEQ14(CHANNEL)   {ADC->SQR1 &= ~(SQ14_BITMASK); ADC->SQR1 |= (CHANNEL << SQ14_BITS_OFFSET);}
#define ADC_SetSEQ15(CHANNEL)   {ADC->SQR1 &= ~(SQ15_BITMASK); ADC->SQR1 |= (CHANNEL << SQ15_BITS_OFFSET);}
#define ADC_SetSEQ16(CHANNEL)   {ADC->SQR1 &= ~(SQ16_BITMASK); ADC->SQR1 |= (CHANNEL << SQ16_BITS_OFFSET);}

void ADC_Init(void);

#endif /* _ADC_DRIVER_H */