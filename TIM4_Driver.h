#ifndef _TIM4_DRIVER_H
#define _TIM4_DRIVER_H

/**************************/
/* Timer 4 - 16 bit timer */
/**************************/

#include "type.h"
#include "Drivers.h"
#include "GenPurpTimer.h"

#define TIM4    ((GP_TIM_Type *) TIM4_BASE)

#define ARPE_BIT                7
#define CMS_BITS_OFFSET         5
#define CMS_BITMASK             0x60
#define DIR_BIT                 4
#define OPM_BIT                 3
#define URS_BIT                 2
#define UDIS_BIT                1
#define CEN_BIT                 0

#define TIM4_CounterEnable()            (TIM4->CR1 |= (1 << CEN_BIT))
#define TIM4_UpdateEnable()             (TIM4->CR1 |= (1 << UDIS_BIT))          // enable UEV
#define TIM4_UpdateDisable()            (TIM4->CR1 &= ~(1 << UDIS_BIT))         // disable UEV
#define TIM4_OnePulseModeEnable()       (TIM4->CR1 |= (1 << OPM_BIT))           // counter stopped at next update event
#define TIM4_OnePulseModeDiable()       (TIM4->CR1 &= ~(1 << OPM_BIT))          // counter is not stopped at an update event
#define TIM4_UpcountMode()              (TIM4->CR1 &= ~(1 << DIR_BIT))
#define TIM4_DowncountMode()            (TIM4->CR1 |= (1 << DIR_BIT))
#define TIM4_AutoReloadBuffered()       (TIM4->CR1 |= (1 << ARPE_BIT))
#define TIM4_AutoReloadUnbuffered()     (TIM4->CR1 &= ~(1 << ARPE_BIT))

#define TIE_BIT         6
#define CC4IE_BIT       4
#define CC2IE_BIT       2
#define CC1IE_BIT       1
#define UIE_BIT         0

#define TIM4_TriggerIntEnable()         (TIM4->DIER |= (1 << TIE_BIT))
#define TIM4_TriggerIntDisable()        (TIM4->DIER &= ~(1 << TIE_BIT))
#define TIM4_CC4IntEnable()             (TIM4->DIER |= (1 << CC4IE_BIT))
#define TIM4_CC4IntDisable()            (TIM4->DIER &= ~(1 << CC4IE_BIT))
#define TIM4_CC2IntEnable()             (TIM4->DIER |= (1 << CC2IE_BIT))
#define TIM4_CC2IntDisable()            (TIM4->DIER &= ~(1 << CC2IE_BIT))
#define TIM4_CC1IntEnable()             (TIM4->DIER |= (1 << CC1IE_BIT))
#define TIM4_CC1IntDisable()            (TIM4->DIER &= ~(1 << CC1IE_BIT))
#define TIM4_UpdateIntEnable()          (TIM4->DIER |= (1 << UIE_BIT))
#define TIM4_UpdateIntDisable()         (TIM4->DIER &= ~(1 << UIE_BIT))

#define UG_BIT          0

#define TIM4_UpdateEvent()      (TIM4->EGR |= (1 << UG_BIT))

#define CHANNEL_OUTPUT          0
#define CHANNEL_INPUT1          1
#define CHANNEL_INPUT2          2
#define CHANNEL_INPUT3          3
#define OC_FROZEN               0
#define OC_ACTIVE_LVL_MATCH     1
#define OC_INACTIVE_LVL_MATCH   2
#define OC_TOGGLE               3
#define OC_FORCE_INACTIVE       4
#define OC_FORCE_ACTIVE         5
#define OC_PWM1                 6
#define OC_PWM2                 7

#define CC1S_BITS_OFFSET        0
#define CC1S_BITMASK            0x3
#define OC1FE_BIT               2
#define OC1PE_BIT               3
#define OC1M_BITS_OFFSET        4
#define OC1M_BITMASK            0x70
#define OC1CE_BIT               7
#define CC2S_BITS_OFFSET        8
#define CC2S_BITMASK            0x300
#define OC2FE_BIT               10
#define OC2PE_BIT               11
#define OC2M_BITS_OFFSET        12
#define OC2M_BITMASK            0x7000
#define OC2CE_BIT               15
#define CC3S_BITS_OFFSET        0
#define CC3S_BITMASK            0x3
#define OC3FE_BIT               2
#define OC3PE_BIT               3
#define OC3M_BITS_OFFSET        4
#define OC3M_BITMASK            0x70
#define OC3CE_BIT               7
#define CC4S_BITS_OFFSET        8
#define CC4S_BITMASK            0x300
#define OC4FE_BIT               10
#define OC4PE_BIT               11
#define OC4M_BITS_OFFSET        12
#define OC4M_BITMASK            0x7000
#define OC4CE_BIT               15

#define TIM4_SelectOutputCompare()              {TIM4->CCMR1 &= ~(CC1S_BITMASK); \
                                                  TIM4->CCMR1 &= ~(CC2S_BITMASK);\
                                                  TIM4->CCMR2 &= ~(CC3S_BITMASK);\
                                                  TIM4->CCMR2 &= ~(CC4S_BITMASK);}
#define TIM4_Channel1OutputCompareMode(MODE)    {TIM4->CCMR1 &= ~(OC1M_BITMASK); TIM4->CCMR1 |= (MODE << OC1M_BITS_OFFSET);}
#define TIM4_Channel2OutputCompareMode(MODE)    {TIM4->CCMR1 &= ~(OC2M_BITMASK); TIM4->CCMR1 |= (MODE << OC2M_BITS_OFFSET);}
#define TIM4_Channel3OutputCompareMode(MODE)    {TIM4->CCMR2 &= ~(OC3M_BITMASK); TIM4->CCMR2 |= (MODE << OC3M_BITS_OFFSET);}
#define TIM4_Channel4OutputCompareMode(MODE)    {TIM4->CCMR2 &= ~(OC4M_BITMASK); TIM4->CCMR2 |= (MODE << OC4M_BITS_OFFSET);}
#define TIM4_Channel1PreloadEnable()            (TIM4->CCMR1 |= (1 << OC1PE_BIT))
#define TIM4_Channel2PreloadEnable()            (TIM4->CCMR1 |= (1 << OC2PE_BIT))
#define TIM4_Channel3PreloadEnable()            (TIM4->CCMR1 |= (1 << OC3PE_BIT))
#define TIM4_Channel4PreloadEnable()            (TIM4->CCMR1 |= (1 << OC4PE_BIT))
#define TIM4_Channel1PreloadDisable()           (TIM4->CCMR1 &= ~(1 << OC1PE_BIT))
#define TIM4_Channel2PreloadDisable()           (TIM4->CCMR1 &= ~(1 << OC2PE_BIT))
#define TIM4_Channel3PreloadDisable()           (TIM4->CCMR1 &= ~(1 << OC3PE_BIT))
#define TIM4_Channel4PreloadDisable()           (TIM4->CCMR1 &= ~(1 << OC4PE_BIT))

#define CC1E_BIT        0
#define CC1P_BIT        1
#define CC1NP_BIT       3
#define CC2E_BIT        4
#define CC2P_BIT        5
#define CC2NP_BIT       7
#define CC3E_BIT        8
#define CC3P_BIT        9
#define CC3NP_BIT       11
#define CC4E_BIT        12
#define CC4P_BIT        13
#define CC4NP_BIT       15

#define TIM4_Ch1OutputEnable()          (TIM4->CCER |= (1 << CC1E_BIT))
#define TIM4_Ch1OutputDisable()         (TIM4->CCER &= ~(1 << CC1E_BIT))
#define TIM4_Ch2OutputEnable()          (TIM4->CCER |= (1 << CC2E_BIT))
#define TIM4_Ch2OutputDisable()         (TIM4->CCER &= ~(1 << CC2E_BIT))
#define TIM4_Ch3OutputEnable()          (TIM4->CCER |= (1 << CC3E_BIT))
#define TIM4_Ch3OutputDisable()         (TIM4->CCER &= ~(1 << CC3E_BIT))
#define TIM4_Ch4OutputEnable()          (TIM4->CCER |= (1 << CC4E_BIT))
#define TIM4_Ch4OutputDisable()         (TIM4->CCER &= ~(1 << CC4E_BIT))
#define TIM4_Ch1OutputActiveHigh()      (TIM4->CCER &= ~(1 << CC1P_BIT))
#define TIM4_Ch1OutputActiveLow()       (TIM4->CCER |= (1 << CC1P_BIT))
#define TIM4_Ch2OutputActiveHigh()      (TIM4->CCER &= ~(1 << CC2P_BIT))
#define TIM4_Ch2OutputActiveLow()       (TIM4->CCER |= (1 << CC2P_BIT))
#define TIM4_Ch3OutputActiveHigh()      (TIM4->CCER &= ~(1 << CC3P_BIT))
#define TIM4_Ch3OutputActiveLow()       (TIM4->CCER |= (1 << CC3P_BIT))
#define TIM4_Ch4OutputActiveHigh()      (TIM4->CCER &= ~(1 << CC4P_BIT))
#define TIM4_Ch4OutputActiveLow()       (TIM4->CCER |= (1 << CC4P_BIT))

#define TIM4_SetPrescaler(PRSC)         (TIM4->PSC = PRSC)

#define TIM4_SetAutoReload(ARR_VAL)     (TIM4->ARR = ARR_VAL)

#define TIM4_SetCaptureCompare1(CCVAL)  (TIM4->CCR1 = CCVAL)
#define TIM4_SetCaptureCompare2(CCVAL)  (TIM4->CCR2 = CCVAL)
#define TIM4_SetCaptureCompare3(CCVAL)  (TIM4->CCR3 = CCVAL)
#define TIM4_SetCaptureCompare4(CCVAL)  (TIM4->CCR4 = CCVAL)

#define TIM4_ClearStatusReg()           (TIM4->SR = 0x00)

void TIM4_Init(void);

#endif /* _TIM4_DRIVER_H */