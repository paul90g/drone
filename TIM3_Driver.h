#ifndef _TIM3_DRIVER_H
#define _TIM3_DRIVER_H

/**************************/
/* Timer 3 - 16 bit timer */
/**************************/

#include "type.h"
#include "Drivers.h"
#include "GenPurpTimer.h"

#define TIM3    ((GP_TIM_Type *) TIM3_BASE)

#define ARPE_BIT                7
#define CMS_BITS_OFFSET         5
#define CMS_BITMASK             0x60
#define DIR_BIT                 4
#define OPM_BIT                 3
#define URS_BIT                 2
#define UDIS_BIT                1
#define CEN_BIT                 0

#define TIM3_CounterEnable()            (TIM3->CR1 |= (1 << CEN_BIT))
#define TIM3_UpdateEnable()             (TIM3->CR1 |= (1 << UDIS_BIT))          // enable UEV
#define TIM3_UpdateDisable()            (TIM3->CR1 &= ~(1 << UDIS_BIT))         // disable UEV
#define TIM3_OnePulseModeEnable()       (TIM3->CR1 |= (1 << OPM_BIT))           // counter stopped at next update event
#define TIM3_OnePulseModeDiable()       (TIM3->CR1 &= ~(1 << OPM_BIT))          // counter is not stopped at an update event
#define TIM3_UpcountMode()              (TIM3->CR1 &= ~(1 << DIR_BIT))
#define TIM3_DowncountMode()            (TIM3->CR1 |= (1 << DIR_BIT))
#define TIM3_AutoReloadBuffered()       (TIM3->CR1 |= (1 << ARPE_BIT))
#define TIM3_AutoReloadUnbuffered()     (TIM3->CR1 &= ~(1 << ARPE_BIT))

#define TIE_BIT         6
#define CC4IE_BIT       4
#define CC2IE_BIT       2
#define CC1IE_BIT       1
#define UIE_BIT         0

#define TIM3_TriggerIntEnable()         (TIM3->DIER |= (1 << TIE_BIT))
#define TIM3_TriggerIntDisable()        (TIM3->DIER &= ~(1 << TIE_BIT))
#define TIM3_CC4IntEnable()             (TIM3->DIER |= (1 << CC4IE_BIT))
#define TIM3_CC4IntDisable()            (TIM3->DIER &= ~(1 << CC4IE_BIT))
#define TIM3_CC2IntEnable()             (TIM3->DIER |= (1 << CC2IE_BIT))
#define TIM3_CC2IntDisable()            (TIM3->DIER &= ~(1 << CC2IE_BIT))
#define TIM3_CC1IntEnable()             (TIM3->DIER |= (1 << CC1IE_BIT))
#define TIM3_CC1IntDisable()            (TIM3->DIER &= ~(1 << CC1IE_BIT))
#define TIM3_UpdateIntEnable()          (TIM3->DIER |= (1 << UIE_BIT))
#define TIM3_UpdateIntDisable()         (TIM3->DIER &= ~(1 << UIE_BIT))

#define UG_BIT          0

#define TIM3_UpdateEvent()      (TIM3->EGR |= (1 << UG_BIT))

#define CHANNEL_OUTPUT          0
#define CHANNEL_INPUT1          1
#define CHANNEL_INPUT2          2
#define CHANNEL_INPUT3          3
#define OC_FROZEN               0
#define OC_ACTIVE_LVL_MATCH     1
#define OC_INACTIVE_LVL_MATCH   2
#define OC_TOGGLE               3
#define OC_FORCE_INACTIVE       4
#define OC_FORCE_ACTIVE         5
#define OC_PWM1                 6
#define OC_PWM2                 7

#define CC1S_BITS_OFFSET        0
#define CC1S_BITMASK            0x3
#define OC1FE_BIT               2
#define OC1PE_BIT               3
#define OC1M_BITS_OFFSET        4
#define OC1M_BITMASK            0x70
#define OC1CE_BIT               7
#define CC2S_BITS_OFFSET        8
#define CC2S_BITMASK            0x300
#define OC2FE_BIT               10
#define OC2PE_BIT               11
#define OC2M_BITS_OFFSET        12
#define OC2M_BITMASK            0x7000
#define OC2CE_BIT               15
#define CC3S_BITS_OFFSET        0
#define CC3S_BITMASK            0x3
#define OC3FE_BIT               2
#define OC3PE_BIT               3
#define OC3M_BITS_OFFSET        4
#define OC3M_BITMASK            0x70
#define OC3CE_BIT               7
#define CC4S_BITS_OFFSET        8
#define CC4S_BITMASK            0x300
#define OC4FE_BIT               10
#define OC4PE_BIT               11
#define OC4M_BITS_OFFSET        12
#define OC4M_BITMASK            0x7000
#define OC4CE_BIT               15

#define TIM3_SelectOutputCompare()              {TIM3->CCMR1 &= ~(CC1S_BITMASK); \
                                                  TIM3->CCMR1 &= ~(CC2S_BITMASK);\
                                                  TIM3->CCMR2 &= ~(CC3S_BITMASK);\
                                                  TIM3->CCMR2 &= ~(CC4S_BITMASK);}
#define TIM3_Channel1OutputCompareMode(MODE)    {TIM3->CCMR1 &= ~(OC1M_BITMASK); TIM3->CCMR1 |= (MODE << OC1M_BITS_OFFSET);}
#define TIM3_Channel2OutputCompareMode(MODE)    {TIM3->CCMR1 &= ~(OC2M_BITMASK); TIM3->CCMR1 |= (MODE << OC2M_BITS_OFFSET);}
#define TIM3_Channel3OutputCompareMode(MODE)    {TIM3->CCMR2 &= ~(OC3M_BITMASK); TIM3->CCMR2 |= (MODE << OC3M_BITS_OFFSET);}
#define TIM3_Channel4OutputCompareMode(MODE)    {TIM3->CCMR2 &= ~(OC4M_BITMASK); TIM3->CCMR2 |= (MODE << OC4M_BITS_OFFSET);}
#define TIM3_Channel1PreloadEnable()            (TIM3->CCMR1 |= (1 << OC1PE_BIT))
#define TIM3_Channel2PreloadEnable()            (TIM3->CCMR1 |= (1 << OC2PE_BIT))
#define TIM3_Channel3PreloadEnable()            (TIM3->CCMR1 |= (1 << OC3PE_BIT))
#define TIM3_Channel4PreloadEnable()            (TIM3->CCMR1 |= (1 << OC4PE_BIT))
#define TIM3_Channel1PreloadDisable()           (TIM3->CCMR1 &= ~(1 << OC1PE_BIT))
#define TIM3_Channel2PreloadDisable()           (TIM3->CCMR1 &= ~(1 << OC2PE_BIT))
#define TIM3_Channel3PreloadDisable()           (TIM3->CCMR1 &= ~(1 << OC3PE_BIT))
#define TIM3_Channel4PreloadDisable()           (TIM3->CCMR1 &= ~(1 << OC4PE_BIT))

#define CC1E_BIT        0
#define CC1P_BIT        1
#define CC1NP_BIT       3
#define CC2E_BIT        4
#define CC2P_BIT        5
#define CC2NP_BIT       7
#define CC3E_BIT        8
#define CC3P_BIT        9
#define CC3NP_BIT       11
#define CC4E_BIT        12
#define CC4P_BIT        13
#define CC4NP_BIT       15

#define TIM3_Ch1OutputEnable()          (TIM3->CCER |= (1 << CC1E_BIT))
#define TIM3_Ch1OutputDisable()         (TIM3->CCER &= ~(1 << CC1E_BIT))
#define TIM3_Ch2OutputEnable()          (TIM3->CCER |= (1 << CC2E_BIT))
#define TIM3_Ch2OutputDisable()         (TIM3->CCER &= ~(1 << CC2E_BIT))
#define TIM3_Ch3OutputEnable()          (TIM3->CCER |= (1 << CC3E_BIT))
#define TIM3_Ch3OutputDisable()         (TIM3->CCER &= ~(1 << CC3E_BIT))
#define TIM3_Ch4OutputEnable()          (TIM3->CCER |= (1 << CC4E_BIT))
#define TIM3_Ch4OutputDisable()         (TIM3->CCER &= ~(1 << CC4E_BIT))
#define TIM3_Ch1OutputActiveHigh()      (TIM3->CCER &= ~(1 << CC1P_BIT))
#define TIM3_Ch1OutputActiveLow()       (TIM3->CCER |= (1 << CC1P_BIT))
#define TIM3_Ch2OutputActiveHigh()      (TIM3->CCER &= ~(1 << CC2P_BIT))
#define TIM3_Ch2OutputActiveLow()       (TIM3->CCER |= (1 << CC2P_BIT))
#define TIM3_Ch3OutputActiveHigh()      (TIM3->CCER &= ~(1 << CC3P_BIT))
#define TIM3_Ch3OutputActiveLow()       (TIM3->CCER |= (1 << CC3P_BIT))
#define TIM3_Ch4OutputActiveHigh()      (TIM3->CCER &= ~(1 << CC4P_BIT))
#define TIM3_Ch4OutputActiveLow()       (TIM3->CCER |= (1 << CC4P_BIT))

#define TIM3_SetPrescaler(PRSC)         (TIM3->PSC = PRSC)

#define TIM3_SetAutoReload(ARR_VAL)     (TIM3->ARR = ARR_VAL)

#define TIM3_SetCaptureCompare1(CCVAL)  (TIM3->CCR1 = CCVAL)
#define TIM3_SetCaptureCompare2(CCVAL)  (TIM3->CCR2 = CCVAL)
#define TIM3_SetCaptureCompare3(CCVAL)  (TIM3->CCR3 = CCVAL)
#define TIM3_SetCaptureCompare4(CCVAL)  (TIM3->CCR4 = CCVAL)

#define TIM3_ClearStatusReg()           (TIM3->SR = 0x00)

void TIM3_Init(void);

#endif /* _TIM3_DRIVER_H */