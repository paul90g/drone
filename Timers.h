#ifndef _TIMERS_H
#define _TIMERS_H

#include "Type.h"

typedef enum{
  Timer_Reset,
  Timer_Running,
  Timer_Passed
} TimerType;

typedef struct{
  TimerType timerState;
  uint32 timerVal;
} Timer32bit;

typedef struct{
  TimerType timerState;
  uint16 timerVal;
} Timer16bit;

void init32bitTimer(Timer32bit* timer);
void init16bitTimer(Timer16bit* timer);

void start32bitTimer(Timer32bit* timer, uint32 timeout);
void start16bitTimer(Timer16bit* timer, uint16 timeout);

boolean is16bitTimerPassed(Timer16bit* timer);
boolean is32bitTimerPassed(Timer32bit* timer);

TimerType getStateNoStart16bitTimer(Timer16bit *timer);
TimerType getStateNoStart32bitTimer(Timer32bit *timer);

TimerType getState32bitTimer(Timer32bit* timer, uint32 timeout);
TimerType getState16bitTimer(Timer16bit* timer, uint16 timeout);

#endif /* _TIMERS_H */