#ifndef _CONFIG_H
#define _CONFIG_H

#define DEBUG_MODE

#define F_CPU  100000000

#define USART1_BAUD  115200
#define USART6_BAUD  115200

#define INTERRUP_EN     1       // enable or disable interrupts

#endif /* _CONFIG_H */