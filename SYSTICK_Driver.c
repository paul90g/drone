#include "SYSTICK_Driver.h"
#include "Type.h"

void SYSTICK_Init(){
  SYSTICK_ReloadValue(0x270F); // SYSTICK every 100us
  SYSTICK_ClearValue();
  SYSTICK_EnableInt();
  SYSTICK_AHBClock();
  SYSTICK_Enable();
  return;
}