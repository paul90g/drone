#ifndef _GPIOB_DRIVER_H
#define _GPIOB_DRIVER_H

#define GPIOB   ((GPIO_TypeDef *)GPIOB_BASE)

#define PIN_MODE_INPUT          0
#define PIN_MODE_OUTPUT         1
#define PIN_MODE_ALT_FUNC       2
#define PIN_MODE_ANALOG         3

#define PIN_OUTPUT_PUSH_PULL    0
#define PIN_OUTPUT_OPEN_DRAIN   1

#define PIN_NO_PUPD             0
#define PIN_PULL_UP             1
#define PIN_PULL_DOWN           2

#define AF_MASK                 0xF

#define GPIOB_PinMode(PIN_NR, MODE)             {GPIOB->MODER &= ~(11 << (PIN_NR * 2)); GPIOB->MODER |= (MODE << (PIN_NR * 2));}
#define GPIOB_PinOutputType(PIN_NR, TYPE)       {GPIOB->OTYPER &= ~(1 << PIN_NR); GPIOB->OTYPER |= (TYPE << PIN_NR);}
#define GPIOB_Pin_PullUp_PullDown(PIN_NR, PUPD) {GPIOB->PUPDR &= ~(11 << (PIN_NR * 2)); GPIOB->PUPDR |= (PUPD << (PIN_NR * 2));}
#define GPIOB_Pin_Speed(PIN_NR, SPD)            {(GPIOB->OSPEEDR &= ~(SPEED_BITMASK << (PIN_NR * 2))); (GPIOB->OSPEEDR |= (SPD << (PIN_NR * 2)));}
#define GPIOB_Pin_ReadData(PIN_NR)              (GPIOB->IDR & (1 << PIN_NR))
#define GPIOB_Pin_WriteData(PIN_NR, DATA)       {GPIOB->ODR &= ~(1 << PIN_NR); GPIOB->ODR |= (DATA << PIN_NR);}
#define GPIOB_Pin_Set(PIN_NR)                   (GPIOB->BSRR |= (1 << PIN_NR))
#define GPIOB_Pin_Reset(PIN_NR)                 (GPIOB->BSRR |= (1 << (PIN_NR + 16)))
#define GPIOB_Pin_SetAltFunc(PIN_NR, AltFunc)   {if(PIN_NR < 8) {                                       \
                                                  GPIOB->AFRL &= ~(AF_MASK << (PIN_NR * 4));            \
                                                  GPIOB->AFRL |= (AltFunc << (PIN_NR * 4));             \
                                                 } else {                                               \
                                                  GPIOB->AFRH &= ~(AF_MASK << ((PIN_NR - 8) * 4));      \
                                                  GPIOB->AFRH |= (AltFunc << ((PIN_NR - 8) * 4));       \
                                                 }}

void GPIOB_Init(void);

#endif /* _GPIOB_DRIVER_H */